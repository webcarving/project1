<style>
    .select-single .select2-selection__rendered {
        line-height: 46px !important;
    }
    .select-single .select2-container .select2-selection--single {
        height: 50px !important;
    }
    .select-single .select2-selection__arrow {
        height: 49px !important;
    }
</style>
<h5>{{__('Personal Information')}}</h5>
{!! Form::model($user, array('method' => 'put', 'route' => array('my.profile'), 'class' => 'form', 'files'=>true)) !!}
<h6>{{__('Profile Image')}}</h6>
<div class="row">
    <div class="col-md-6 mb-4">
        <div class="formrow"> {{ ImgUploader::print_image("user_images/$user->image", 150, 150) }} </div>
    </div>
    <div class="col-md-6 mb-4">
        <div class="formrow">
            <div id="thumbnail">
                <div id="thumbnail_img"><img src="images/no-image.jpg" /></div>
                <div id="image_new_upload" class="formrow {!! APFrmErrHelp::hasError($errors, 'image_new') !!}"></div>
            </div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6 mb-4">
                <label class="form-lable">First Name</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'first_name') !!}"> {!! Form::text('first_name', null, array('class'=>'form-control', 'id'=>'first_name',
                'placeholder'=>__('First Name').' '.__('(required)')
                )) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'first_name') !!} </div>
            </div>
            <div class="col-md-6 mb-4">
                <label class="form-lable">Profile Image
                    <i id="image_information" class="fa fa-info-circle" data-toggle="popover" data-trigger="hover"
                       data-placement="top" data-html="true"
                       data-content="
                       Format : <span style='font-weight:bold'>.jpg</span> or <span style='font-weight:bold'>.jpeg</span> or <span style='font-weight:bold'>.gif</span><br>
                       Max Size :  <span style='font-weight:bold'>50KB</span> <br>
                       Dimension : <span style='font-weight:bold'>150 x 150 pixels</span><br>
                       "></i>
                </label>
                <label class="btn btn-default"> {{__('Select Profile Image')}}
                    <input type="file" name="image_new" id="image_new" style="display: none;">
                </label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'image_new') !!}" >
                    {!! '<span id="name-error" class="help-block help-block-error">'.$errors->first('image_new').'</span>' !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6 mb-4">
                <label class="form-lable">Middle Name</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'middle_name') !!}"> {!! Form::text('middle_name', null, array('class'=>'form-control', 'id'=>'middle_name', 'placeholder'=>__('Middle Name'))) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'middle_name') !!}</div>
            </div>
            <div class="col-md-6 mb-4">
                <label class="form-lable">Last Name</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'last_name') !!}"> {!! Form::text('last_name', null, array('class'=>'form-control', 'id'=>'last_name',
                'placeholder'=>__('Last Name').' '.__('(required)')
                )) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'last_name') !!}</div>
            </div>
<?php /*    <div class="col-md-6">
        <label class="form-lable">Father Name</label>
        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'father_name') !!}"> {!! Form::text('father_name', null, array('class'=>'form-control', 'id'=>'father_name', 'placeholder'=>__('Father Name'))) !!}
            {!! APFrmErrHelp::showErrors($errors, 'father_name') !!} </div>
    </div>*/?>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6 mb-4">
                <label class="form-lable">Email</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'email') !!}"> {!! Form::text('email', null, array('class'=>'form-control', 'id'=>'email',
                'placeholder'=>__('Email').' '.__('(required)')
                )) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'email') !!} </div>
            </div>
        <?php /*    <div class="col-md-6 mb-4">
                <label class="form-lable">Password</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'password') !!}"> {!! Form::password('password', array('class'=>'form-control', 'id'=>'password', 'placeholder'=>__('Password'))) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'password') !!} </div>
            </div> */?>
            <div class="col-md-6 mb-4 select-single">
                <label class="form-lable">Gender</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'gender_id') !!}">
                    <?php	$gender_id = old('gender_id', null);
                    ?>	{!! Form::select('gender_id', ['' => __('Select Gender').' '.__('(required)')] + $genders , $gender_id, array('class'=>'form-control select-multiple-gender', 'id'=>'gender_id','style' => 'width:100%')) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'gender_id') !!} </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6 mb-4 select-single">
                <label class="form-lable">Marital Status</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'marital_status_id') !!}">
                    <?php	$marital_status_id = old('marital_status_id', null);
                    ?>	{!! Form::select('marital_status_id', ['' => __('Select Marital Status').' '.__('(required)')] + $maritalStatuses, $marital_status_id, array('class'=>'form-control select-multiple-marital-status', 'id'=>'marital_status_id','style' => 'width:100%')) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'marital_status_id') !!} </div>
            </div>
            <div class="col-md-6 mb-4 select-single">
                <label class="form-lable">Country</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'country_id') !!}">
                    <?php	$country_id = old('country_id', null);
                    ?>	{!! Form::select('country_id', ['' => __('Select Country').' '.__('(required)')] + $countries , $country_id, array('class'=>'form-control select-multiple-country', 'id'=>'country_id','style' => 'width:100%')) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'country_id') !!} </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6 mb-4 select-single">
                <label class="form-lable">State</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'state_id') !!}">
                    <?php	$state_id = old('state_id', null);
                    ?> <span id="default_state_dd">	{!! Form::select('state_id', ['' => __('Select State').' '.__('(required)')] , $state_id, array('class'=>'form-control select-multiple-state', 'id'=>'state_id','style' => 'width:100%')) !!} </span>
                    {!! APFrmErrHelp::showErrors($errors, 'state_id') !!} </div>
            </div>
            <div class="col-md-6 mb-4 select-single">
                <label class="form-lable">City</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'city_id') !!}">
                    <?php	$city_id = old('city_id', null);
                    ?> <span id="default_city_dd"> {!! Form::select('city_id', ['' => __('Select City').' '.__('(required)')] , $city_id, array('class'=>'form-control select-multiple-city', 'id'=>'city_id','style' => 'width:100%')) !!} </span>
                    {!! APFrmErrHelp::showErrors($errors, 'city_id') !!} </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6 mb-4 select-single">
                <label class="form-lable">Nationality</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'nationality_id') !!}">
                    <?php	$nationality_id = old('nationality_id', null);
                    ?>	{!! Form::select('nationality_id', ['' => __('Select Nationality').' '.__('(required)')] + $nationalities , $nationality_id, array('class'=>'form-control select-multiple-nationality', 'id'=>'nationality_id','style' => 'width:100%')) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'nationality_id') !!} </div>
            </div>
            <div class="col-md-6 mb-4">
                <label class="form-lable">Date of Birth</label>
                @php
                if(isset($user->date_of_birth)){
                    $dob = date("M j, Y", strtotime($user->date_of_birth));
                }else{
                    $dob = null;
                }
                @endphp
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'date_of_birth') !!}"> {!! Form::text('date_of_birth', $dob, array('class'=>'form-control datepicker',  'id'=>'date_of_birth',
                'placeholder'=>__('Date of Birth').' '.__('(required)'), 'autocomplete'=>'off')) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'date_of_birth') !!} </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6 mb-4">
                <label class="form-lable">National ID Card Number</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'national_id_card_number') !!}"> {!! Form::text('national_id_card_number', null, array('class'=>'form-control', 'id'=>'national_id_card_number',
                'placeholder'=>__('National ID Card Number').' '.__('(required)')
                )) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'national_id_card_number') !!} </div>
            </div>
            <div class="col-md-6 mb-4">
                <label class="form-lable">Phone No</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'phone') !!}"> {!! Form::tel('phone', null, array('class'=>'form-control', 'id'=>'phone', 'placeholder'=>__('Phone No'))) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'phone') !!} </div>
            </div>
        </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6 mb-4">
                <label class="form-lable">Mobile Number</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'mobile_num') !!}"> {!! Form::tel('mobile_num', null, array('class'=>'form-control', 'id'=>'mobile_num',
                'placeholder'=>__('Mobile Number').' '.__('(required)')
                )) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'mobile_num') !!} </div>
            </div>
            <div class="col-md-6 mb-4 select-single">
                <label class="form-lable">Experience</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'job_experience_id') !!}">
                    <?php	$job_experience_id = old('job_experience_id', null);
                    ?>	{!! Form::select('job_experience_id', ['' => __('Select Experience').' '.__('(required)')] + $jobExperiences , $job_experience_id, array('class'=>'form-control select-multiple-experience', 'id'=>'job_experience_id','style' => 'width:100%')) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'job_experience_id') !!} </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6 mb-4 select-single">
                <label class="form-lable">Career Level</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'job_experience_id') !!}">
                    <?php	$job_experience_id = old('job_experience_id', null);
                    ?>	{!! Form::select('job_experience_id', ['' => __('Select Career Level').' '.__('(required)')] + $careerLevels, $job_experience_id, array('class'=>'form-control select-multiple-career-level', 'id'=>'job_experience_id','style' => 'width:100%')) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'job_experience_id') !!} </div>
            </div>
            <div class="col-md-6 mb-4 select-single">
                <label class="form-lable">Industry</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'industry_id') !!}">
                    <?php	$industry_id = old('industry_id', null);
                    ?>	{!! Form::select('industry_id', ['' => __('Select Industry').' '.__('(required)')] + $industries, $industry_id, array('class'=>'form-control select-multiple-industry', 'id'=>'industry_id','style' => 'width:100%')) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'industry_id') !!} </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6 mb-4 select-single">
                <label class="form-lable">Functional Area</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'functional_area_id') !!}">
                    <?php	$functional_area_id = old('functional_area_id', null);
                    ?>	{!! Form::select('functional_area_id', ['' => __('Select Functional Area').' '.__('(required)')] + $functionalAreas, $functional_area_id, array('class'=>'form-control select-multiple-functional-area', 'id'=>'functional_area_id','style' => 'width:100%')) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'functional_area_id') !!} </div>
            </div>
            <div class="col-md-6 mb-4">
                <label class="form-lable">Whatsapp Number</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'whatsapp') !!}"> {!! Form::tel('whatsapp', null, array('class'=>'form-control', 'id'=>'whatsapp', 'placeholder'=>__('Whatsapp Number With Country Code'))) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'whatsapp') !!} </div>
            </div>
        </div>
    </div>
<?php /*    <div class="col-md-6 mb-4">
        <label class="form-lable">Imessage No</label>
        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'Imessage') !!}"> {!! Form::text('Imessage', null, array('class'=>'form-control', 'id'=>'Imessage', 'placeholder'=>__('Imessage Number'))) !!}
            {!! APFrmErrHelp::showErrors($errors, 'Imessage') !!} </div>
    </div> */?>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6 mb-4">
                <label class="form-lable">Telegram ID</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'telegram') !!}"> {!! Form::text('telegram', null, array('class'=>'form-control', 'id'=>'telegram', 'placeholder'=>__('Telegram ID'))) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'telegram') !!} </div>
            </div>
            <div class="col-md-6 mb-4">
                <label class="form-lable">Skype ID</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'skype') !!}"> {!! Form::text('skype', null, array('class'=>'form-control', 'id'=>'skype', 'placeholder'=>__('Skype ID'))) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'skype') !!} </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 mb-4">
                <label class="form-lable">Current Salary</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'current_salary') !!}"> {!! Form::text('current_salary', null, array('class'=>'form-control', 'id'=>'current_salary',
                'placeholder'=>__('Current Salary').' '.__('(required)')
                )) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'current_salary') !!} </div>
            </div>
            <div class="col-md-4 mb-4">
                <label class="form-lable">Expected Salary</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'expected_salary') !!}"> {!! Form::text('expected_salary', null, array('class'=>'form-control', 'id'=>'expected_salary',
                'placeholder'=>__('Expected Salary').' '.__('(required)')
                )) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'expected_salary') !!} </div>
            </div>
            <div class="col-md-4 mb-4 select-single">
                <label class="form-lable">Salary Currency</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'salary_currency') !!}">
                    <?php	$salary_currency = old('salary_currency', null);
                    ?>	{!! Form::select('salary_currency', ['' => __('Select Salary Currency').' '.__('(required)')] + $currencies, $salary_currency, array('class'=>'form-control select-multiple-salary-currency', 'id'=>'salary_currency','style' => 'width:100%')) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'salary_currency') !!} </div>
            </div>
        </div>
    </div>
    <div class="col-md-12 mb-4">
        <label class="form-lable">Street Address</label>
        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'street_address') !!}"> {!! Form::textarea('street_address', null, array('class'=>'form-control', 'id'=>'street_address',
        'placeholder'=>__('Street Address').' '.__('(required)')
        )) !!}
            {!! APFrmErrHelp::showErrors($errors, 'street_address') !!} </div>
    </div>
    <div class="col-md-12 mb-4">
        <label class="form-lable">Summary</label>
        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'summary') !!}">{!! Form::textarea('summary', $user->getProfileSummary('summary'), array('class'=>'form-control', 'id'=>'summary',
            'placeholder'=>__('Summary')
            )) !!}
            {!! APFrmErrHelp::showErrors($errors, 'summary') !!} </div>
    </div>

    <div class="col-md-12">
        <div class="row">
            <div class="col-md-12 mb-4">
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'is_subscribed') !!}">
                <?php
                $is_checked = 'checked="checked"';
                if (old('is_subscribed', ((isset($user)) ? $user->is_subscribed : 1)) == 0) {
                    $is_checked = '';
                }
                ?>
                  <input type="checkbox" value="1" name="is_subscribed" {{$is_checked}} />
                  {{__('Subscribe to news letter')}}
                  {!! APFrmErrHelp::showErrors($errors, 'is_subscribed') !!}
                </div>
            </div>
            <div class="col-md-12 mb-4">
                <div class="formrow"><button type="submit" class="btn">{{__('Update Profile and Save')}}  <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></button></div>
            </div>
        </div>
    </div>
</div>
{!! Form::close() !!}
<hr>
@push('styles')
<style type="text/css">
    .datepicker>div {
        display: block;
    }
    .iti{
        width: 100%;
    }
    .help-block-error{
        position: absolute;
    }
    .mb-4{margin-bottom:1.5rem!important}
</style>
@endpush
@push('scripts')
<script type="text/javascript">

    $(document).ready(function () {
        initdatepicker();
        $('#current_salary, #expected_salary').priceFormat({prefix:'',thousandsSeparator:',',limit:21,centsLimit:0,clearOnEmpty:true});
        $('#image_information').popover();
        $('#salary_currency').typeahead({
            source: function (query, process) {
                return $.get("{{ route('typeahead.currency_codes') }}", {query: query}, function (data) {
                    data = $.parseJSON(data);
                    return process(data);
                });
            }
        });

        $('#country_id').on('change', function (e) {
            e.preventDefault();
            filterStates(0);
        });
        $(document).on('change', '#state_id', function (e) {
            e.preventDefault();
            filterCities(0);
        });
        filterStates(<?php echo old('state_id', $user->state_id); ?>);

        /*******************************/
        var fileInput = document.getElementById("image_new");
        fileInput.addEventListener("change", function (e) {
            var files = this.files
            showThumbnail(files)
        }, false)
        function showThumbnail(files) {
            $('#thumbnail_img').html('<img src="images/no-image.jpg" />');
            for (var i = 0; i < files.length; i++) {
                var file = files[i]
                var img = new Image();
                img.src = window.URL.createObjectURL( file );
                img.onload = function() {
                    var width = img.naturalWidth,
                        height = img.naturalHeight;
                    var err_message = ''
                    if(width != 150){
                        err_message = "Image width should be 150 px";
                    }
                    if(height != 150){
                        err_message = "Image height should be 150 px";
                    }
                    if(file.size >  50000){
                        err_message = "Image size should be lower than 50 KB";
                    }
                    if (!file.type.match('image/jpeg') && !file.type.match('image/png')) {
                        err_message = "File must be jpeg or jpg or png";
                    }
                    if(err_message != ''){
                        $('#image_new_upload').addClass('has-error msg_cls_for_focus');
                        $('#image_new_upload').html('<span id="name-error" class="help-block help-block-error">' + err_message + '</span>');
                        return false;
                    }
                    $('#image_new_upload').removeClass('has-error msg_cls_for_focus');
                    $('#image_new_upload').html();
                    var reader = new FileReader()
                    reader.onload = (function (theFile) {
                        return function (e) {
                            $('#thumbnail_img').html('<div class="fileattached"><img height="150px" src="' + e.target.result + '" > <div>' + theFile.name + '</div><div class="clearfix"></div></div>');
                        };
                    }(file))
                    var ret = reader.readAsDataURL(file);
                };

            }
        }
        $('.select-multiple-gender').select2({
            placeholder: "{{__('Select Gender').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-marital-status').select2({
            placeholder: "{{__('Select Marital Status').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-country').select2({
            placeholder: "{{__('Select Country').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-state').select2({
            placeholder: "{{__('Select State').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-city').select2({
            placeholder: "{{__('Select City').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-nationality').select2({
            placeholder: "{{__('Select Nationality').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-experience').select2({
            placeholder: "{{__('Select Experience').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-career-level').select2({
            placeholder: "{{__('Select Career Level').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-industry').select2({
            placeholder: "{{__('Select Industry').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-functional-area').select2({
            placeholder: "{{__('Select Functional Area').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-salary-currency').select2({
            placeholder: "{{__('Select Salary Currency').' '.__('(required)')}}",
//            allowClear: true
        });

    });

    function filterStates(state_id)
    {
        var country_id = $('#country_id').val();
        if (country_id != '') {
            $.post("{{ route('filter.lang.states.dropdown') }}", {country_id: country_id, state_id: state_id, _method: 'POST', _token: '{{ csrf_token() }}'})
                    .done(function (response) {
                        $('#default_state_dd').html(response);
                        filterCities(<?php echo old('city_id', $user->city_id); ?>);
                        $('.select-multiple-state').select2({
                            placeholder: "{{__('Select State').' '.__('(required)')}}",
//            allowClear: true
                        });
                    });
        }
    }
    function filterCities(city_id)
    {
        var state_id = $('#state_id').val();
        if (state_id != '') {
            $.post("{{ route('filter.lang.cities.dropdown') }}", {state_id: state_id, city_id: city_id, _method: 'POST', _token: '{{ csrf_token() }}'})
                    .done(function (response) {
                        $('#default_city_dd').html(response);
                        $('.select-multiple-city').select2({
                            placeholder: "{{__('Select City').' '.__('(required)')}}",
//            allowClear: true
                        });
                    });
        }
    }
    function initdatepicker() {
        $(".datepicker").datepicker({
            autoclose: true,
            format: 'M d, yyyy'
        });
    }
<?php /*    var input = document.querySelector("#phone");
     window.intlTelInput(input, {

      utilsScript: "{{ asset('/') }}build/js/utils.js",
    });

    var input2 = document.querySelector("#whatsapp");
     window.intlTelInput(input2, {
      utilsScript: "{{ asset('/') }}build/js/utils.js",
    });

    var input3 = document.querySelector("#mobile_num");
    window.intlTelInput(input3, {
        utilsScript: "{{ asset('/') }}build/js/utils.js",
    });*/?>

<?php /*     var input2 = document.querySelector("#Imessage");
     window.intlTelInput(input2, {
      utilsScript: "{{ asset('/') }}build/js/utils.js",
    }); */?>
</script>
@endpush