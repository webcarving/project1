<div class="modal-body">
    <div class="form-body" id="experience">
        <div class="formrow" id="div_title">
            <label class="form-lable">Experience Title</label>
            <input class="form-control" id="title" placeholder="{{__('Experience Title').' '.__('(required)')}}" name="title" type="text" value="{{(isset($profileExperience)? $profileExperience->title:'')}}">
            <span class="help-block title-error"></span> </div>

        <div class="formrow" id="div_company">
            <label class="form-lable">Company</label>
            <input class="form-control" id="company" placeholder="{{__('Company').' '.__('(required)')}}" name="company" type="text" value="{{(isset($profileExperience)? $profileExperience->company:'')}}">
            <span class="help-block company-error"></span> </div>
        <div class="formrow" id="div_career_levels_id">
            <label class="form-lable"> Level</label>
            <?php
            $career_levels_id=(isset($profileExperience->career_levels_id)) ? $profileExperience->career_levels_id : '';
               ?>
           {!! Form::select('career_levels_id', [''=>__('Select  Level')]+$careerLevels, $career_levels_id, array('class'=>'form-control', 'id'=>'career_levels_id')) !!}
            <span class="help-block career_levels_id-error"></span> </div>

        <div class="formrow" id="div_industry_id">
             <label class="form-lable">Industry</label>
            <?php
            $industry_id=(isset($profileExperience->industry_id)) ? $profileExperience->industry_id : '';
               ?>
           {!! Form::select('industry_id', [''=>__('Select Industry')]+$industries, $industry_id, array('class'=>'form-control', 'id'=>'industry_id')) !!}
            <span class="help-block industry_id-error"></span> </div>

        <div class="formrow" id="div_functional_area_id">
            <label class="form-lable">Functional Area</label>
            <?php
            $functional_area_id=(isset($profileExperience->functional_area_id)) ? $profileExperience->functional_area_id : '';
               ?>
           {!! Form::select('functional_area_id', [''=>__('Select Industry')]+$functionalAreas, $functional_area_id, array('class'=>'form-control', 'id'=>'functional_area_id')) !!}
            <span class="help-block functional_area_id-error"></span> </div>


        <div class="formrow" id="div_experience_country_id">
            <label class="form-lable">Select Country</label>
            <?php
            $country_id = (isset($profileExperience) ? $profileExperience->country_id : $siteSetting->default_country_id);
            ?>
            {!! Form::select('country_id', [''=>__('Select Country')]+$countries, $country_id, array('class'=>'form-control', 'id'=>'experience_country_id')) !!}
            <span class="help-block country_id-error"></span> </div>


        <div class="formrow" id="div_state_id">
            <label class="form-lable">Select State</label>
            <span id="default_state_experience_dd">
                {!! Form::select('state_id', [''=>__('Select State')], null, array('class'=>'form-control', 'id'=>'experience_state_id')) !!}
            </span>
            <span class="help-block state_id-error"></span> </div>

        <div class="formrow" id="div_city_id">
            <label class="form-lable">Select City</label>
            <span id="default_city_experience_dd">
                {!! Form::select('city_id', [''=>__('Select City')], null, array('class'=>'form-control', 'id'=>'city_id')) !!}
            </span>
            <span class="help-block city_id-error"></span> </div>

        <div class="formrow" id="div_date_start">
            <label class="form-lable">Experience Start Date</label>
            <input class="form-control datepicker"  autocomplete="off" id="date_start" placeholder="{{__('Experience Start Date').' '.__('(required)')}}" name="date_start" type="text" value="{{(isset($profileExperience)? $profileExperience->date_start->format('M j, Y'):'')}}">
            <span class="help-block date_start-error"></span> </div>
        <div class="formrow" id="div_date_end">
            <label class="form-lable">Experience End Date</label>
            @php
                $originalDate = NULL;
                $endDate = NULL;

                if(isset($profileExperience) && $profileExperience->date_end !== NULL){
                $originalDate = $profileExperience->date_end;
  $endDate = date("M j, Y", strtotime($originalDate));
}
            @endphp
            <input class="form-control datepicker" autocomplete="off" id="date_end" placeholder="{{__('Experience End Date').' '.__('(required)')}}" name="date_end" type="text" value="{{(isset($profileExperience)? $endDate :'')}}"
                   @php
                       if (isset($profileExperience) && $profileExperience->is_currently_working == 1) {
                        echo "disabled";
                       }
                   @endphp
            >
            <span class="help-block date_end-error"></span> </div>
        <div class="formrow" id="div_is_currently_working">

            <label for="is_currently_working" class="bold">{{__('Is Currently Working?')}}</label>
            <div class="radio-list">
                <?php
                $val_1_checked = '';
                $val_2_checked = 'checked="checked"';

                if (isset($profileExperience) && $profileExperience->is_currently_working == 1) {
                    $val_1_checked = 'checked="checked"';
                    $val_2_checked = '';
                }
                ?>
                <label class="radio-inline"><input id="currently_working" name="is_currently_working" type="radio" value="1" {{$val_1_checked}}> {{__('Yes')}} </label>
                <label class="radio-inline"><input id="not_currently_working" name="is_currently_working" type="radio" value="0" {{$val_2_checked}}> {{__('No')}} </label>
            </div>
            <span class="help-block is_currently_working-error"></span>
        </div>
        <div class="formrow" id="div_description">
             <label class="form-lable">Experience description</label>
            <textarea name="description" class="form-control" id="description" placeholder="{{__('Experience description').' '.__('(required)')}}">{{(isset($profileExperience)? $profileExperience->description:'')}}</textarea>
            <span class="help-block description-error"></span> </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('div#experience #not_currently_working').click(function(){
            $('div#experience #date_end').removeAttr("disabled");
        });
        $('div#experience #currently_working').click(function(){
            $('div#experience #date_end').val('');
            $('div#experience #date_end').prop('disabled','disabled');
        });
    });
</script>