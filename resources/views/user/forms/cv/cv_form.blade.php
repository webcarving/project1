<div class="modal-body">
    <div class="form-body">
        <div class="formrow" id="div_title">
            <label class="form-lable">Title
                <i id="document_information"
                   class="fa fa-info-circle"
                   rel="popover"
                   data-container="#div_title"
                   data-placement="right"
                   data-toggle="popover"
                   data-trigger="hover"
                   data-html="true"
                   data-content="
                   Format : <span style='font-weight:bold'>.doc</span> or <span style='font-weight:bold'>.docx</span> or <span style='font-weight:bold'>.pdf</span><br>
                   Max Size :  <span style='font-weight:bold'>1MB</span>
                   "></i>
            </label>
            <input class="form-control" placeholder="{{__('CV title')}}" name="title" id="title" type="text" value="{{(isset($profileCv)? $profileCv->title:'')}}">
            <span class="help-block title-error"></span> </div>

        @if(isset($profileCv))
        <div class="formrow">
            {{ImgUploader::print_doc("cvs/$profileCv->cv_file", $profileCv->title, $profileCv->title)}}
        </div>
        @endif

        <div class="formrow" id="div_cv_file">
            <input name="cv_file" id="cv_file" type="file" />
            <span id="name-error-cv" class="help-block cv_file-error"></span> </div>

        <div class="formrow" id="div_is_default">
            <label for="is_default" class="bold">{{__('Is Default?')}}</label>
            <div class="radio-list">
                <?php
                $val_1_checked = '';
                $val_2_checked = 'checked="checked"';

                if (isset($profileCv) && $profileCv->is_default == 1) {
                    $val_1_checked = 'checked="checked"';
                    $val_2_checked = '';
                }
                ?>

                <label class="radio-inline"><input id="default" name="is_default" type="radio" value="1" {{$val_1_checked}}> {{__('Yes')}} </label>
                <label class="radio-inline"><input id="not_default" name="is_default" type="radio" value="0" {{$val_2_checked}}> {{__('No')}} </label>
            </div>
            <span class="help-block is_default-error"></span>
        </div>
    </div>
</div>
<script>
    $(function () {
        $('[data-toggle="popover"]').popover()
    })
    $(document).ready(function () {
        var fileInputCV = document.getElementById("cv_file");
        fileInputCV.addEventListener("change", function (e) {
            var files = this.files
            checkfile(files)
        }, false)
        function checkfile(files) {
            for (var i = 0; i < files.length; i++) {
                var file = files[i]
                    var err_message = ''
                    if(file.size >  1000000){
                        err_message = "The cv file may not be greater than 1 MB";
                    }
                    if (!file.type.match('application/pdf') &&
                        !file.type.match('application/msword') &&
                        !file.type.match('application/vnd.openxmlformats-officedocument.wordprocessingml.document')
                    ) {
                        err_message = "Only PDF and DOC and DOCX files can be uploaded.";
                    }
                    if(err_message != ''){
                        $('.cv_file-error').html('<strong>' + err_message + '</strong>');
                        $('#div_cv_file').addClass('has-error');
                        return false;
                    }
                    $('#div_cv_file').removeClass('has-error');
                    $('#div_cv_file span.cv_file-error').html('');
            }
        }
    })
</script>