<div class="modal-body">
    <div class="form-body" id="project">
        <div class="formrow" id="div_name">
            <label class="form-lable">Project Name</label>
            <input class="form-control" id="name" placeholder="{{__('Project Name').' '.__('(required)')}}" name="name" type="text" value="{{(isset($profileProject)? $profileProject->name:'')}}">
            <span class="help-block name-error"></span> </div>

        @if(isset($profileProject))
            <div class="formrow">
                {{ImgUploader::print_image("project_images/thumb/$profileProject->image")}}
            </div>
        @endif

        <div class="formrow" id="div_image">
            <div class="uploadphotobx dropzone needsclick dz-clickable"  id="dropzone"> <i class="fa fa-upload" aria-hidden="true"></i>
                <div class="dz-message" data-dz-message><span>{{__('Drop files here or click to upload Project Image')}}.</span></div>
                <div class="fallback">
                    <input name="image" type="file" />
                </div>
            </div>
            <span class="help-block image-error" id="error_project"></span> </div>


        <div class="formrow" id="div_url">
            <label class="form-lable">Project URL</label>

            <input class="form-control" id="url" placeholder="{{__('Project URL')}}" name="url" type="text" value="{{(isset($profileProject)? $profileProject->url:'')}}">
            <span class="help-block url-error"></span> </div>
        <div class="formrow" id="div_date_start">
            <label class="form-lable">Project Start Date</label>
             @php
                if(isset($profileProject)){
                $originalDate = $profileProject->date_start;
  $startDate = date("M j, Y", strtotime($originalDate));
}
                    @endphp
            <input class="form-control datepicker" id="date_start" placeholder="{{__('Project Start Date').' '.__('(required)')}}" name="date_start" type="text" autocomplete="off" value="{{(isset($profileProject)? $startDate:'')}}">
            <span class="help-block date_start-error"></span> </div>
        <div class="formrow" id="div_date_end">
            <label class="form-lable">Project End Date   </label>
              @php
                  $originalDate = NULL;
                  $endDate = NULL;

                  if(isset($profileProject) && $profileProject->date_end !== NULL){
                    $originalDate = $profileProject->date_end;
                    $endDate = date("M j, Y", strtotime($originalDate));
                  }
              @endphp
            <input class="form-control datepicker" autocomplete="off" id="date_end" placeholder="{{__('Project End Date').' '.__('(required)')}}" name="date_end" type="text" value="{{(isset($profileProject)? $endDate:'')}}"
                    @php
                        if (isset($profileProject) && $profileProject->is_on_going == 1) {
                         echo "disabled";
                        }
                    @endphp

            >
            <span class="help-block date_end-error"></span> </div>

        <div class="formrow" id="div_is_on_going">
            <label for="is_on_going" class="bold">{{__('Is Currently Ongoing?')}}</label>
            <div class="radio-list">
                <?php
                $val_1_checked = '';
                $val_2_checked = 'checked="checked"';

                if (isset($profileProject) && $profileProject->is_on_going == 1) {
                    $val_1_checked = 'checked="checked"';
                    $val_2_checked = '';
                }
                ?>

                <label class="radio-inline"><input id="on_going" name="is_on_going" type="radio" value="1" {{$val_1_checked}}> {{__('Yes')}} </label>
                <label class="radio-inline"><input id="not_on_going" name="is_on_going" type="radio" value="0" {{$val_2_checked}}> {{__('No')}} </label>
            </div>
            <span class="help-block is_on_going-error"></span>
        </div>

        <div class="formrow" id="div_description">
            <label class="form-lable">Project Description</label>
            <textarea name="description" class="form-control" id="description" placeholder="{{__('Project description')}}">{{(isset($profileProject)? $profileProject->description:'')}}</textarea>
            <span class="help-block description-error"></span> </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('div#project #not_on_going').click(function(){
            $('div#project #date_end').removeAttr("disabled");
        });
        $('div#project #on_going').click(function(){
            $('div#project #date_end').val('');
            $('div#project #date_end').prop('disabled','disabled');
        });
    });
</script>