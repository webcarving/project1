<div class="modal-body">
    <div class="form-body">

        <div class="row">
            <div class="col-md-12">
                <div class="row">
                    <div class="col-md-12 mb-2">
                        <div class="formrow" id="div_degree_level_id">
                            <label class="form-lable">Select Degree Level</label>
                            <?php
                            $degree_level_id = (isset($profileEducation) ? $profileEducation->degree_level_id : null);
                            ?>
                            {!! Form::select('degree_level_id', [''=>__('Select Degree Level')]+$degreeLevels, $degree_level_id, array('class'=>'form-control', 'id'=>'degree_level_id')) !!}
                            <span class="help-block degree_level_id-error"></span> </div>
                    </div>
                    <div class="col-md-12 mb-2">
                        <div class="formrow" id="div_degree_type_id">
                            <label class="form-lable">Select Degree Type</label>
                            <?php
                            $degree_type_id = (isset($profileEducation) ? $profileEducation->degree_type_id : null);
                            ?>
                            <span id="degree_types_dd">
                                {!! Form::select('degree_type_id', [''=>__('Select Degree Type')], $degree_type_id, array('class'=>'form-control', 'id'=>'degree_type_id')) !!}
                            </span>
                            <span class="help-block degree_type_id-error"></span> </div>
                    </div>
                    <div class="col-md-12 mb-2">
                        <div class="formrow" id="div_degree_title">
                            <label class="form-lable">Degree Title</label>
                            <input class="form-control" id="degree_title" placeholder="{{__('Degree Title').' '.__('(required)')}}" name="degree_title" type="text" value="{{(isset($profileEducation)? $profileEducation->degree_title:'')}}">
                            <span class="help-block degree_title-error"></span> </div>
                    </div>
                    <div class="col-md-12 mb-2">
                        <div class="formrow" id="div_major_subjects">
                            <label class="form-lable">Select Major Subject</label>
                            <?php
                            $profileEducationMajorSubjectIds = old('major_subjects', $profileEducationMajorSubjectIds);
                            ?>
                            {!! Form::select('major_subjects[]', $majorSubjects, $profileEducationMajorSubjectIds, array('class'=>'form-control select2-multiple', 'id'=>'major_subjects', 'multiple'=>'multiple')) !!}
                            <span class="help-block major_subjects-error"></span> </div>
                    </div>
                    <div class="col-md-12 mb-2">
                        <div class="formrow" id="div_country_id">
                            <label class="form-lable">Select Country</label>
                            <?php
                            $country_id = (isset($profileEducation) ? $profileEducation->country_id : $siteSetting->default_country_id);
                            ?>
                            {!! Form::select('country_id', [''=>__('Select Country')]+$countries, $country_id, array('class'=>'form-control', 'id'=>'education_country_id')) !!}
                            <span class="help-block country_id-error"></span> </div>
                    </div>
                    <div class="col-md-12 mb-2">
                        <div class="formrow" id="div_state_id">
                            <label class="form-lable">Select State</label>
                            <span id="default_state_education_dd">
                            {!! Form::select('state_id', [''=>__('Select State')], null, array('class'=>'form-control', 'id'=>'education_state_id')) !!}
                            </span>
                            <span class="help-block state_id-error"></span> </div>
                    </div>
                    <div class="col-md-12 mb-2">
                        <div class="formrow" id="div_city_id">
                            <label class="form-lable">Select City</label>
                            <span id="default_city_education_dd">
                            {!! Form::select('city_id', [''=>__('Select City')], null, array('class'=>'form-control', 'id'=>'city_id')) !!}
                            </span>
                            <span class="help-block city_id-error"></span> </div>
                    </div>
                    <div class="col-md-12 mb-2">
                        <div class="formrow" id="div_institution">
                            <label class="form-lable">Institution</label>
                            <input class="form-control" id="institution" placeholder="{{__('Institution').' '.__('(required)')}}" name="institution" type="text" value="{{(isset($profileEducation)? $profileEducation->institution:'')}}">
                            <span class="help-block institution-error"></span> </div>
                    </div>
                    <div class="col-md-12 mb-2">
                        <div class="formrow" id="div_date_completion">
                            <label class="form-lable">Select Year</label>
                            <?php
                            $date_completion = (isset($profileEducation) ? $profileEducation->date_completion : null);
                            ?>
                            {!! Form::select('date_completion', [''=>__('Select Year')]+MiscHelper::getEstablishedIn(), $date_completion, array('class'=>'form-control', 'id'=>'date_completion')) !!}
                            <span class="help-block date_completion-error"></span> </div>
                    </div>
                    <div class="col-md-12 mb-2">
                        <div class="formrow" id="div_degree_result">
                            <label class="form-lable">Degree Result</label>
                            <input class="form-control" id="degree_result" placeholder="{{__('Degree Result').' '.__('(required)')}}" name="degree_result" type="text" value="{{(isset($profileEducation)? $profileEducation->degree_result:'')}}">
                            <span class="help-block degree_result-error"></span> </div>
                    </div>
                    <div class="col-md-12 mb-2">
                        <div class="formrow" id="div_result_type_id">
                            <label class="form-lable">Select Result Type</label>
                            <?php
                            $result_type_id = (isset($profileEducation) ? $profileEducation->result_type_id : null);
                            ?>
                            {!! Form::select('result_type_id', [''=>__('Select Result Type')]+$resultTypes, $result_type_id, array('class'=>'form-control', 'id'=>'result_type_id')) !!}
                            <span class="help-block result_type_id-error"></span> </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<script>
    $(document).ready(function(){
        $('.select2-multiple').select2({
            placeholder: "{{__('Select Major Subjects').' '.__('(required)')}}",
        });
    });
</script>