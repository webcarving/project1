@extends('layouts.app')
@section('content')
    <!-- Header start -->
    @include('includes.header')
    <!-- Header end -->
    <!-- Inner Page Title start -->
    @include('includes.inner_page_title', ['page_title'=>__($page_title)])
    <!-- Inner Page Title end -->
    <div class="listpgWraper">
        <div class="container">
        @include('flash::message')
        <!-- Job Header start -->
            <div class="job-header">
                <div class="jobinfo">
                    <div class="row">
                        @if(Auth::check())
                            @include('includes.user_dashboard_menu')
                        @elseif(Auth::guard('company')->check())
                            @include('includes.company_dashboard_menu')
                        @endif
                        <div class="col-md-6 col-sm-8">
                            <!-- Candidate Info -->
                            <div class="candidateinfo">
                                <div class="col-sm-4">
                                    <div class="img-responsive" style="width:150px;height:150px;background:#cbcbcb">{{$user->printUserImage()}}</div>
                                </div>
                                <div class="col-sm-8">
                                    <div class="title">
                                        {{$user->getName()}}
                                        @if((bool)$user->is_immediate_available)
                                            <sup style="font-size:12px; color:#090;">{{__('Immediate Available For Work')}}</sup>
                                        @endif
                                    </div>
                                    <div class="desi">{{$user->getLocation()}}</div>
                                    <div class="loctext"><i class="fa fa-history" aria-hidden="true"></i> {{__('Member Since')}}, {{$user->created_at->format('M j, Y')}}</div>
                                    <div class="loctext"><i class="fa fa-map-marker" aria-hidden="true"></i> {{$user->street_address}}</div>
                                    <div class="clearfix"></div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-8">
                            <!-- Candidate Info -->
                            <div class="candidateinfo">
                                @if(!empty($user->phone))
                                    <div class="loctext"><i class="fa fa-phone" aria-hidden="true"></i> <a href="tel:{{$user->phone}}">{{$user->phone}}</a></div>
                                @endif
                                @if(!empty($user->mobile_num))
                                    <div class="loctext"><i class="fa fa-mobile" aria-hidden="true"></i> <a href="tel:{{$user->mobile_num}}">{{$user->mobile_num}}</a></div>
                                @endif

                                <?php /*                            @if(!empty($user->Imessage))
                                <div class="loctext"><i class="fa fa-comment-o" aria-hidden="true"></i><a href="tel:{{$user->mobile_num}}">{{$user->Imessage}}</a></div>
                                @endif */?>
                                @if(!empty($user->email))
                                    <div class="loctext"><i class="fa fa-envelope" aria-hidden="true"></i> <a href="mailto:{{$user->email}}">{{$user->email}}</a></div>
                                @endif
                                <div class="loctext">
                                    @if(!empty($user->whatsapp))
                                        <a href="https://wa.me/{{str_replace(" ","",str_replace("+","",$user->whatsapp))}}" target="_blanck"><img src="{{asset('/')}}/images/whatsapp.png" height="33"></a>
                                    @endif
                                    @if(!empty($user->telegram))
                                        <a href="https://telegram.me/{{$user->telegram}}" target="_blanck"><img src="{{asset('/')}}/images/t_logo.png" height="33"></a>
                                    @endif
                                    @if(!empty($user->skype))
                                        <a href="skype:{{$user->skype}}?chat" target="_blanck"><img src="{{asset('/')}}/images/skype.png" height="33"></a>
                                    @endif
                                </div>
                            </div>
                        </div>
                        <div class="col-md-9 col-sm-8">
                            <!-- Candidate Contact -->
                            <div class="jobButtons">
                                @if(isset($job) && isset($company))
                                    @if(Auth::guard('company')->check() && Auth::guard('company')->user()->isFavouriteApplicant($user->id, $job->id, $company->id))
                                        <a href="{{route('remove.from.favourite.applicant', [$job_application->id, $user->id, $job->id, $company->id])}}" class="btn"><i class="fa fa-floppy-o" aria-hidden="true"></i> {{__('Short Listed Applicant')}} </a>
                                    @else
                                        <a href="{{route('add.to.favourite.applicant', [$job_application->id, $user->id, $job->id, $company->id])}}" class="btn"><i class="fa fa-floppy-o" aria-hidden="true"></i> {{__('Short List This Applicant')}}</a>
                                    @endif
                                @endif

                                @if(null !== $profileCv)<a href="{{asset('/')}}public/cvs/{{$profileCv->cv_file}}" class="btn"><i class="fa fa-download" aria-hidden="true"></i> {{__('Download CV')}}</a>@endif
<?php /*                                <a data-toggle="modal" href="#contact_applicant" class="btn"><i class="fa fa-envelope" aria-hidden="true"></i> {{__('Send Message')}}</a> */?>

                            </div>
                            <div class="row">

                                <div class="col-md-8">
                                @if($user->getProfileSummary('summary')!='')
                                    <!-- About Employee start -->
                                        <div class="job-header">
                                            <div class="contentbox">
                                                <h3>{{__('About me')}}</h3>
                                                <p>{{$user->getProfileSummary('summary')}}</p>
                                            </div>
                                        </div>
                                @endif
                                <!-- Education start -->
                                    <div class="job-header">
                                        <div class="contentbox">
                                            <h3>{{__('Education')}}</h3>
                                            <div class="" id="education_div"></div>
                                        </div>
                                    </div>

                                    <!-- Experience start -->
                                    <div class="job-header">
                                        <div class="contentbox">
                                            <h3>{{__('Experience')}}</h3>
                                            <div class="" id="experience_div"></div>
                                        </div>
                                    </div>

                                    <!-- Portfolio start -->
                                    <div class="job-header">
                                        <div class="modal fade" id="add_project_modal" role="dialog"></div>
                                        <div class="contentbox">
                                            <h3>{{__('Portfolios')}}</h3>

                                            <div class="" id="projects_div"></div>

                                        </div>
                                    </div>

                                </div>
                                <div class="col-md-4">
                                    <!-- Candidate Detail start -->
                                    <div class="job-header">
                                        <div class="jobdetail">
                                            <h3>{{__('Candidate Detail')}}</h3>
                                            <ul class="jbdetail">

                                                <li class="row">
                                                    <div class="col-md-6 col-xs-6">{{__('Is Email Verified')}}</div>
                                                    <div class="col-md-6 col-xs-6"><span>{{((bool)$user->verified)? 'Yes':'No'}}</span></div>
                                                </li>
                                                <li class="row">
                                                    <div class="col-md-6 col-xs-6">{{__('Immediate Available')}}</div>
                                                    <div class="col-md-6 col-xs-6"><span>{{((bool)$user->is_immediate_available)? 'Yes':'No'}}</span></div>
                                                </li>
                                                @if($user->getAge()!=0)
                                                    <li class="row">
                                                        <div class="col-md-6 col-xs-6">{{__('Age')}}</div>
                                                        <div class="col-md-6 col-xs-6"><span>{{$user->getAge()}} Years</span></div>
                                                    </li>
                                                @endif
                                                @if($user->getGender('gender')!='')
                                                    <li class="row">
                                                        <div class="col-md-6 col-xs-6">{{__('Gender')}}</div>
                                                        <div class="col-md-6 col-xs-6"><span>{{$user->getGender('gender')}}</span></div>
                                                    </li>
                                                @endif
                                                <li class="row">
                                                    <div class="col-md-6 col-xs-6">{{__('Marital Status')}}</div>
                                                    <div class="col-md-6 col-xs-6"><span>{{$user->getMaritalStatus('marital_status')}}</span></div>
                                                </li>
                                                @if($user->getJobExperience('job_experience')!='')
                                                    <li class="row">
                                                        <div class="col-md-6 col-xs-6">{{__('Experience')}}</div>
                                                        <div class="col-md-6 col-xs-6"><span>{{$user->getJobExperience('job_experience')}}</span></div>
                                                    </li>
                                                @endif
                                                @if($user->getCareerLevel('career_level')!='')
                                                    <li class="row">
                                                        <div class="col-md-6 col-xs-6">{{__('Career Level')}}</div>
                                                        <div class="col-md-6 col-xs-6"><span>{{$user->getCareerLevel('career_level')}}</span></div>
                                                    </li>
                                                @endif
                                                @if($user->current_salary!='')
                                                    <li class="row">
                                                        <div class="col-md-6 col-xs-6">{{__('Current Salary')}}</div>
                                                        <div class="col-md-6 col-xs-6"><span class="permanent">{{$user->current_salary}} {{$user->salary_currency}}</span></div>
                                                    </li>
                                                @endif
                                                @if($user->expected_salary!='')
                                                    <li class="row">
                                                        <div class="col-md-6 col-xs-6">{{__('Expected Salary')}}</div>
                                                        <div class="col-md-6 col-xs-6"><span class="freelance">{{$user->expected_salary}} {{$user->salary_currency}}</span></div>
                                                    </li>
                                                @endif
                                            </ul>
                                        </div>
                                    </div>

                                    <!-- Google Map start -->
                                    <div class="job-header">
                                        <div class="jobdetail">
                                            <h3>{{__('Skills')}}</h3>
                                            <div id="skill_div"></div>
                                        </div>
                                    </div>

                                    <div class="job-header">
                                        <div class="jobdetail">
                                            <h3>{{__('Languages')}}</h3>
                                            <div id="language_div"></div>
                                        </div>
                                    </div>
                                    <!-- Contact Company start -->
                                    @if(Auth::guard('company')->check())
                                        <div class="job-header">
                                            <div class="jobdetail">
                                                <h3 id="contact_applicant">{{__($form_title)}}</h3>
                                                <div id="alert_messages"></div>
                                                <?php
                                                $from_name = $from_email = $from_phone = $subject = $message = $from_id = '';
                                                if (isset($company)) {
                                                    $from_name = $company->name;
                                                    $from_email = $company->email;
                                                    $from_phone = $company->mobile_num;
                                                    $from_id = $company->id;
                                                }
                                                if (Auth::guard('company')->check()) {
                                                    $from_name = Auth::guard('company')->user()->name;
                                                    $from_email = Auth::guard('company')->user()->email;
                                                    $from_phone = Auth::guard('company')->user()->phone;
                                                    $from_id = Auth::guard('company')->user()->id;
                                                }
                                                $from_name = old('name', $from_name);
                                                $from_email = old('email', $from_email);
                                                $from_phone = old('phone', $from_phone);
                                                $subject = old('subject');
                                                $message = old('message');
                                                ?>
                                                <form method="post" id="send-applicant-message-form">
                                                    {{ csrf_field() }}
                                                    <input type="hidden" name="to_id" value="{{ $user->id }}">
                                                    <input type="hidden" name="from_id" value="{{ $from_id }}">
                                                    <input type="hidden" name="user_id" value="{{ $user->id }}">
                                                    <input type="hidden" name="user_name" value="{{ $user->getName() }}">
                                                    <div class="formpanel">
                                                        <div class="formrow">
                                                            <input type="text" name="from_name" value="{{ $from_name }}" class="form-control" placeholder="{{__('Full Name')}}">
                                                        </div>
                                                        <div class="formrow">
                                                            <input type="text" name="from_email" value="{{ $from_email }}" class="form-control" placeholder="{{__('Email')}}">
                                                        </div>
                                                        <div class="formrow">
                                                            <input type="text" name="from_phone" value="{{ $from_phone }}" class="form-control" placeholder="{{__('Phone')}}">
                                                        </div>
                                                        <div class="formrow">
                                                            <input type="text" name="subject" value="{{ $subject }}" class="form-control" placeholder="{{__('Subject')}}">
                                                        </div>
                                                        <div class="formrow">
                                                            <textarea name="message" class="form-control" placeholder="Message">{{ $message }}</textarea>
                                                        </div>
                                                        <div class="formrow">
                                                            <input type="button" class="btn" value="{{__('Submit')}}" id="send_applicant_message">
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                            </div>
                        </div>


                    </div>
                </div>

                <!-- Buttons -->

            </div>

        </div>
    </div>
    @include('includes.footer')
@endsection
@push('styles')
    <style type="text/css">
        .formrow iframe {
            height: 78px;
        }
    </style>
@endpush
@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).on('click', '#send_applicant_message', function () {
                var postData = $('#send-applicant-message-form').serialize();
                $.ajax({
                    type: 'POST',
                    url: "{{ route('contact.applicant.message.send') }}",
                    data: postData,
                    //dataType: 'json',
                    success: function (data)
                    {
                        response = JSON.parse(data);
                        var res = response.success;
                        if (res == 'success')
                        {
                            var errorString = '<div role="alert" class="alert alert-success">' + response.message + '</div>';
                            $('#alert_messages').html(errorString);
                            $('#send-applicant-message-form').hide('slow');
                            $(document).scrollTo('.alert', 2000);
                        } else
                        {
                            var errorString = '<div class="alert alert-danger" role="alert"><ul>';
                            response = JSON.parse(data);
                            $.each(response, function (index, value)
                            {
                                errorString += '<li>' + value + '</li>';
                            });
                            errorString += '</ul></div>';
                            $('#alert_messages').html(errorString);
                            $(document).scrollTo('.alert', 2000);
                        }
                    },
                });
            });
            showEducation();
            showProjects();
            showExperience();
            showSkills();
            showLanguages();
        });
        function showProjects()
        {
            $.post("{{ route('show.applicant.profile.projects', $user->id) }}", {user_id: {{$user->id}}, _method: 'POST', _token: '{{ csrf_token() }}'})
                .done(function (response) {
                    $('#projects_div').html(response);
                });
        }
        function showExperience()
        {
            $.post("{{ route('show.applicant.profile.experience', $user->id) }}", {user_id: {{$user->id}}, _method: 'POST', _token: '{{ csrf_token() }}'})
                .done(function (response) {
                    console.log(response);
                    $('#experience_div').html(response);
                });
        }
        function showEducation()
        {
            $.post("{{ route('show.applicant.profile.education', $user->id) }}", {user_id: {{$user->id}}, _method: 'POST', _token: '{{ csrf_token() }}'})
                .done(function (response) {
                    $('#education_div').html(response);
                });
        }
        function showLanguages()
        {
            $.post("{{ route('show.applicant.profile.languages', $user->id) }}", {user_id: {{$user->id}}, _method: 'POST', _token: '{{ csrf_token() }}'})
                .done(function (response) {
                    $('#language_div').html(response);
                });
        }
        function showSkills()
        {
            $.post("{{ route('show.applicant.profile.skills', $user->id) }}", {user_id: {{$user->id}}, _method: 'POST', _token: '{{ csrf_token() }}'})
                .done(function (response) {
                    $('#skill_div').html(response);
                });
        }
        function showProfileProjectEditModal(project_id){
            $("#add_project_modal").modal();
            loadProfileProjectEditForm(project_id);
        }
        function loadProfileProjectEditForm(project_id){
            $.ajax({
                type: "POST",
                url: "{{ route('get.front.profile.project.view.form', $user->id) }}",
                data: {"project_id": project_id, "_token": "{{ csrf_token() }}"},
                datatype: 'json',
                success: function (json) {
                    $("#add_project_modal").html(json.html);
                    createDropZone();
                    initdatepicker();
                }
            });
        }
    </script>
@endpush