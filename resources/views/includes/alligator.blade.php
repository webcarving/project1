<div class="section">
    <div   style="
    padding-left: 0px;
">
        <div class="topsearchwrap">
			<div class="tabswrap">
				<div class="row">
					<div class="col-md-12">
						<ul class="nav nav-tabs" style="float: left">
						  <li class="active" style="
    padding-left: 0px;
    padding-right: 0px;
    padding-top: 0px;
    padding-bottom: 0px;
"><a data-toggle="tab" href="#CareerJet" aria-expanded="true">{{__('CareerJet')}}</a></li>
						  <li class="" style="
    padding-left: 0px;
    padding-right: 0px;
    padding-top: 0px;
    padding-bottom: 0px;
"><a data-toggle="tab" href="#Indeed" aria-expanded="false">{{__('Indeed')}}</a></li>
						  <li class="" style="
    padding-left: 0px;
    padding-right: 0px;
    padding-top: 0px;
    padding-bottom: 0px;
"><a data-toggle="tab" href="#CareerBuilder" aria-expanded="false">{{__('CareerBuilder')}}</a></li>
<li class="" style="
    padding-left: 0px;
    padding-right: 0px;
    padding-top: 0px;
    padding-bottom: 0px;
"><a data-toggle="tab" href="#Monster" aria-expanded="false">{{__('Monster')}}</a></li>
						</ul>
					</div>
				</div>
			
			</div>
				
			<div class="tab-content">
				<div id="CareerJet" class="tab-pane fade active in">
					<div class="srchbx">				
                <!--Categories start-->
               
                <div class="srchint"   style="padding-left: 0;">
                    <ul class="row catelist">
                        @if(isset($careerjet['data']) && count($careerjet['data']))
                        @foreach($careerjet['data'] as $job)
                     <li>
                            <div class="row">
                                <div class="col-md-8 col-sm-8">
                                    
                                    <div class="jobinfo">
                                        <h3><a  target="_blanck" href="{{$job['url']}}" title="{{$job['title']}}">{{$job['title']}}</a></h3>
                                        <div class="companyName"> </div>
                                        <div class="location">
                                            <label class="fulltime" title="Indeed">Career Jet</label>
                                            - <span>{{$job['date']}}</span></div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="listbtn"><a target="_blanck" href="{{$job['url']}}">View Details</a></div>
                                </div>
                            </div>
                            <p>{{str_limit(strip_tags($job['snippet']), 150, '...')}}</p>
                        </li> 
                         @endforeach
                        @endif
                    </ul>
                    <!--Categories end-->
                </div>
            </div>
				</div>
				<div id="Indeed" class="tab-pane fade">
					<div class="srchbx">
                <!--Cities start-->
                
                <div class="srchint" style="padding-left: 0;">
                    <ul class="row catelist">
                          @if(isset($aligator['data']) && count($aligator['data']))
                        @foreach($aligator['data'] as $job)
                     <li>
                            <div class="row">
                                <div class="col-md-8 col-sm-8">
                                    
                                    <div class="jobinfo">
                                        <h3><a  target="_blanck" href="{{$job['url']}}" title="{{$job['title']}}">{{$job['title']}}</a></h3>
                                        <div class="companyName"><a  target="_blanck" href="{{$job['url']}}" title="{{$job['company']}}">{{$job['company']}}</a></div>
                                        <div class="location">
                                            <label class="fulltime" title="Indeed">Indeed</label>
                                            - <span>{{$job['formattedLocation']}}, {{$job['country']}}</span></div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="listbtn"><a target="_blanck" href="{{$job['url']}}">View Details</a></div>
                                </div>
                            </div>
                            <p>{{str_limit(strip_tags($job['snippet']), 150, '...')}}</p>
                        </li> 
                         @endforeach
                        @endif
                    </ul>
                    <!--Cities end-->
                </div>
            </div>
				</div>
				<div id="CareerBuilder" class="tab-pane fade">
					<div class="srchbx">
                <!--Categories start-->
               
                <div class="srchint" style="padding-left: 0;">
                    <ul class="row catelist">					
                        @if(isset($careerbuilder['data']) && count($careerbuilder['data']))
                        @foreach($careerbuilder['data'] as $job)
                     <li>

                            <div class="row"> 
                                <div class="col-md-8 col-sm-8">
                                    
                                    <div class="jobinfo">
                                        <h3><a  target="_blanck" href="{{$job->link}}" title="{{$job->title}}">{{$job->title}}</a></h3>
                                        <div class="companyName"> </div>
                                        <div class="location">
                                            <label class="fulltime" title="Indeed">Career Builder</label>
                                            - <span>{{$job->date}}</span></div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="listbtn"><a target="_blanck" href="{{$job->link}}">View Details</a></div>
                                </div>
                            </div>
                            <p>{{str_limit(strip_tags($job->desc), 150, '...')}}</p>
                        </li> 
                         @endforeach
                        @endif
                    </ul>
                    <!--Categories end-->
                </div>
            </div>				
				</div>
                <div id="Monster" class="tab-pane fade">
                    <div class="srchbx">
                <!--Categories start-->
               
                <div class="srchint" style="padding-left: 0;">
                    <ul class="row catelist">                   
                       @if(isset($monster['data']) && count($monster['data']))
                        @foreach($monster['data'] as $job)
                     <li>

                            <div class="row"> 
                                <div class="col-md-8 col-sm-8">
                                    
                                    <div class="jobinfo">
                                        <h3><a  target="_blanck" href="{{$job->link}}" title="{{$job->title}}">{{$job->title}}</a></h3>
                                        <div class="companyName"> </div>
                                        <div class="location">
                                            <label class="fulltime" title="Indeed">Monster</label>
                                            - <span>{{$job->date}}</span></div>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="col-md-4 col-sm-4">
                                    <div class="listbtn"><a target="_blanck" href="{{$job->link}}">View Details</a></div>
                                </div>
                            </div>
                            <p>{{str_limit(strip_tags($job->desc), 150, '...')}}</p>
                        </li> 
                         @endforeach
                        @endif
                    </ul>
                    <!--Categories end-->
                </div>
            </div>              
                </div>
			</div>
			

            

            

            
        </div>
    </div>
</div>