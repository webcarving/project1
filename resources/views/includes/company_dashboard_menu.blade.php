<div class="col-md-3 col-sm-4">
    <ul class="usernavdash">
        @php
        if(isset($active)){ 
        }else{ 
            $active='';
        } 
        @endphp
        <li class="{{($active=="index")?'active':''}}"><a href="{{route('company.home')}}"><i class="fa fa-tachometer" aria-hidden="true"></i> {{__('Dashboard')}}</a></li>
        <li class="{{($active=="companyProfile")?'active':''}}"><a href="{{ route('company.profile') }}"><i class="fa fa-user" aria-hidden="true"></i> {{__('Edit Company Profile')}}</a></li>
        <li class="{{($active=="companyDetail")?'active':''}}"><a  href="{{ route('company.detail', Auth::guard('company')->user()->slug) }}"><i class="fa fa-user" aria-hidden="true"></i> {{__('View Company Public Profile')}}</a></li>
        <li class="{{($active=="jobpost")?'active':''}}"><a href="{{ route('post.job') }}"><i class="fa fa-desktop" aria-hidden="true"></i> {{__('Post Job')}}</a></li>
        <li class="{{($active=="postedJobs")?'active':''}}"><a href="{{ route('posted.jobs') }}"><i class="fa fa-desktop" aria-hidden="true"></i> {{__('Company Jobs')}}</a></li>

        <li class="{{($active=="companyMessages")?'active':''}}"><a href="{{route('company.messages')}}"><i class="fa fa-envelope-o" aria-hidden="true"></i> {{__('Company Messages')}}</a></li>
        <li class="{{($active=="companyFollowers")?'active':''}}"><a href="{{route('company.followers')}}"><i class="fa fa-user-o" aria-hidden="true"></i> {{__('Company Followers')}}</a></li>
        <li class="{{($active=="public-profile2")?'active':''}}"><a href="{{ route('company.logout') }}" onclick="event.preventDefault(); document.getElementById('logout-form').submit();"><i class="fa fa-sign-out" aria-hidden="true"></i> {{__('Logout')}}</a>
            <form id="logout-form" action="{{ route('company.logout') }}" method="POST" style="display: none;">{{ csrf_field() }}</form>
        </li>
    </ul>
    <div class="row">
        <div class="col-md-12">{!! $siteSetting->dashboard_page_ad !!}</div>
    </div>
</div>