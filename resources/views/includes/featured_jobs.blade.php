<div class="section greybg">
    <div class="container"> 
        <!-- title start -->
        <div class="titleTop">
            <div class="subtitle">{{__('Here You Can See')}}</div>
            <h3>{{__('Featured')}} <span>{{__('Jobs')}}</span></h3>
        </div>
        <!-- title end -->
        <!--Featured Job start-->
        <ul class="jobslist row">
            @if(isset($featuredJobs) && count($featuredJobs))
                @php
                    $numOfCols = 2;
                    $rowCount = 0;
                    $bootstrapColWidth = 12 / $numOfCols;
                @endphp
            <li class="col-md-12">
                <ul>
            @foreach($featuredJobs as $featuredJob)
            <?php $company = $featuredJob->getCompany(); ?>
            @if(null !== $company)
            <!--Job start-->
            <li class="col-md-6">
                <div class="jobint">
                    <div class="row">
                        <div class="col-md-2 col-sm-2">
                            <a href="{{route('job.detail', [$featuredJob->slug])}}" title="{{$featuredJob->title}}">
                                {{$company->printCompanyImage()}}
                            </a>
                        </div>
                        <div class="col-md-7 col-sm-7">
                            <h4><a href="{{route('job.detail', [$featuredJob->slug])}}" title="{{$featuredJob->title}}">{{$featuredJob->title}}</a></h4>
                            <div class="company"><a href="{{route('company.detail', $company->slug)}}" title="{{$company->name}}">{{$company->name}}</a></div>
                            <div class="jobloc">
                                <label class="fulltime" title="{{$featuredJob->getJobType('job_type')}}">{{$featuredJob->getJobType('job_type')}}</label> - <span>{{$featuredJob->getCity('city')}}</span></div>
                        </div>
                        <div class="col-md-3 col-sm-3"><a href="{{route('job.detail', [$featuredJob->slug])}}" class="applybtn">{{__('View Detail')}}</a></div>
                    </div>
                </div>
            </li>
            @php $rowCount++; @endphp
            @if($rowCount % $numOfCols == 0)
                </ul>
                </li>
                <li class="col-md-12">
                    <ul>
            @endif
            <!--Job end-->
            @endif
            @endforeach
            @endif
                    </ul>
                </li>
        </ul>
        <!--Featured Job end--> 
        @if(isset($featuredJobs) && count($featuredJobs))
         @else 
        <center><h3> No Data Found</h3></center>
            @endif
        <!--button start-->
        <div class="viewallbtn"><a href="{{route('job.list', ['is_featured'=>1,'country_id[]'=>$country_id])}}">{{__('View All Featured Jobs')}}</a></div>
        <!--button end--> 
    </div>
</div>