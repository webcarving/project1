<style>
    .select-single .select2-selection__rendered {
        line-height: 46px !important;
    }
    .select-single .select2-container .select2-selection--single {
        height: 50px !important;
    }
    .select-single .select2-selection__arrow {
        height: 49px !important;
    }
</style>
<!-- Modal -->
<div class="modal fade" id="AddSkill" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <form class="form" id="add_profile_skill" method="POST" action="{{ route('addskill.front.job') }}">{{ csrf_field() }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">{{__('Add Skill')}}</h4>
                </div>
                <div class="modal-body">
                    <div class="formrow">
                        <label class="form-lable">Skill</label>

                        <div class="input-group control-group after-add-more-skill" style="font-size:14px !important; font-weight: 400 !important;">
                            {!! Form::text('skill[]', null, array('class'=>'form-control', 'placeholder'=>__('Skill'))) !!}
                            <div class="input-group-btn" style="font-size:14px !important; font-weight: 400 !important;">
                                <button class="btn btn-success add-more-skill" type="button" style="font-size:14px !important; font-weight: 400 !important;"><i class="glyphicon glyphicon-plus"></i> Add</button>
                            </div>
                        </div>

                        <!-- Copy Fields -->
                        <div class="copy-skill hide">
                            <div class="control-group input-group" style="margin-top:10px;font-size:14px !important; font-weight: 400 !important;">
                                {!! Form::text('skill[]', null, array('class'=>'form-control', 'placeholder'=>__('Skill'))) !!}
                                <div class="input-group-btn" style="font-size:14px !important; font-weight: 400 !important;">
                                    <button class="btn btn-danger remove" type="button" style="font-size:14px !important; font-weight: 400 !important;background-color:#d9534f; border:#d43f3a"><i class="glyphicon glyphicon-remove"></i> Remove</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div id="message_skill" class="text-center"></div>
                    <br>
                    <button type="button" class="btn btn-large btn-primary" onClick="submitAddSkillForm();">{{__('Add')}}</button>
                </div>
            </form>
        </div>

    </div>
</div>
<div class="modal fade" id="AddBenefit" role="dialog">
    <div class="modal-dialog">

        <!-- Modal content-->
        <div class="modal-content">
            <form class="form" id="add_profile_benefit" method="POST" action="{{ route('addbenefit.front.job') }}">{{ csrf_field() }}
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title">{{__('Add Benefit')}}</h4>
                </div>
                <div class="modal-body">
                    <div class="formrow">
                        <label class="form-lable">Benefit</label>

                        <div class="input-group control-group after-add-more-benefit" style="font-size:14px !important; font-weight: 400 !important;">
                            {!! Form::text('benefit[]', null, array('class'=>'form-control', 'placeholder'=>__('Benefit'))) !!}
                            <div class="input-group-btn" style="font-size:14px !important; font-weight: 400 !important;">
                                <button class="btn btn-success add-more-benefit" type="button" style="font-size:14px !important; font-weight: 400 !important;"><i class="glyphicon glyphicon-plus"></i> Add</button>
                            </div>
                        </div>

                        <!-- Copy Fields -->
                        <div class="copy-benefit hide">
                            <div class="control-group input-group" style="margin-top:10px;font-size:14px !important; font-weight: 400 !important;">
                                {!! Form::text('benefit[]', null, array('class'=>'form-control', 'placeholder'=>__('Benefit'))) !!}
                                <div class="input-group-btn" style="font-size:14px !important; font-weight: 400 !important;">
                                    <button class="btn btn-danger remove" type="button" style="font-size:14px !important; font-weight: 400 !important;background-color:#d9534f; border:#d43f3a"><i class="glyphicon glyphicon-remove"></i> Remove</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <div id="message_benefit" class="text-center"></div>
                    <br>
                    <button type="button" class="btn btn-large btn-primary" onClick="submitAddBenefitForm();">{{__('Add')}}</button>
                </div>
            </form>
        </div>

    </div>
</div>
<h5>{{__('Job Details')}}</h5>
@if(isset($job))
{!! Form::model($job, array('method' => 'put', 'route' => array('update.front.job', $job->id), 'class' => 'form')) !!}
{!! Form::hidden('id', $job->id) !!}
@php $expiry_date = date("M j, Y", strtotime($job->expiry_date)) @endphp
@else
{!! Form::open(array('method' => 'post', 'route' => array('store.front.job'), 'class' => 'form')) !!}
{!! $expiry_date = null !!}
@endif

<script type="text/javascript">
var allcountry_list=[];
</script>
<div class="row">  
    <div class="col-md-12">
         <label class="form-lable">Job Title</label>
         
        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'title') !!}"> {!! Form::text('title', null, array('class'=>'form-control', 'id'=>'title',
        'placeholder'=>__('Job Title').' '.__('(required)')
        )) !!}
            {!! APFrmErrHelp::showErrors($errors, 'title') !!} </div>
    </div>
    <div class="col-md-12">
        <label class="form-lable">Job Description</label>
        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'description') !!}"> {!! Form::textarea('description', null, array('class'=>'form-control', 'id'=>'description',
        'placeholder'=>__('Job Description').' '.__('(required)')
        )) !!}
            {!! APFrmErrHelp::showErrors($errors, 'description') !!} </div>
    </div>
    <div class="col-md-12">
        <label class="form-lable">Skills</label>
        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'skills') !!}">
            <?php
            $skills = old('skills', $jobSkillIds);
            ?>
            <div class="row">
                <div class="col-md-11">
                {!! Form::select('skills[]', $jobSkills, $skills, array('class'=>'form-control select-multiple-skill', 'id'=>'skills', 'multiple'=>'multiple')) !!}
                {!! APFrmErrHelp::showErrors($errors, 'skills') !!}
                </div>
                <div class="col-md-1">
                    <button type="button" class="btn btn-primary btn-sm" style="font-size:12px !important; padding:5px 10px !important; border-radius: 3px !important;" data-toggle="modal" data-target="#AddSkill">+</button>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <label class="form-lable">Benefits</label>
        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'benefits') !!}">
            <?php
            $benefits = old('benefits', $jobBenefitIds);
            ?>
            <div class="row">
                <div class="col-md-11">
                    {!! Form::select('benefits[]', $jobBenefits, $benefits, array('class'=>'form-control select-multiple-benefit', 'id'=>'benefits', 'multiple'=>'multiple')) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'benefits') !!}
                </div>
                <div class="col-md-1">
                    <button type="button" class="btn btn-primary btn-sm" style="font-size:12px !important; padding:5px 10px !important; border-radius: 3px !important;" data-toggle="modal" data-target="#AddBenefit">+</button>
                </div>
            </div>
        </div>
    </div>
<?php /*    <div class="col-md-4">
        <label class="form-lable">Country</label>
        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'country_id') !!}" id="country_id_div"> {!! Form::select('country_id', ['' => __('Select Country')]+$countries, old('country_id', (isset($job))? $job->country_id:$siteSetting->default_country_id), array('class'=>'form-control', 'id'=>'country_id')) !!}
            {!! APFrmErrHelp::showErrors($errors, 'country_id') !!} </div>
    </div>
    <div class="col-md-4">
        <label class="form-lable">State</label>
        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'state_id') !!}" id="state_id_div"> <span id="default_state_dd"> {!! Form::select('state_id', ['' => __('Select State')], null, array('class'=>'form-control', 'id'=>'state_id')) !!} </span> {!! APFrmErrHelp::showErrors($errors, 'state_id') !!} </div>
    </div>
    <div class="col-md-4">
        <label class="form-lable">City</label>
        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'city_id') !!}" id="city_id_div"> <span id="default_city_dd"> {!! Form::select('city_id', ['' => __('Select City')], null, array('class'=>'form-control', 'id'=>'city_id')) !!} </span> {!! APFrmErrHelp::showErrors($errors, 'city_id') !!} </div>
    </div> */ ?>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6">
                <label class="form-lable">Salary From</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'salary_from') !!}" id="salary_from_div"> {!! Form::text('salary_from', null, array('class'=>'form-control', 'id'=>'salary_from',
                'placeholder'=>__('Salary From').' '.__('(required)')
                )) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'salary_from') !!} </div>
            </div>
            <div class="col-md-6">
                <label class="form-lable">Salary To</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'salary_to') !!}" id="salary_to_div">
                    {!! Form::text('salary_to', null, array('class'=>'form-control', 'id'=>'salary_to',
                    'placeholder'=>__('Salary To').' '.__('(required)')
                    )) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'salary_to') !!} </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-5 select-single">
<?php /*                <label class="form-lable">Salary Currency</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'salary_currency') !!}" id="salary_currency_div">
                    @php
                    $salary_currency = Request::get('salary_currency', (isset($job))? $job->salary_currency:$siteSetting->default_currency_code);
                    @endphp

                    {!! Form::select('salary_currency', ['' => __('Select Salary Currency')]+$currencies, $salary_currency, array('class'=>'form-control', 'id'=>'salary_currency')) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'salary_currency') !!} </div> */ ?>
                <label class="form-lable">Salary Currency</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'salary_currency') !!}">
                    <?php	$salary_currency = old('salary_currency', array());?>
                        {!! Form::select('salary_currency', ['' => __('Select Salary Currency').' '.__('(required)')]+$currencies, $salary_currency, array('class'=>'form-control select-multiple-salary-currency', 'id'=>'salary_currency')) !!}
                        {!! APFrmErrHelp::showErrors($errors, 'salary_currency') !!} </div>
            </div>
            <div class="col-md-5 select-single">
                <label class="form-lable">Salary Period</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'salary_period') !!}">
                    <?php	$salary_period = old('salary_period', array());
                    ?>	{!! Form::select('salary_period', ['' => __('Select Salary Period').' '.__('(required)')]+$salaryPeriods, $salary_period, array('class'=>'form-control select-multiple-salary-period', 'id'=>'salary_period','style' => 'width:100%')) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'salary_period') !!} </div>
            </div>
            <div class="col-md-2">
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'hide_salary') !!}">
                    {!! Form::label('hide_salary', __('Hide Salary?'), ['class' => 'bold']) !!}
                    <div class="radio-list">
                        <?php
                        $hide_salary_1 = '';
                        $hide_salary_2 = 'checked="checked"';
                        if (old('hide_salary', ((isset($job)) ? $job->hide_salary : 0)) == 1) {
                            $hide_salary_1 = 'checked="checked"';
                            $hide_salary_2 = '';
                        }
                        ?>
                        <label class="radio-inline">
                            <input id="hide_salary_yes" name="hide_salary" type="radio" value="1" {{$hide_salary_1}}>
                            {{__('Yes')}} </label>
                        <label class="radio-inline">
                            <input id="hide_salary_no" name="hide_salary" type="radio" value="0" {{$hide_salary_2}}>
                            {{__('No')}} </label>
                    </div>
                    {!! APFrmErrHelp::showErrors($errors, 'hide_salary') !!} </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6 select-single">
                <label class="form-lable">Career Level</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'career_level_id') !!}">
                    <?php	$career_level_id = old('career_level_id', array());
                    ?>	{!! Form::select('career_level_id', ['' => __('Select Career Level').' '.__('(required)')]+$careerLevels, $career_level_id, array('class'=>'form-control select-multiple-career-level', 'id'=>'career_level_id','style' => 'width:100%')) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'career_level_id') !!} </div>
            </div>

            <div class="col-md-6 select-single">
                <label class="form-lable">Functional Area</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'functional_area_id') !!}">
                    <?php	$functional_area_id = old('functional_area_id', array());
                    ?>	{!! Form::select('functional_area_id', ['' => __('Select Functional Area').' '.__('(required)')]+$functionalAreas, $functional_area_id, array('class'=>'form-control select-multiple-functional-area', 'id'=>'functional_area_id','style' => 'width:100%')) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'functional_area_id') !!} </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6 select-single">
                <label class="form-lable">Job Type</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'job_type_id') !!}">
                    <?php	$job_type_id = old('job_type_id', array());
                    ?>	{!! Form::select('job_type_id', ['' => __('Select Job Type').' '.__('(required)')]+ $jobTypes, $job_type_id, array('class'=>'form-control select-multiple-job-type', 'id'=>'job_type_id','style' => 'width:100%')) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'job_type_id') !!} </div>
            </div>
            <div class="col-md-6 select-single">
                <label class="form-lable">Job Shift</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'job_shift_id') !!}">
                    <?php	$job_shift_id = old('job_shift_id', array());
                    ?>	{!! Form::select('job_shift_id', ['' => __('Select Job Shift').' '.__('(required)')]+$jobShifts, $job_shift_id, array('class'=>'form-control select-multiple-job-shift', 'id'=>'job_shift_id','style' => 'width:100%')) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'job_shift_id') !!} </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6 select-single">
                <label class="form-lable">Number of Positions</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'num_of_positions') !!}">
                    <?php	$num_of_positions = old('num_of_positions', array());
                    ?>	{!! Form::select('num_of_positions', ['' => __('Select Number of Positions').' '.__('(required)')] + MiscHelper::getNumPositions(), $num_of_positions, array('class'=>'form-control select-multiple-number-of-positions', 'id'=>'num_of_positions','style' => 'width:100%')) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'num_of_positions') !!} </div>
            </div>
            <div class="col-md-6 select-single">
                <label class="form-lable">Gender Preference</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'gender_id') !!}">
                    <?php	$gender_id = old('gender_id', array());
                    ?>	{!! Form::select('gender_id', ['' => __('Select Gender Preference').' '.__('(required)')] + $genders, $gender_id, array('class'=>'form-control select-multiple-gender-preference', 'id'=>'gender_id','style' => 'width:100%')) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'gender_id') !!} </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6">
                 <label class="form-lable">Job Expiry Date</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'expiry_date') !!}"> {!! Form::text('expiry_date', $expiry_date, array('class'=>'form-control datepicker', 'id'=>'expiry_date',
                'placeholder'=>__('Job Expiry Date').' '.__('(required)')
                , 'autocomplete'=>'off')) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'expiry_date') !!} </div>
            </div>
            <div class="col-md-6 select-single">
                <label class="form-lable">Required Degree Level</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'degree_level_id') !!}">
                    <?php	$degree_level_id = old('degree_level_id', array());
                    ?>	{!! Form::select('degree_level_id', ['' => __('Select Required Degree Level').' '.__('(required)')] + $degreeLevels, $degree_level_id, array('class'=>'form-control select-multiple-required-degree-level', 'id'=>'degree_level_id','style' => 'width:100%')) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'degree_level_id') !!} </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6 select-single">
                <label class="form-lable">Job Experience</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'job_experience_id') !!}">
                    <?php	$job_experience_id = old('job_experience_id', array());
                    ?>	{!! Form::select('job_experience_id', ['' => __('Select Job Experience').' '.__('(required)')] + $jobExperiences, $job_experience_id, array('class'=>'form-control select-multiple-job-experience', 'id'=>'job_experience_id','style' => 'width:100%')) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'job_experience_id') !!} </div>
            </div>
            <div class="col-md-6">
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'is_freelance') !!}">
                    {!! Form::label('is_freelance', __('Is Freelance?'), ['class' => 'bold']) !!}
                    <div class="radio-list">
                        <?php
                        $is_freelance_1 = '';
                        $is_freelance_2 = 'checked="checked"';
                        if (old('is_freelance', ((isset($job)) ? $job->is_freelance : 0)) == 1) {
                            $is_freelance_1 = 'checked="checked"';
                            $is_freelance_2 = '';
                        }
                        ?>
                        <label class="radio-inline">
                            <input id="is_freelance_yes" name="is_freelance" type="radio" value="1" {{$is_freelance_1}}>
                            {{__('Yes')}} </label>
                        <label class="radio-inline">
                            <input id="is_freelance_no" name="is_freelance" type="radio" value="0" {{$is_freelance_2}}>
                            {{__('No')}} </label>
                    </div>
                    {!! APFrmErrHelp::showErrors($errors, 'is_freelance') !!} </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <label class="form-lable">Language</label>
        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'language') !!}">
            <?php
            $language = old('language', $jobLanguageIds);
            ?>
            {!! Form::select('language[]', $jobLanguages, $language, array('class'=>'form-control select-multiple-language', 'id'=>'language', 'multiple'=>'multiple')) !!}
            {!! APFrmErrHelp::showErrors($errors, 'language') !!} </div>
    </div>

    @php if(!isset($joblocationNew)) $joblocationNew = null; @endphp
 
    <div class="col-md-12 select-single">
        <label class="form-lable">Job Location</label>
        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'job_location_id') !!}">
            <?php	$job_location_id = old('job_location_id', array());
            ?>	{!! Form::select('job_location_id', ['' => __('Select Job Location').' '.__('(required)')] + $Joblocations, $job_location_id, array('class'=>'form-control select-multiple-job-location', 'id'=>'job_location_id','style' => 'width:100%')) !!}
            {!! APFrmErrHelp::showErrors($errors, 'job_location_id') !!} </div>

<?php /*        <label class="form-lable">Job Location</label>
        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'job_location_id') !!}" id="joblocation"> {!! Form::select('job_location_id', ['' => __('Select Job Location').' '.__('(required)')]+$Joblocations, $joblocationNew, array('class'=>'form-control', 'id'=>'job_location_id')) !!}
            {!! APFrmErrHelp::showErrors($errors, 'job_location_id') !!} </div> */ ?>
    </div> 
    <div class="col-md-12" id="country_list"> 
<div id="myDIV"> 
     
    @php 
    foreach ($allcurentlocation  as $key => $value) {
        $list=$value['list'];
        $selected=$value['selected'];
        $name2='locations_'.$key.'[]';
        $name3='locations_'.$key;
     @endphp
     <script type="text/javascript">
         allcountry_list.push({{$key}});
     </script>
      <div id="work_location_{{$key}}" >
    <div class="col-md-2" id="work_location_{{$key}}a">
        <label class="form-lable"><br>{{$value['name']}}</label>
    </div>
   <div class="col-md-10" id="work_location_{{$key}}b">
        <label class="form-lable"></label>
        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'locations') !!}">
            <?php
            $locations = old('locations', $selected);
            
            ?>
            <span id="worklocation_{{$key}}" class="worklocation_{{$key}}"> {!! Form::select($name2, $list, $locations, array('class'=>'form-control select-multiple-job-location', 'id'=>$name3, 'multiple'=>'multiple')) !!}
            </span>
            {!! APFrmErrHelp::showErrors($errors, 'locations') !!} </div>
    </div>
</div>
 @php
    }
    @endphp
   {{--  @for($allcurentlocation  as $key2 => $value2)
        {{$key}}
    @endfor --}}
</div>
    </div> 
  
    <div class="col-md-12"> 
        <div class="formrow">
            <button type="submit" class="btn">{{__('Update Job')}} <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></button>
        </div>
    </div>
</div>
<input type="file" name="image" id="image" style="display:none;" accept="image/*"/>
{!! Form::close() !!}
<hr>
@push('styles')
<style type="text/css">
    .datepicker>div {
        display: block;
    }
</style>
@endpush
@push('scripts')
@include('includes.tinyMCEFront')
<script type="text/javascript">

    function submitAddSkillForm() {
        var form = $('#add_profile_skill');
        var skill_length = $("form :input[name='skill[]']").length;
        var counter = 0;
        var formData = $("#add_profile_skill *").filter(function(index, element){
            if($(element).attr('name') == 'skill[]'){
                counter = counter+1;
                if(counter < skill_length){
                    return $(element).val();
                }
            }else{
                return $(element);
            }
        });
        $.ajax({
            url     : form.attr('action'),
            type    : form.attr('method'),
            data    : formData.serialize(),
            dataType: 'json',
            success : function (resJSON){
//                $ ("#add_skill_modal").html(json.html);
//                showSkills();
                var code = 200;
                $.each(resJSON.message, function (key, value) {
                    $('#message_skill').html('<strong>'+value+'</strong>');
                    $('#message_skill').addClass('text-success');
                });
                $('.after-add-more-skill').nextUntil('div.copy-skill').remove();
                $('.after-add-more-skill > input').val('');
            },
            error: function(json){
/*                if (json.status === 422) {
                    var resJSON = json.responseJSON;
                    $('.help-block').html('');
                    $.each(resJSON.errors, function (key, value) {
                        $('.' + key + '-error').html('<strong>' + value + '</strong>');
                        $('#div_' + key).addClass('has-error');
                    });
                } else {
                    // Error
                    // Incorrect credentials
                    // alert('Incorrect credentials. Please try again.')
                }*/
                var code = json.status;
                var resJSON = json.responseJSON;
                $.each(resJSON.message, function (key, value) {
                    $('#message_skill').html('<strong>'+value+'</strong>');
                    $('#message_skill').addClass('text-danger');
                });
            }
        });
    }
    function submitAddBenefitForm() {
        var form = $('#add_profile_benefit');
        var benefit_length = $("form :input[name='benefit[]']").length;
        var counter = 0;
        var formData = $("#add_profile_benefit *").filter(function(index, element){
            if($(element).attr('name') == 'benefit[]'){
                counter = counter+1;
                if(counter < benefit_length){
                    return $(element).val();
                }
            }else{
                return $(element);
            }
        });
        $.ajax({
            url     : form.attr('action'),
            type    : form.attr('method'),
            data    : formData.serialize(),
            dataType: 'json',
            success : function (resJSON){
//                $ ("#add_benefit_modal").html(json.html);
//                showBenefits();
                var code = 200;
                $.each(resJSON.message, function (key, value) {
                    $('#message_benefit').html('<strong>'+value+'</strong>');
                    $('#message_benefit').addClass('text-success');
                });
                $('.after-add-more-benefit').nextUntil('div.copy-benefit').remove();
                $('.after-add-more-benefit > input').val('');
            },
            error: function(json){
                /*                if (json.status === 422) {
                                    var resJSON = json.responseJSON;
                                    $('.help-block').html('');
                                    $.each(resJSON.errors, function (key, value) {
                                        $('.' + key + '-error').html('<strong>' + value + '</strong>');
                                        $('#div_' + key).addClass('has-error');
                                    });
                                } else {
                                    // Error
                                    // Incorrect credentials
                                    // alert('Incorrect credentials. Please try again.')
                                }*/
                var code = json.status;
                var resJSON = json.responseJSON;
                $.each(resJSON.message, function (key, value) {
                    $('#message_benefit').html('<strong>'+value+'</strong>');
                    $('#message_benefit').addClass('text-danger');
                });
            }
        });
    }
    function address(value) {
       var name=$("#job_location_id option[value="+value+"]").text(); 
        $("<div id='work_location_"+value+"'><div class='col-md-2' id='work_location_"+value+"a'><label class='form-lable'><br>"+name+"</label></div><div class='col-md-10' id='work_location_"+value+"b'><label class='form-lable'></label><div class='formrow '><span id='worklocation_"+value+"' class='worklocation_"+value+"'><select class='form-control select-multiple-job-location' id='locations_"+value+"' multiple='multiple' name='locations_"+value+"[]'></select></span> </div></div></div>").appendTo('#myDIV');
         $('.select-multiple-job-location').select2({
            placeholder: "{{__(' ')}}",
//            allowClear: true
        });
    }

    $(document).ready(function () {
        $('#salary_from, #salary_to').priceFormat({prefix:'',thousandsSeparator:',',limit:21,centsLimit:0,clearOnEmpty:true});
        $("#AddSkill").on('show.bs.modal', function(){
            $('#message_skill').html('');
            $('#message_skill').removeClass('text-success');
            $('#message_skill').removeClass('text-danger');
        });
        $("#AddBenefit").on('show.bs.modal', function(){
            $('#message_benefit').html('');
            $('#message_benefit').removeClass('text-success');
            $('#message_benefit').removeClass('text-danger');
        });
         $('.select-multiple-skill').select2({
            placeholder: "{{__('Select Skills').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-benefit').select2({
            placeholder: "{{__('Select Benefits')}}",
//            allowClear: true
        });
        $('.select-multiple-language').select2({
            placeholder: "{{__('Select Language').' '.__('(required)')}}",
//            allowClear: true
        }); 
        $('.select-multiple-job-location').select2({
            placeholder: "{{__(' ')}}",
//            allowClear: true
        });
        $('.select-multiple-salary-currency').select2({
            placeholder: "{{__('Select Salary Currency').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-salary-period').select2({
            placeholder: "{{__('Select Salary Period').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-career-level').select2({
            placeholder: "{{__('Select Career Level').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-functional-area').select2({
            placeholder: "{{__('Select Functional Area').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-job-type').select2({
            placeholder: "{{__('Select Job Type').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-job-shift').select2({
            placeholder: "{{__('Select Job Shift').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-number-of-positions').select2({
            placeholder: "{{__('Select Number of Positions').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-gender-preference').select2({
            placeholder: "{{__('Select Gender Preference').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-required-degree-level').select2({
            placeholder: "{{__('Select Required Degree Level').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-job-experience').select2({
            placeholder: "{{__('Select Job Experience').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-job-location').select2({
            placeholder: "{{__('Select Job Location').' '.__('(required)')}}",
//            allowClear: true
        });
        $(".datepicker").datepicker({
            autoclose: true,
            format: 'M d, yyyy',
        });
        $('#country_id').on('change', function (e) {
            e.preventDefault();
            filterLangStates(0);
        });
        $(document).on('change', '#state_id', function (e) {
            e.preventDefault();
            filterLangCities(0);
        });
        $('#multi_location , #multi_location2 , #multi_location3 , #multi_location4, #multi_location5').on('change', function (e) { 
            var id=(e.target.id);  
            var x = document.getElementById(id).checked;
            var val=$("#"+id).val();
            if(x==true){ 
                multiLocation(val);
            }else{
               // document.getElementById("work_location_"+val).style.visibility = "hidden"; job_location_id
                document.getElementById("work_location_"+val+"a").style.visibility = "hidden";
                 document.getElementById("work_location_"+val+"b").style.visibility = "hidden";
                 document.getElementById("work_location_"+val+"b").style.height = "0px";
                        document.getElementById("work_location_"+val+"a").style.height = "0px";
                $(".worklocation_"+val+" .select2-selection__rendered").empty();
            } 
        });
        $('#job_location_id').on('change', function (e) {  
            var val=$("#job_location_id").val();
            if(val!=''){ 
                var found = allcountry_list.find(function(element) {
                    return element == val;
                });
                if(found){
                    var elem = document.getElementById('work_location_'+val);
                    elem.remove();   
                }else{
                    address(val); 
                    allcountry_list.push(val);
                    multiLocation(val);
                }
            }  
        }); 
        filterLangStates(<?php echo old('state_id', (isset($job)) ? $job->state_id : 0); ?>);

        $(".add-more-skill").click(function(){
            var html = $(".copy-skill").html();
            $(".after-add-more-skill").after(html);
        });

        $(".add-more-benefit").click(function(){
            var html = $(".copy-benefit").html();
            $(".after-add-more-benefit").after(html);
        });


        $("body").on("click",".remove",function(){
            $(this).parents(".control-group").remove();
        });
    });

    function filterLangStates(state_id)
    {
        var country_id = $('#country_id').val();
        if (country_id != '') {
            $.post("{{ route('filter.lang.states.dropdown') }}", {country_id: country_id, state_id: state_id, _method: 'POST', _token: '{{ csrf_token() }}'})
                    .done(function (response) {
                        $('#default_state_dd').html(response);
                        filterLangCities(<?php echo old('city_id', (isset($job)) ? $job->city_id : 0); ?>);
                    });
        }
    }
    function filterLangCities(city_id)
    {
        var state_id = $('#state_id').val();
        if (state_id != '') {
            $.post("{{ route('filter.lang.cities.dropdown') }}", {state_id: state_id, city_id: city_id, _method: 'POST', _token: '{{ csrf_token() }}'})
                    .done(function (response) {
                        $('#default_city_dd').html(response);
                    });
        }
    }
    function multiLocation(country_id)
    {  
        if (country_id != '') {
            $.post("{{ route('filter-lang-addressmulti-dropdown') }}", {country_id: country_id,  _method: 'POST', _token: '{{ csrf_token() }}'})
                    .done(function (response) { 
                        $('#locations_'+country_id).html(response);
                        //document.getElementById("work_location_"+country_id).style.visibility = "visible"; 
                       document.getElementById("work_location_"+country_id+"a").style.visibility = "visible"; 
                        document.getElementById("work_location_"+country_id+"b").style.visibility = "visible";
                        document.getElementById("work_location_"+country_id+"b").style.height = "auto";
                        document.getElementById("work_location_"+country_id+"a").style.height = "auto";  
                    });
        }
    }
</script> 
@endpush
  
  