@extends('layouts.app')
@section('content') 
<!-- Header start --> 
@include('includes.header') 
<!-- Header end --> 
<!-- Inner Page Title start --> 
@include('includes.inner_page_title', ['page_title'=>__('Apply on Job')])
<style>
    .select-single .select2-selection__rendered {
        line-height: 46px !important;
    }
    .select-single .select2-container .select2-selection--single {
        height: 50px !important;
    }
    .select-single .select2-selection__arrow {
        height: 49px !important;
    }
</style>
<!-- Inner Page Title end -->
<div class="listpgWraper">
    <div class="container"> @include('flash::message')
        <div class="row">
            <div class="col-md-12">
                <div class="userccount">
                    <div class="formpanel"> {!! Form::open(array('method' => 'post', 'route' => ['post.apply.job', $job_slug])) !!} 
                        <!-- Job Information -->
                        <h5>{{$job->title}}</h5>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-6 mb-4 select-single">
                                        <label class="form-lable">CV</label>
                                        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'cv_id') !!}">
                                            <?php	$cv_id = old('cv_id', null);
                                            ?>	{!! Form::select('cv_id', ['' => __('Select CV').' '.__('(required)')] + $myCvs, $cv_id, array('class'=>'form-control select-multiple-cvs', 'id'=>'cv_id','style' => 'width:100%')) !!}
                                            {!! APFrmErrHelp::showErrors($errors, 'cv_id') !!} </div>
                                    </div>
                                    <div class="col-md-6 mb-4">
                                        <div style="padding-top:15px;padding-bottom:15px"></div>
                                        <div class="formrow">
                                            @if (count($myCvs) > 0)
                                                <button id="add_cv_btn" type="button" disabled class="btn btn-disabled"> {{__('Add CV')}} </button>
                                            @else
                                                <button id="add_cv_btn" type="button" onclick="showProfileCvModal();" class="btn btn-primary"> {{__('Add CV')}} </button>
                                            @endif
                                        </div>
                                        <div class="modal fade" id="add_cv_modal" role="dialog"></div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12">
                                <div class="row">
                                    <div class="col-md-4">
                                        <label class="form-lable">Current Salary</label>
                                        <div class="formrow{{ $errors->has('current_salary') ? ' has-error' : '' }}"> {!! Form::text('current_salary', null, array('class'=>'form-control', 'id'=>'current_salary',
                                        'placeholder'=>__('Current salary').' ('.$job->getSalaryPeriod('salary_period').')'.' '.'*')) !!}
                                            @if ($errors->has('current_salary')) <span class="help-block"> <strong>{{ $errors->first('current_salary') }}</strong> </span> @endif </div>
                                    </div>
                                    <div class="col-md-4">
                                        <label class="form-lable">Expected Salary</label>
                                        <div class="formrow{{ $errors->has('expected_salary') ? ' has-error' : '' }}"> {!! Form::text('expected_salary', null, array('class'=>'form-control', 'id'=>'expected_salary',
                                        'placeholder'=>__('Expected salary').' ('.$job->getSalaryPeriod('salary_period').')'.' '.'*')) !!}
                                            @if ($errors->has('expected_salary')) <span class="help-block"> <strong>{{ $errors->first('expected_salary') }}</strong> </span> @endif </div>
                                    </div>
                                    <div class="col-md-4 select-single">
                                        <label class="form-lable">Salary Currency</label>
                                        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'salary_currency') !!}">
                                            <?php	$salary_currency = old('salary_currency', null);
                                            ?>	{!! Form::select('salary_currency', ['' => __('Select Salary Currency').' '.__('(required)')] + $currencies, $salary_currency, array('class'=>'form-control select-multiple-salary-currency', 'id'=>'salary_currency', 'style' => 'width:100%')) !!}
                                            {!! APFrmErrHelp::showErrors($errors, 'salary_currency') !!} </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <br>
                        <input type="submit" class="btn" value="{{__('Apply on Job')}}">
                        {!! Form::close() !!} </div>
                </div>
            </div>
        </div>
    </div>
</div>
@include('includes.footer')
@endsection
@push('scripts') 
<script>
    $(document).ready(function () {
        $('#current_salary, #expected_salary').priceFormat({prefix:'',thousandsSeparator:',',limit:21,centsLimit:0,clearOnEmpty:true});
        $('.select-multiple-cvs').select2({
            placeholder: "{{__('Select Gender').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-salary-currency').select2({
            placeholder: "{{__('Select Salary Currency').' '.__('(required)')}}",
//            allowClear: true
        });

<?php /*        $('#salary_currency').typeahead({
            source: function (query, process) {
                return $.get("{{ route('typeahead.currency_codes') }}", {query: query}, function (data) {
                    data = $.parseJSON(data);
                    return process(data);
                });
            }
        });*/ ?>
    });
    function showProfileCvModal(){
        $("#add_cv_modal").modal();
        loadProfileCvForm();
    }
    function loadProfileCvForm(){
        $.ajax({
            type: "POST",
            url: "{{ route('get.front.profile.cv.form', $user->id) }}",
            data: {"_token": "{{ csrf_token() }}"},
            datatype: 'json',
            success: function (json) {
                $("#add_cv_modal").html(json.html);
                $("#add_cv_btn").replaceWith('<button id="add_cv_btn" type="button" disabled class="btn btn-disabled"> {{__("Add CV")}} </button>');
            }
        });
    }
    function submitProfileCvForm() {
        var form = $('#add_edit_profile_cv');
        var formData = new FormData();
        formData.append("id", $('#id').val());
        formData.append("_token", $('input[name=_token]').val());
        formData.append("title", $('#title').val());
        formData.append("is_default", $('input[name=is_default]:checked').val());
        if (document.getElementById("cv_file").value != "") {
            formData.append("cv_file", $('#cv_file')[0].files[0]);
        }
        //form.attr('method'),
        $.ajax({
            url     : form.attr('action'),
            type    : 'POST',
            data    : formData,
            dataType: 'json',
            contentType: false,
            processData: false,
            success : function (json){
                $ ("#add_cv_modal").html(json.html);
                showSelectionCvs();
            },
            error: function(json){
                if (json.status === 422) {
                    var resJSON = json.responseJSON;
                    $('.help-block').html('');
                    $.each(resJSON.errors, function (key, value) {
                        $('.' + key + '-error').html('<strong>' + value + '</strong>');
                        $('#div_' + key).addClass('has-error');
                    });
                } else {
                    // Error
                    // Incorrect credentials
                    // alert('Incorrect credentials. Please try again.')
                }
            }
        });
    }
    function showSelectionCvs()
    {
        $.post("{{ route('show.front.profile.cvs.select', $user->id) }}", {user_id: {{$user->id}}, _method: 'POST', _token: '{{ csrf_token() }}'})
            .done(function (response) {
                $('#cv_id').html(response);
            });
    }

</script>

@endpush