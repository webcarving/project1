@extends('layouts.app')
@section('content') 
<!-- Header start --> 
@include('includes.header') 
<!-- Header end --> 
<!-- Inner Page Title start --> 
@include('includes.inner_page_title', ['page_title'=>__('Job Detail')]) 
<!-- Inner Page Title end -->
@php
$company = $job->getCompany();
@endphp
<div class="listpgWraper">
    <div class="container">
        @include('flash::message')
        <!-- Job Detail start -->
        <div class="row">
            <div class="col-md-8">
				 <!-- Job Header start -->
                <div class="job-header">
                    <div class="jobinfo">
                        <h2>{{$job->title}} - {{$company->name}}</h2>
                        <div class="ptext">{{__('Date Posted')}}: {{$job->created_at->format('M j, Y')}}</div>
                        @if(!(bool)$job->hide_salary)
                            <div class="salary">{{__('Monthly Salary')}}: <strong>{{$job->salary_from.' '.$job->salary_currency}} - {{$job->salary_to.' '.$job->salary_currency}}</strong></div>
                        @endif
                    </div>
                    <div class="jobButtons">
                        @if(Auth::guard('company')->user())
                            <a href="{{route('edit.front.job', [$job->id])}}" class="btn apply"><i class="fa fa-edit" aria-hidden="true"></i>{{__('Edit')}}</a>
                        @else
                            @if($job->isJobExpired())
                                <span class="jbexpire"><i class="fa fa-paper-plane" aria-hidden="true"></i> {{__('Job is expired')}}</span>
                            @elseif(Auth::check() && Auth::user()->isAppliedOnJob($job->id))
                                <a href="javascript:;" class="btn apply"><i class="fa fa-paper-plane" aria-hidden="true"></i> {{__('Already Applied')}}</a>
                            @else
                                <a href="{{route('apply.job', $job->slug)}}" class="btn apply"><i class="fa fa-paper-plane" aria-hidden="true"></i> {{__('Apply Now')}}</a>
                            @endif
                        @endif
<?php /*                        <a href="{{route('edit.front.job', [$job->id])}}" class="btn"><i class="fa fa-edit" aria-hidden="true"></i>{{__('Edit')}}</a> */?>
                        @if(!Auth::guard('company')->user())
                        <a href="{{route('email.to.friend', $job->slug)}}" class="btn"><i class="fa fa-envelope" aria-hidden="true"></i> {{__('Email to Friend')}}</a>
                        @endif

                        @if(Auth::check() && Auth::user()->isFavouriteJob($job->slug))
                                <a href="{{route('remove.from.favourite', $job->slug)}}" class="btn">
                                    <i class="fa fa-floppy-o" aria-hidden="true"></i> {{__('Remove Favourite')}} </a>
                        @elseif(!Auth::guard('company')->user())
                                <a href="{{route('add.to.favourite', $job->slug)}}" class="btn"><i class="fa fa-floppy-o" aria-hidden="true">
                                    </i> {{__('Add Favourite')}}</a>
                        @endif
<?php /*                        <a href="javascript:;" onclick="deleteJob({{$job->id}});" class="btn"><i class="fa fa-trash" aria-hidden="true"></i>{{__('Delete')}}</a> */?>

                        @if(Auth::guard('company')->user())
                                <a href="javascript:;" onclick="deleteJob({{$job->id}});" class="btn report"><i class="fa fa-trash" aria-hidden="true"></i>{{__('Delete')}}</a>
                        @else
                                <a href="{{route('report.abuse', $job->slug)}}" class="btn report"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> {{__('Report Abuse')}}</a>
                        @endif
                    </div>
                </div>
                <!-- Job Description start -->
                <div class="job-header">
                    <div class="contentbox">
                        <h3>{{__('Job Description')}}</h3>
                        <p class="text-wrap">{!! $job->description !!}</p>

                        <hr>
                        <h3>{{__('Skills Required')}}</h3>
                        <ul class="skillslist">
                            {!!$job->getJobSkillsList()!!} 
                        </ul>
                    </div>
                </div>
                <!-- Job Description end -->
                <!-- related jobs start -->
                <div class="relatedJobs">
                    <h3>{{__('Related Jobs')}}</h3>
                        @if(isset($relatedJobs) && count($relatedJobs) == 0)
                            No Jobs Related
                        @endif
                        @if(isset($relatedJobs) && count($relatedJobs))
                        @foreach($relatedJobs as $relatedJob)
                        <?php $relatedJobCompany = $relatedJob->getCompany(); ?>
                        @if(null !== $relatedJobCompany)
                        <!--Job start-->
                        <div class="job-header col-md-6 searchListNew">
                            <div class="row">
                                <div class="col-md-4 col-sm-4" style="margin-top:10px">
                                    <a href="{{route('job.detail', [$relatedJob->slug])}}" title="{{$relatedJob->title}}">
                                            {{$relatedJobCompany->printCompanyImage()}}
                                        </a>
                                </div>
                                <div class="col-md-8 col-sm-8" style="margin-top:10px">
                                    <div class="row">
                                        <h3><a href="{{route('job.detail', [$relatedJob->slug])}}" title="{{$relatedJob->title}}">{{$relatedJob->title}}</a></h3>
                                        <div class="companyName"><a href="{{route('company.detail', $relatedJobCompany->slug)}}" title="{{$relatedJobCompany->name}}">{{$relatedJobCompany->name}}</a></div>
                                    </div>
                                </div>
                                <div class="col-md-12 col-sm-12" style="margin-top:10px">
                                    <div class="location">
                                        <label class="fulltime">{{$relatedJob->getJobType('job_type')}}</label>
                                        <?php /*<label class="partTime">{{$relatedJob->getJobShift('job_shift')}}</label> */?>
                                        <br><br> <span><i class="fa fa-map-marker" aria-hidden="true"></i>&nbsp;{{$relatedJob->getCity('city')}}</span></div>
                                        <br><br>
                                </div>
                                <div class="clearfix"></div>
<?php /*                                </div> */?>
<?php /*                                <div class="col-md-4 col-sm-4">
                                    <div class="listbtn"><a href="{{route('job.detail', [$relatedJob->slug])}}">{{__('View Detail')}}</a></div>
                                </div>*/?>
                            </div>
<?php /*                            <div class="module_posted_job line-clamp">{!! $relatedJob->description !!}</div> */?>
                        </div>

                        <!--Job end-->
                        @endif
                        @endforeach
                        @endif
                        <!-- Job end -->
                </div>
            </div>
            <!-- related jobs end -->

            <div class="col-md-4"> 
				
				<div class="companyinfo">
                            <div class="companylogo"><a href="{{route('company.detail',$company->slug)}}">{{$company->printCompanyImage()}}</a></div>
                            <div class="title"><a href="{{route('company.detail',$company->slug)}}">{{$company->name}}</a></div>
                            <div class="ptext">{{$company->getLocation()}}</div>
                            <div class="opening">
                                <a href="{{route('company.detail',$company->slug)}}">
                                    {{App\Company::countNumJobs('company_id', $company->id)}} {{__('Current Jobs Openings')}}
                                </a>
                            </div>
                            <div class="clearfix"></div>
                        </div>
				
				
                <!-- Job Detail start -->
                <div class="job-header">
                    <div class="jobdetail">
                        <h3>{{__('Job Detail')}}</h3>
                        <ul class="jbdetail">
                            <li class="row">
                                <div class="col-md-12 col-xs-12"> 
                                    @if((bool)$job->is_freelance)
                                    <span>Freelance</span>
                                    @else 
                                    {{--  <ul class="nav navbar-nav"> 
                                            <li class="dropdown userbtn" title="Job Location's">Job Locations <i class="fa fa-caret-down" aria-hidden="true"></i>
                                                <ul class="dropdown-menu"> 
                                                    {!!$job->getJobLocation()!!}    
                                                </ul>
                                            </li>                 
                                        </ul> --}}
                                        <div style="margin-bottom:5px">{{__('Job Locations')}}</div>
                                        @if($job->jobLocation->count() <= 1)
                                            <div>{!!$job->getJobLocation()!!}</div>
                                        @else
                                            <a href="#" style="text-align:left; cursor:pointer;" data-toggle="popover" data-trigger="hover focus" data-placement="bottom"
                                               data-html="true" data-content="{!!$job->getJobLocation()!!}">{{__('Several work locations')}}</a>
                                        @endif
                                    @endif
                                </div>
                            </li>
                            <li class="row">
                                <div class="col-md-4 col-xs-5">{{__('Company')}}</div>
                                <div class="col-md-8 col-xs-7"><a href="{{route('company.detail',$company->slug)}}">{{$company->name}}</a></div>
                            </li>
                            <li class="row">
                                <div class="col-md-4 col-xs-5">{{__('Type')}}</div>
                                <div class="col-md-8 col-xs-7"><span class="permanent">{{$job->getJobType('job_type')}}</span></div>
                            </li>
                            <li class="row">
                                <div class="col-md-4 col-xs-5">{{__('Shift')}}</div>
                                <div class="col-md-8 col-xs-7"><span class="freelance">{{$job->getJobShift('job_shift')}}</span></div>
                            </li>
                            <li class="row">
                                <div class="col-md-4 col-xs-5">{{__('Career Level')}}</div>
                                <div class="col-md-8 col-xs-7"><span>{{$job->getCareerLevel('career_level')}}</span></div>
                            </li>
                            <li class="row">
                                <div class="col-md-4 col-xs-5">{{__('Positions')}}</div>
                                <div class="col-md-8 col-xs-7"><span>{{$job->num_of_positions}}</span></div>
                            </li>
                            <li class="row">
                                <div class="col-md-4 col-xs-5">{{__('Experience')}}</div>
                                <div class="col-md-8 col-xs-7"><span>{{$job->getJobExperience('job_experience')}}</span></div>
                            </li>
                            <li class="row">
                                <div class="col-md-4 col-xs-5">{{__('Gender')}}</div>
                                <div class="col-md-8 col-xs-7"><span>{{$job->getGender('gender')}}</span></div>
                            </li>
                            <li class="row">
                                <div class="col-md-4 col-xs-5">{{__('Degree')}}</div>
                                <div class="col-md-8 col-xs-7"><span>{{$job->getDegreeLevel('degree_level')}}</span></div>
                            </li>
                            <li class="row">
                                <div class="col-md-4 col-xs-5">{{__('Apply Before')}}</div>
                                <div class="col-md-8 col-xs-7"><span>{{$job->expiry_date->format('M j, Y')}}</span></div>
                            </li>
                            <li class="row">
                                <div class="col-md-4 col-xs-5">{{__('Language')}}</div>
                                <div class="col-md-8 col-xs-7"><span class="permanent">{!!$job->getJobLanguage()!!}</span></div>
                            </li>
                            <li class="row">
                                <div class="col-md-4 col-xs-5">{{__('Benefit')}}</div>
                                <div class="col-md-8 col-xs-7"><span>{!!$job->getJobBenefits()!!}</span></div>
                            </li>
                        </ul>
                    </div>
                </div>
                <!-- Google Map start -->
<?php /*                @php
                if(($company->latitude!='' OR $company->longitude!='')){
                @endphp 
                <div class="job-header">
                    <div class="jobdetail">
                        <h3>{{__('Google Map')}}</h3>
                        <div class="gmap"> 

                            @include('includes.map')  
                            @php   
                            $country =$job->countyname($company->country_id);
                             $state=$job->statename($company->state_id);
                              $city= $job->cityname($company->city_id);
                              ($company->latitude!='' OR $company->longitude!='')?
                            drow_map($company->latitude,$company->longitude , $company->name,$company->location, $country , $state , $city):'';
                            @endphp 
                        </div>
                    </div>
                </div>
                @php 
                }
                @endphp */?>
            </div>
        </div>
    </div>
</div>
@include('includes.footer')
@endsection
@push('styles')
<style type="text/css">
    .view_more{display:none !important;}
</style>
@endpush
@push('scripts') 
<script>
    $(document).ready(function ($) {
        $("form").submit(function () {
            $(this).find(":input").filter(function () {
                return !this.value;
            }).attr("disabled", "disabled");
            return true;
        });
        $(function () {
            $('[data-toggle="popover"]').popover()
        })
        $("form").find(":input").prop("disabled", false);

        $(".view_more_ul").each(function () {
            if ($(this).height() > 100)
            {
                $(this).css('height', 100);
                $(this).css('overflow', 'hidden');
                //alert($( this ).next());
                $(this).next().removeClass('view_more');
            }
        });
    });
    function deleteJob(id) {
        var msg = 'Are you sure you want to delete this job?';
        if (confirm(msg)) {
            $.post("{{ route('delete.front.job') }}", {id: id, _method: 'DELETE', _token: '{{ csrf_token() }}'})
                .done(function (response) {
                    if (response == 'ok')
                    {
                        $('#job_li_' + id).remove();
                    } else
                    {
                        alert('Request Failed!');
                    }
                });
        }
    }
</script>
@endpush