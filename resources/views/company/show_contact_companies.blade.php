<div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal">&times;</button>
            <h4 class="modal-title">{{__('Contact Company')}}</h4>
        </div>
        <div class="modal-body">
            <div id="alert_messages">{!! isset($errors->alert_message)?$errors->alert_message:'' !!}</div>
            <?php
            $from_name = $from_email = $from_phone = $subject = $message = $from_id = '';
            if (Auth::check()) {
                $from_name = Auth::user()->name;
                $from_email = Auth::user()->email;
                $from_phone = Auth::user()->mobile_num;
                $from_id = Auth::user()->id;
            }
            $from_name = old('name', $from_name);
            $from_email = old('email', $from_email);
            $from_phone = old('phone', $from_phone);
            $subject = old('subject');
            $message = old('message');
            ?>
            <form method="post" id="send-company-message-form">
                {{ csrf_field() }}
                <input type="hidden" name="to_id" value="{{ $company->id }}">
                <input type="hidden" name="from_id" value="{{ $from_id }}">
                <input type="hidden" name="company_id" value="{{ $company->id }}">
                <input type="hidden" name="company_name" value="{{ $company->name }}">
                <div class="formpanel">
                    <div class="formrow {{isset($errors->from_name[0])?'has-error':''}}">
                        <input type="text" name="from_name" value="{{ $from_name }}" class="form-control" placeholder="{{__('Your Name').' '.__('(required)')}}">
                        <span class="help-block"> <strong>{{ isset($errors->from_name[0])?$errors->from_name[0]:'' }}</strong> </span>
                    </div>
                    <div class="formrow {{isset($errors->from_email[0])?'has-error':''}}">
                        <input type="text" name="from_email" value="{{ $from_email }}" class="form-control" placeholder="{{__('Your Email').' '.__('(required)')}}">
                        <span class="help-block"> <strong>{{ isset($errors->from_email[0])?$errors->from_email[0]:'' }}</strong> </span>
                    </div>
                    <div class="formrow {{isset($errors->from_phone[0])?'has-error':''}}">
                        <input type="text" name="from_phone" value="{{ $from_phone }}" class="form-control" placeholder="{{__('Phone').' '.__('(required)')}}">
                        <span class="help-block"> <strong>{{ isset($errors->from_phone[0])?$errors->from_phone[0]:'' }}</strong> </span>
                    </div>
                    <div class="formrow {{isset($errors->subject[0])?'has-error':''}}">
                        <input type="text" name="subject" value="{{ $subject }}" class="form-control" placeholder="{{__('Subject').' '.__('(required)')}}">
                        <span class="help-block"> <strong>{{ isset($errors->subject[0])?$errors->subject[0]:'' }}</strong> </span>
                    </div>
                    <div class="formrow {{isset($errors->message[0])?'has-error':''}}">
                        <textarea name="message" class="form-control" placeholder="{{__('Message').' '.__('(required)')}}">{{ $message }}</textarea>
                        <span class="help-block"> <strong>{{ isset($errors->message[0])?$errors->message[0]:'' }}</strong> </span>
                    </div>
                    <div class="formrow">{!! app('captcha')->display() !!}</div>
                    <div class="formrow">
                        <input type="button" class="btn" value="{{__('Submit')}}" id="send_company_message">
                    </div>
                </div>
            </form>
        </div>
    </div>

</div>

<script src='https://www.google.com/recaptcha/api.js'></script>
