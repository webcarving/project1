<style>
    .select-single .select2-selection__rendered {
        line-height: 46px !important;
    }
    .select-single .select2-container .select2-selection--single {
        height: 50px !important;
    }
    .select-single .select2-selection__arrow {
        height: 49px !important;
    }
</style>
<h5>{{__('Company Information')}}</h5>
{!! Form::model($company, array('method' => 'put', 'route' => array('update.company.profile'), 'class' => 'form', 'files'=>true)) !!}
<h6>{{__('Company Logo')}}</h6>
<div class="row">
    <div class="col-md-6">
        <div class="formrow"> {{ ImgUploader::print_image("company_logos/$company->logo", 150, 150) }} </div>
    </div>
    <div class="col-md-6">
        <div class="formrow">
            <div id="thumbnail">
                <div id="thumbnail_img"><img src="images/no-image.jpg" /></div>
                <div id="logo_upload" class="formrow {!! APFrmErrHelp::hasError($errors, 'logo') !!}"></div>
            </div>
        </div>
    </div>
</div>
<div class="row">
    <?php /*    <div class="col-md-6 col-md-offset-6" style="margin-bottom:20px;">
        <div id="logo_upload" class="formrow {!! APFrmErrHelp::hasError($errors, 'logo') !!}" style="padding-top: 25px">
        <label class="form-lable">Profile Image
            <i id="image_information" class="fa fa-info-circle" data-toggle="popover" data-trigger="hover"
               data-placement="top" data-html="true"
               data-content="
                   Format : .jpg or .jpeg or .gif<br>
                   Max Size :  50KB <br>
                   Dimension : 150 x 150 pixels<br>
                   "></i>
        </label>
        <label class="btn btn-default"> {{__('Select Company Logo')}}
            <input type="file" name="logo" id="logo" style="display: none;">
        </label>
            {!! APFrmErrHelp::showErrors($errors, 'logo') !!}</div>
    </div> */?>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4">
                <label class="form-lable">Company Name</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'name') !!}"> {!! Form::text('name', null, array('class'=>'form-control', 'id'=>'name',
                'placeholder'=>__('Company Name').' '.__('(required)')
                )) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'name') !!} </div>
            </div>
            <div class="col-md-4">
                <label class="form-lable">Company Email</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'email') !!}"> {!! Form::text('email', null, array('class'=>'form-control', 'id'=>'email', 'placeholder'=>__('Company Email').' '.__('(required)'))) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'email') !!} </div>
            </div>
            <div class="col-md-4">
                <label class="form-lable">Profile Image
                    <i id="image_information" class="fa fa-info-circle" data-toggle="popover" data-trigger="hover"
                       data-placement="top" data-html="true"
                       data-content="
                   Format : .jpg or .jpeg or .gif<br>
                   Max Size :  50KB <br>
                   Dimension : 150 x 150 pixels<br>
                   "></i>
                </label>
                <label class="btn btn-default">{{__('Select Company Logo')}}
                    <input type="file" name="logo" id="logo" style="display: none;">
                </label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'logo') !!}">
                    {!! '<span id="name-error" class="help-block help-block-error">' . $errors->first('logo') . '</span>' !!}
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4">
                <label class="form-lable">Company CEO</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'ceo') !!}"> {!! Form::text('ceo', null, array('class'=>'form-control', 'id'=>'ceo',
                'placeholder'=>__('Company CEO').' '.__('(required)')
                )) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'ceo') !!} </div>
            </div>
            <div class="col-md-4 select-single">
                <label class="form-lable">Industry Type</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'industry_id') !!}">
                    <?php	$industry_id = old('industry_id', null);
                    ?>	{!! Form::select('industry_id', ['' => __('Select Industry Type').' '.__('(required)')] + $industries, $industry_id, array('class'=>'form-control select-multiple-industry-type', 'id'=>'industry_id','style' => 'width:100%')) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'industry_id') !!} </div>
            </div>
            <div class="col-md-4 select-single">
                <label class="form-lable">Ownership Type</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'ownership_type_id') !!}">
                    <?php	$ownership_type_id = old('ownership_type_id', null);
                    ?>	{!! Form::select('ownership_type_id', ['' => __('Select Ownership Type').' '.__('(required)')] + $ownershipTypes, $ownership_type_id, array('class'=>'form-control select-multiple-ownership-type', 'id'=>'ownership_type_id','style' => 'width:100%')) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'ownership_type_id') !!} </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <label class="form-lable"> Company details</label>
        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'description') !!}"> {!! Form::textarea('description', null, array('class'=>'form-control', 'id'=>'description',
        'placeholder'=>__('Company details').' '.__('(required)')
        )) !!}
            {!! APFrmErrHelp::showErrors($errors, 'description') !!} </div>
    </div>
    <?php /*
    <div class="col-md-12">
        <label class="form-lable">Location</label>
        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'location') !!}"> {!! Form::text('location', null, array('class'=>'form-control', 'id'=>'location', 'placeholder'=>__('Location'))) !!}
            {!! APFrmErrHelp::showErrors($errors, 'location') !!} </div>
    </div>
 */?>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 select-single">
                <label class="form-lable">Number of Offices</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'no_of_offices') !!}">
                    <?php	$no_of_offices = old('no_of_offices', null);
                    ?>	{!! Form::select('no_of_offices', ['' => __('Select Number of Offices').' '.__('(required)')] + MiscHelper::getNumOffices(), $no_of_offices, array('class'=>'form-control select-multiple-number-of-offices', 'id'=>'no_of_offices','style' => 'width:100%')) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'no_of_offices') !!} </div>
            </div>
            <div class="col-md-4">
                <label class="form-lable">Website</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'website') !!}"> {!! Form::text('website', null, array('class'=>'form-control', 'id'=>'website',
                'placeholder'=>__('Website').' '.__('(required)')
                )) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'website') !!} </div>
            </div>
            <div class="col-md-4 select-single">
                <label class="form-lable">Number of Employees</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'no_of_employees') !!}">
                    <?php	$no_of_employees = old('no_of_employees', null);
                    ?>	{!! Form::select('no_of_employees', ['' => __('Select Number of Employees').' '.__('(required)')] + MiscHelper::getNumEmployees(), $no_of_employees, array('class'=>'form-control select-multiple-number-of-employees', 'id'=>'no_of_employees','style' => 'width:100%')) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'no_of_employees') !!} </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 select-single">
                <label class="form-lable">Established In</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'no_of_employees') !!}">
                    <?php	$no_of_employees = old('no_of_employees', null);
                    ?>	{!! Form::select('no_of_employees', ['' => __('Select Established In').' '.__('(required)')] + MiscHelper::getEstablishedIn(), $no_of_employees, array('class'=>'form-control select-multiple-established-in', 'id'=>'no_of_employees','style' => 'width:100%')) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'no_of_employees') !!} </div>
            </div>
            <div class="col-md-4">
                <label class="form-lable">Fax</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'fax') !!}"> {!! Form::text('fax', null, array('class'=>'form-control', 'id'=>'fax',
                'placeholder'=>__('Fax')
                )) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'fax') !!} </div>
            </div>
            <div class="col-md-4">
                <label class="form-lable">Phone</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'phone') !!}"> {!! Form::text('phone', null, array('class'=>'form-control', 'id'=>'phone',
                'placeholder'=>__('Phone').' '.__('(required)')
                )) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'phone') !!} </div>
            </div>
        </div>
    </div>
    <div class="clearfix"></div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-6">
                <label class="form-lable">Facebook</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'facebook') !!}"> {!! Form::text('facebook ', null, array('class'=>'form-control', 'id'=>'facebook', 'placeholder'=>__('Facebook'))) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'facebook') !!} </div>
            </div>
            <div class="col-md-6">
                <label class="form-lable">Twitter</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'twitter') !!}"> {!! Form::text('twitter', null, array('class'=>'form-control', 'id'=>'twitter', 'placeholder'=>__('Twitter'))) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'twitter') !!} </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4">
                <label class="form-lable">Linkedin</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'linkedin') !!}"> {!! Form::text('linkedin', null, array('class'=>'form-control', 'id'=>'linkedin', 'placeholder'=>__('Linkedin'))) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'linkedin') !!} </div>
            </div>
            <div class="col-md-4">
                <label class="form-lable">Google+</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'google_plus') !!}"> {!! Form::text('google_plus', null, array('class'=>'form-control', 'id'=>'google_plus', 'placeholder'=>__('Google+'))) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'google_plus') !!} </div>
            </div>
            <div class="col-md-4">
                <label class="form-lable">Pinterest</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'pinterest') !!}"> {!! Form::text('pinterest', null, array('class'=>'form-control', 'id'=>'pinterest', 'placeholder'=>__('Pinterest'))) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'pinterest') !!} </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4 select-single">
                <label class="form-lable">Country</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'country_id') !!}">
                    <?php	$country_id = old('country_id', null);
                    ?>	{!! Form::select('country_id', ['' => __('Select Country').' '.__('(required)')] + $countries, $country_id, array('class'=>'form-control select-multiple-country', 'id'=>'country_id','style' => 'width:100%')) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'country_id') !!} </div>
            </div>
            <div class="col-md-4 select-single">
                <label class="form-lable">State</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'state_id') !!}">
                    <?php	$state_id = old('state_id', null);
                    ?>	<span id="default_state_dd"> {!! Form::select('state_id', ['' => __('Select State').' '.__('(required)')] , $state_id, array('class'=>'form-control select-multiple-state', 'id'=>'state_id','style' => 'width:100%')) !!} </span>
                    {!! APFrmErrHelp::showErrors($errors, 'state_id') !!} </div>
            </div>
            <div class="col-md-4 select-single">
                <label class="form-lable">City</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'city_id') !!}">
                    <?php	$city_id = old('city_id', null);
                    ?>	<span id="default_city_dd"> {!! Form::select('city_id', ['' => __('Select City').' '.__('(required)')] , $city_id, array('class'=>'form-control select-multiple-city', 'id'=>'city_id','style' => 'width:100%')) !!} </span>
                    {!! APFrmErrHelp::showErrors($errors, 'city_id') !!} </div>
            </div>
        </div>
    </div>
    <?php /*    <div class="col-md-12">
        <div class="row">
            <div class="col-md-4">
                <label class="form-lable">Longitude</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'longitude') !!}"> {!! Form::text('longitude', null, array('class'=>'form-control', 'id'=>'longitude', 'placeholder'=>__('Longitude'))) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'longitude') !!} </div>
            </div>
            <div class="col-md-4">
                <label class="form-lable">Latitude</label>
                <div class="formrow {!! APFrmErrHelp::hasError($errors, 'latitude') !!}"> {!! Form::text('latitude', null, array('class'=>'form-control', 'id'=>'latitude', 'placeholder'=>__('Latitude'))) !!}
                    {!! APFrmErrHelp::showErrors($errors, 'latitude') !!} </div>
            </div>
        </div>
    </div> */?>

    <div class="col-md-12">
        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'is_subscribed') !!}">
            <?php
            $is_checked = 'checked="checked"';
            if (old('is_subscribed', ((isset($company)) ? $company->is_subscribed : 1)) == 0) {
                $is_checked = '';
            }
            ?>
            <input type="checkbox" value="1" name="is_subscribed" {{$is_checked}} />
            {{__('Subscribe to news letter')}}
            {!! APFrmErrHelp::showErrors($errors, 'is_subscribed') !!}
        </div>
    </div>
    <div class="col-md-12">
        <div class="formrow">
            <button type="submit" class="btn">{{__('Update Profile and Save')}} <i class="fa fa-arrow-circle-right" aria-hidden="true"></i></button>
        </div>
    </div>
</div>
<input type="file" name="image" id="image" style="display:none;" accept="image/*"/>
{!! Form::close() !!}
<hr>
@push('styles')
    <style type="text/css">
        .datepicker>div {
            display: block;
        }
    </style>
@endpush
@push('scripts')
    @include('includes.tinyMCEFront')
    <script type="text/javascript">
        $(document).ready(function () {
            $('[data-toggle="popover"]').popover();
            $('#country_id').on('change', function (e) {
                e.preventDefault();
                filterLangStates(0);
            });
            $(document).on('change', '#state_id', function (e) {
                e.preventDefault();
                filterLangCities(0);
            });
            filterLangStates(<?php echo old('state_id', (isset($company)) ? $company->state_id : 0); ?>);

            /*******************************/
            var fileInput = document.getElementById("logo");
            fileInput.addEventListener("change", function (e) {
                var files = this.files
                showThumbnail(files)
            }, false);
            $('.select-multiple-industry-type').select2({
                placeholder: "{{__('Select Industry Type').' '.__('(required)')}}",
//            allowClear: true
            });
            $('.select-multiple-ownership-type').select2({
                placeholder: "{{__('Select Ownership Type').' '.__('(required)')}}",
//            allowClear: true
            });
            $('.select-multiple-number-of-offices').select2({
                placeholder: "{{__('Select Number of Offices').' '.__('(required)')}}",
//            allowClear: true
            });
            $('.select-multiple-number-of-employees').select2({
                placeholder: "{{__('Select Number of Employees').' '.__('(required)')}}",
//            allowClear: true
            });
            $('.select-multiple-established-in').select2({
                placeholder: "{{__('Select Established in').' '.__('(required)')}}",
//            allowClear: true
            });
            $('.select-multiple-country').select2({
                placeholder: "{{__('Select Country').' '.__('(required)')}}",
//            allowClear: true
            });
            $('.select-multiple-state').select2({
                placeholder: "{{__('Select State').' '.__('(required)')}}",
//            allowClear: true
            });
            $('.select-multiple-city').select2({
                placeholder: "{{__('Select City').' '.__('(required)')}}",
//            allowClear: true
            });
        });

        function showThumbnail(files) {
            $('#thumbnail_img').html('<img src="images/no-image.jpg" />');
            for (var i = 0; i < files.length; i++) {
                var file = files[i]
                var img = new Image();
                img.src = window.URL.createObjectURL( file );
                img.onload = function() {
                    var width = img.naturalWidth,
                        height = img.naturalHeight;
                    var err_message = ''
                    if (width != 150) {
                        err_message = "Image width should be 150 px";
                    }
                    if (height != 150) {
                        err_message = "Image height should be 150 px";
                    }
                    if (file.size > 50000) {
                        err_message = "Image size should be lower than 50 KB";
                    }
                    if (!file.type.match('image/jpeg') && !file.type.match('image/png')) {
                        err_message = "File must be jpeg or jpg or png";
                    }
                    if (err_message != '') {
                        $('#logo_upload').addClass('has-error msg_cls_for_focus');
                        $('#logo_upload').html('<span id="name-error" class="help-block help-block-error">' + err_message + '</span>');
                        return false;
                    }
                    $('#logo_upload').removeClass('has-error msg_cls_for_focus');
                    $('#logo_upload').html();
                    var reader = new FileReader()
                    reader.onload = (function (theFile) {
                        return function (e) {
                            $('#thumbnail_img').html('<div class="fileattached"><img height="150px" src="' + e.target.result + '" > <div>' + theFile.name + '</div><div class="clearfix"></div></div>');
                        };
                    }(file))
                    var ret = reader.readAsDataURL(file);
                };
            }
        }


        function filterLangStates(state_id)
        {
            var country_id = $('#country_id').val();
            if (country_id != '') {
                $.post("{{ route('filter.lang.states.dropdown') }}", {country_id: country_id, state_id: state_id, _method: 'POST', _token: '{{ csrf_token() }}'})
                    .done(function (response) {
                        $('#default_state_dd').html(response);
                        filterLangCities(<?php echo old('city_id', (isset($company)) ? $company->city_id : 0); ?>);
                        $('.select-multiple-state').select2({
                            placeholder: "{{__('Select State').' '.__('(required)')}}",
//            allowClear: true
                        });

                    });
            }
        }
        function filterLangCities(city_id)
        {
            var state_id = $('#state_id').val();
            if (state_id != '') {
                $.post("{{ route('filter.lang.cities.dropdown') }}", {state_id: state_id, city_id: city_id, _method: 'POST', _token: '{{ csrf_token() }}'})
                    .done(function (response) {
                        $('#default_city_dd').html(response);
                        $('.select-multiple-city').select2({
                            placeholder: "{{__('Select City').' '.__('(required)')}}",
//            allowClear: true
                        });
                    });
            }
        }
    </script>
@endpush