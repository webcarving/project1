{!! APFrmErrHelp::showErrorsNotice($errors) !!}
@include('flash::message')
<div class="form-body">
    <h3>Drag and Drop to Sort Degree Levels</h3>
    {!! Form::select('lang', ['' => __('Select Language')]+$languages, config('default_lang'), array('class'=>'form-control select-multiple-language','style' => 'width:100%', 'id'=>'lang', 'onchange'=>'refreshDegreeLevelSortData();')) !!}
    <div id="degreeLevelSortDataDiv"></div>
</div>
@push('scripts') 
<script>
    $(document).ready(function () {
        refreshDegreeLevelSortData();
        $('.select-multiple-language').select2({
            placeholder: "{{__('Select Language')}}",
//            allowClear: true
        });
    });
    function refreshDegreeLevelSortData() {
        var language = $('#lang').val();
        $.ajax({
            type: "GET",
            url: "{{ route('degree.level.sort.data') }}",
            data: {lang: language},
            success: function (responseData) {
                $("#degreeLevelSortDataDiv").html('');
                $("#degreeLevelSortDataDiv").html(responseData);
                /**************************/
                $('#sortable').sortable({
                    update: function (event, ui) {
                        var degreeLevelOrder = $(this).sortable('toArray').toString();
                        $.post("{{ route('degree.level.sort.update') }}", {degreeLevelOrder: degreeLevelOrder, _method: 'PUT', _token: '{{ csrf_token() }}'})
                    }
                });
                $("#sortable").disableSelection();
                /***************************/
            }
        });
    }
</script> 
@endpush
