{!! APFrmErrHelp::showErrorsNotice($errors) !!}
@include('flash::message')
<div class="form-body">  
<script type="text/javascript"> 
var allcountry_list=[];
</script>      
    {!! Form::hidden('id', null) !!}
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'company_id') !!}" id="company_id_div">
        {!! Form::label('company_id', __('Company'), ['class' => 'bold']) !!}
        {!! Form::select('company_id', ['' => __('Select Company').' '.__('(required)')]+$companies, null, ['class' => 'form-control select-multiple-company','style' => 'width:100%']) !!}
        {!! APFrmErrHelp::showErrors($errors, 'company_id') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'title') !!}">
        {!! Form::label('title', __('Job Title'), ['class' => 'bold']) !!}
        {!! Form::text('title', null, array('class'=>'form-control', 'id'=>'title', 'placeholder'=>__('Job Title').' '.__('(required)'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'title') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'description') !!}">
        {!! Form::label('description', __('Job Description'), ['class' => 'bold']) !!}
        {!! Form::textarea('description', null, array('class'=>'form-control', 'id'=>'description', 'placeholder'=>__('Job Description').' '.__('(required)'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'description') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'skills') !!}">
        {!! Form::label('skills', __('Skills'), ['class' => 'bold']) !!}
        <?php
        $skills = old('skills', $jobSkillIds);
        ?>
        {!! Form::select('skills[]', $jobSkills, $skills, array('class'=>'form-control select-multiple-skill', 'id'=>'skills', 'multiple'=>'multiple')) !!}
        {!! APFrmErrHelp::showErrors($errors, 'skills') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'beneftis') !!}">
        {!! Form::label('benefits', __('Benefits'), ['class' => 'bold']) !!}
        <?php
        $benefits = old('benefits', $jobBenefitIds);
        ?>
        {!! Form::select('benefits[]', $jobBenefits, $benefits, array('class'=>'form-control select-multiple-benefit', 'id'=>'benefits', 'multiple'=>'multiple')) !!}
        {!! APFrmErrHelp::showErrors($errors, 'benefits') !!}
    </div>
<?php /*    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'country_id') !!}" id="country_id_div">
        {!! Form::label('country_id', 'Country', ['class' => 'bold']) !!}                    
        {!! Form::select('country_id', ['' => __('Select Country')]+$countries, old('country_id', (isset($job))? $job->country_id:$siteSetting->default_country_id), array('class'=>'form-control select-multiple-country','style' => 'width:100%', 'id'=>'country_id')) !!}
        {!! APFrmErrHelp::showErrors($errors, 'country_id') !!}                                       
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'state_id') !!}" id="state_id_div">
        {!! Form::label('state_id', 'State', ['class' => 'bold']) !!}                    
        <span id="default_state_dd">
            {!! Form::select('state_id', ['' => __('Select State')], null, array('class'=>'form-control select-multiple-state','style' => 'width:100%', 'id'=>'state_id')) !!}
        </span>
        {!! APFrmErrHelp::showErrors($errors, 'state_id') !!}                                       
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'city_id') !!}" id="city_id_div">
        {!! Form::label('city_id', 'City', ['class' => 'bold']) !!}                    
        <span id="default_city_dd">
            {!! Form::select('city_id', ['' => __('Select City')], null, array('class'=>'form-control select-multiple-city','style' => 'width:100%', 'id'=>'city_id')) !!}
        </span>
        {!! APFrmErrHelp::showErrors($errors, 'city_id') !!}                                       
    </div>
 */ ?>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'salary_from') !!}" id="salary_from_div">
        {!! Form::label('salary_from', __('Salary From'), ['class' => 'bold']) !!}
        {!! Form::text('salary_from', null, array('class'=>'form-control', 'id'=>'salary_from', 'placeholder'=>__('Salary From').' '.__('(required)'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'salary_from') !!}                                       
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'salary_to') !!}" id="salary_to_div">
        {!! Form::label('salary_to', __('Salary To'), ['class' => 'bold']) !!}
        {!! Form::text('salary_to', null, array('class'=>'form-control', 'id'=>'salary_to', 'placeholder'=>__('Salary To').' '.__('(required)'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'salary_to') !!}                                       
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'salary_currency') !!}" id="salary_currency_div">
        {!! Form::label('salary_currency', __('Salary Currency'), ['class' => 'bold']) !!}
        {!! Form::select('salary_currency', ['' => __('Select Salary Currency').' '.__('(required)')]+$currencies, null, ['class' => 'form-control select-multiple-salary-currency','style' => 'width:100%']) !!}
        {!! APFrmErrHelp::showErrors($errors, 'salary_currency') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'salary_period_id') !!}" id="salary_period_id_div">
        {!! Form::label('salary_period_id', __('Salary Period'), ['class' => 'bold']) !!}
        {!! Form::select('salary_period_id', ['' => __('Select Salary Period').' '.__('(required)')]+$salaryPeriods, null, ['class' => 'form-control select-multiple-salary-period','style' => 'width:100%']) !!}
        {!! APFrmErrHelp::showErrors($errors, 'salary_period_id') !!}

    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'hide_salary') !!}">
        {!! Form::label('hide_salary', __('Hide Salary?'), ['class' => 'bold']) !!}
        <div class="radio-list">
            <?php
            $hide_salary_1 = '';
            $hide_salary_2 = 'checked="checked"';
            if (old('hide_salary', ((isset($job)) ? $job->hide_salary : 0)) == 1) {
                $hide_salary_1 = 'checked="checked"';
                $hide_salary_2 = '';
            }
            ?>
            <label class="radio-inline">
                <input id="hide_salary_yes" name="hide_salary" type="radio" value="1" {{$hide_salary_1}}>
                {{__('Yes')}} </label>
            <label class="radio-inline">
                <input id="hide_salary_no" name="hide_salary" type="radio" value="0" {{$hide_salary_2}}>
                {{__('No')}} </label>
        </div>
        {!! APFrmErrHelp::showErrors($errors, 'hide_salary') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'career_level_id') !!}" id="career_level_id_div">
        {!! Form::label('career_level_id', __('Career level'), ['class' => 'bold']) !!}
        {!! Form::select('career_level_id', ['' => __('Select Career level').' '.__('(required)')]+$careerLevels, null, ['class' => 'form-control select-multiple-career-level','style' => 'width:100%']) !!}
        {!! APFrmErrHelp::showErrors($errors, 'career_level_id') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'functional_area_id') !!}" id="functional_area_id_div">
        {!! Form::label('functional_area_id', __('Functional Area'), ['class' => 'bold']) !!}
        {!! Form::select('functional_area_id', ['' => __('Select Functional Area').' '.__('(required)')]+$functionalAreas, null, ['class' => 'form-control select-multiple-functional-area','style' => 'width:100%']) !!}
        {!! APFrmErrHelp::showErrors($errors, 'functional_area_id') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'job_type_id') !!}" id="job_type_id_div">
        {!! Form::label('job_type_id', __('Job Type'), ['class' => 'bold']) !!}
        {!! Form::select('job_type_id', ['' => __('Select Job Type').' '.__('(required)')]+$jobTypes, null, ['class' => 'form-control select-multiple-job-type','style' => 'width:100%']) !!}
        {!! APFrmErrHelp::showErrors($errors, 'job_type_id') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'job_shift_id') !!}" id="job_shift_id_div">
        {!! Form::label('job_shift_id', __('Job Shift'), ['class' => 'bold']) !!}
        {!! Form::select('job_shift_id', ['' => __('Select Job Shift').' '.__('(required)')]+$jobShifts, null, ['class' => 'form-control select-multiple-job-shift','style' => 'width:100%']) !!}
        {!! APFrmErrHelp::showErrors($errors, 'job_shift_id') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'num_of_positions') !!}" id="num_of_positions_div">
        {!! Form::label('num_of_positions', __('Number of Positions'), ['class' => 'bold']) !!}
        {!! Form::select('num_of_positions', ['' => __('Select Number of Positions').' '.__('(required)')]+MiscHelper::getNumPositions(), null, ['class' => 'form-control select-multiple-number-of-positions','style' => 'width:100%']) !!}
        {!! APFrmErrHelp::showErrors($errors, 'num_of_positions') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'gender_id') !!}" id="gender_id_div">
        {!! Form::label('gender_id', __('Gender'), ['class' => 'bold']) !!}
        {!! Form::select('gender_id', ['' => __('Select Gender').' '.__('(required)')]+$genders, null, ['class' => 'form-control select-multiple-gender-preference','style' => 'width:100%']) !!}
        {!! APFrmErrHelp::showErrors($errors, 'gender_id') !!}
    </div>
    @php
        if(isset($job->expiry_date)){
            $expiry_date = date("M j, Y", strtotime($job->expiry_date));
        }else{
            $expiry_date = null;
        }
    @endphp
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'expiry_date') !!}">
        {!! Form::label('expiry_date', __('Job Expiry Date'), ['class' => 'bold']) !!}
        {!! Form::text('expiry_date', $expiry_date, array('class'=>'form-control datepicker', 'id'=>'expiry_date', 'placeholder'=>__('Job Expiry Date').' '.__('(required)'), 'autocomplete'=>'off')) !!}
        {!! APFrmErrHelp::showErrors($errors, 'expiry_date') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'degree_level_id') !!}" id="degree_level_id_div">
        {!! Form::label('degree_level_id', __('Required Degree Level'), ['class' => 'bold']) !!}
        {!! Form::select('degree_level_id', ['' => __('Select Required Degree Level').' '.__('(required)')]+$degreeLevels, null, ['class' => 'form-control select-multiple-required-degree-level','style' => 'width:100%']) !!}
        {!! APFrmErrHelp::showErrors($errors, 'degree_level_id') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'job_experience_id') !!}" id="job_experience_id_div">
        {!! Form::label('job_experience_id', __('Job Experience'), ['class' => 'bold']) !!}
        {!! Form::select('job_experience_id', ['' => __('Select Job Experience').' '.__('(required)')]+$jobExperiences, null, ['class' => 'form-control select-multiple-job-experience','style' => 'width:100%']) !!}
        {!! APFrmErrHelp::showErrors($errors, 'job_experience_id') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'is_freelance') !!}">
        {!! Form::label('is_freelance', __('Is Freelance?'), ['class' => 'bold']) !!}
        <div class="radio-list">
            <?php
            $is_freelance_1 = '';
            $is_freelance_2 = 'checked="checked"';
            if (old('is_freelance', ((isset($job)) ? $job->is_freelance : 0)) == 1) {
                $is_freelance_1 = 'checked="checked"';
                $is_freelance_2 = '';
            }
            ?>
            <label class="radio-inline">
                <input id="is_freelance_yes" name="is_freelance" type="radio" value="1" {{$is_freelance_1}}>
                {{__('Yes')}} </label>
            <label class="radio-inline">
                <input id="is_freelance_no" name="is_freelance" type="radio" value="0" {{$is_freelance_2}}>
                {{__('No')}} </label>
        </div>
        {!! APFrmErrHelp::showErrors($errors, 'is_freelance') !!}
    </div>

    <div class="form-group " id="num_of_positions_div">
        <label class="form-lable bold" >Language</label>
        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'language') !!}">
            <?php
            $language = old('language', $jobLanguageIds);
            ?>
            {!! Form::select('language[]', $jobLanguages, $language, array('class'=>'form-control select-multiple-language', 'id'=>'language', 'multiple'=>'multiple')) !!}
            {!! APFrmErrHelp::showErrors($errors, 'language') !!} </div>                                     
    </div>
      
  
 <div class="form-group {!! APFrmErrHelp::hasError($errors, 'job_location_id') !!}" id="job_experience_id_div">
     {!! Form::label('job_location_id', __('Job Location'), ['class' => 'bold']) !!}
     @php
         $job_location_id = old('job_location_id', array());
     @endphp
     {!! Form::select('job_location_id', ['' => __('Select Job Location').' '.__('(required)')]+$Joblocations, $job_location_id, ['class' => 'form-control select-multiple-job-location','style' => 'width:100%']) !!}
     {!! APFrmErrHelp::showErrors($errors, 'job_location_id') !!}
    </div>
    <div class="form-group">
    <div id="myDIV"> 
        @php
    foreach ($allcurentlocation  as $key => $value) {
        $list=$value['list'];
        $selected=$value['selected'];
        $name2='locations_'.$key.'[]';
        $name3='locations_'.$key;
     @endphp
     <script type="text/javascript">
         allcountry_list.push({{$key}});
     </script>

<div class="form-group " id="work_location_{{$key}}"> 
   <div  id="work_location_{{$key}}b" >
        <label class="form-lable Bold">{{$value['name']}}</label>
        <div class="formrow {!! APFrmErrHelp::hasError($errors, 'locations') !!}">
            <?php
            $locations = old('locations', $selected);
            ?>
            <span id="worklocation_{{$key}}" class="worklocation_{{$key}}"> {!! Form::select($name2, $list, $locations, array('class'=>'form-control select-multiple-job-location', 'id'=>$name3, 'multiple'=>'multiple')) !!}
            </span>
            {!! APFrmErrHelp::showErrors($errors, 'locations') !!} </div>
    </div> 
    </div> 
 @php
    }
    @endphp
    </div></div>
<div class="cler-fixed"></div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'is_active') !!}">
        {!! Form::label('is_active', __('Is Active?'), ['class' => 'bold']) !!}
        <div class="radio-list">
            <?php
            $is_active_1 = 'checked="checked"';
            $is_active_2 = '';
            if (old('is_active', ((isset($job)) ? $job->is_active : 1)) == 0) {
                $is_active_1 = '';
                $is_active_2 = 'checked="checked"';
            }
            ?>
            <label class="radio-inline">
                <input id="active" name="is_active" type="radio" value="1" {{$is_active_1}}>
                {{__('Yes')}} </label>
            <label class="radio-inline">
                <input id="not_active" name="is_active" type="radio" value="0" {{$is_active_2}}>
                {{__('No')}} </label>
        </div>
        {!! APFrmErrHelp::showErrors($errors, 'is_active') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'is_featured') !!}">
        {!! Form::label('is_featured', __('Is Featured?'), ['class' => 'bold']) !!}
        <div class="radio-list">
            <?php
            $is_featured_1 = '';
            $is_featured_2 = 'checked="checked"';
            if (old('is_featured', ((isset($job)) ? $job->is_featured : 0)) == 1) {
                $is_featured_1 = 'checked="checked"';
                $is_featured_2 = '';
            }
            ?>
            <label class="radio-inline">
                <input id="featured" name="is_featured" type="radio" value="1" {{$is_featured_1}}>
                {{__('Yes')}} </label>
            <label class="radio-inline">
                <input id="not_featured" name="is_featured" type="radio" value="0" {{$is_featured_2}}>
                {{__('No')}} </label>
        </div>
        {!! APFrmErrHelp::showErrors($errors, 'is_featured') !!} </div>	



    <div class="form-actions">
        {!! Form::button(__($action).' <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>', array('class'=>'btn btn-large btn-primary', 'type'=>'submit')) !!}
    </div>
</div>
@push('css')
<style type="text/css">
    .datepicker>div {
        display: block;
    }
</style>
@endpush
@push('scripts')
@include('admin.shared.tinyMCEFront')  
<script type="text/javascript">
     function address(value) {
       var name=$("#job_location_id option[value="+value+"]").text(); 
        $("<div id='work_location_"+value+"' class='form-group'><div  id='work_location_"+value+"a'><label class='form-lable Bold'>"+name+"</label></div><div class='formrow' id='work_location_"+value+"b'><div class='formrow '><span id='worklocation_"+value+"' class='worklocation_"+value+"'><select class='form-control select3-multiple' id='locations_"+value+"' multiple='multiple' name='locations_"+value+"[]'></select></span> </div></div></div>").appendTo('#myDIV');
         $('.select3-multiple').select2({
            placeholder: "{{__(' ')}}",
//            allowClear: true
        });
    }
    $(document).ready(function () {
        $('#salary_from, #salary_to').priceFormat({prefix:'',thousandsSeparator:',',limit:21,centsLimit:0,clearOnEmpty:true});
        $('.select-multiple-company').select2({
            placeholder: "Select Company (required)",
//            allowClear: true
        });
        $('.select-multiple-skill').select2({
            placeholder: "Select Skills (required)",
//            allowClear: true
        });
        $('.select-multiple-benefit').select2({
            placeholder: "Select Benefits",
//            allowClear: true
        });
         $('.select-multiple-language').select2({
            placeholder: "{{__('Select Language (required)')}}",
//            allowClear: true
        });
        $('.select-multiple-job-location').select2({
            placeholder: "{{__(' ')}}",
//            allowClear: true
        });
        $('.select-multiple-salary-currency').select2({
            placeholder: "{{__('Select Salary Currency').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-salary-period').select2({
            placeholder: "{{__('Select Salary Period').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-career-level').select2({
            placeholder: "{{__('Select Career Level').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-functional-area').select2({
            placeholder: "{{__('Select Functional Area').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-job-type').select2({
            placeholder: "{{__('Select Job Type').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-job-shift').select2({
            placeholder: "{{__('Select Job Shift').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-number-of-positions').select2({
            placeholder: "{{__('Select Number of Positions').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-gender-preference').select2({
            placeholder: "{{__('Select Gender Preference').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-required-degree-level').select2({
            placeholder: "{{__('Select Required Degree Level').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-job-experience').select2({
            placeholder: "{{__('Select Job Experience').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-job-location').select2({
            placeholder: "{{__('Select Job Location').' '.__('(required)')}}",
//            allowClear: true
        });
        $(".datepicker").datepicker({
            autoclose: true,
            format: 'M d, yyyy'
        });
        $('#country_id').on('change', function (e) {
            e.preventDefault();
            filterDefaultStates(0);
        });
        $(document).on('change', '#state_id', function (e) {
            e.preventDefault();
            filterDefaultCities(0);
        });
        $('#multi_location , #multi_location2 , #multi_location3 , #multi_location4, #multi_location5').on('change', function (e) { 
            var id=(e.target.id);  
            var x = document.getElementById(id).checked;
            var val=$("#"+id).val();
            if(x==true){ 
                multiLocation(val);
            }else{
               // document.getElementById("work_location_"+val).style.visibility = "hidden"; 
                 document.getElementById("work_location_"+val+"b").style.visibility = "hidden";
                 document.getElementById("work_location_"+val+"b").style.height = "0px"; 
                $(".worklocation_"+val+" .select2-selection__rendered").empty();
            } 
        });
         $('#job_location_id').on('change', function (e) {  
            var val=$("#job_location_id").val();
            if(val!=''){ 
                var found = allcountry_list.find(function(element) {
                    return element == val;
                });
                if(found){
                    var elem = document.getElementById('work_location_'+val);
                    elem.remove();   
                }else{
                    address(val); 
                    allcountry_list.push(val);
                    multiLocation(val);
                }
            }  
        }); 
        filterDefaultStates(<?php echo old('state_id', (isset($job)) ? $job->state_id : 0); ?>);
    });
    function filterDefaultStates(state_id)
    {
        var country_id = $('#country_id').val();
        if (country_id != '') {
            $.post("{{ route('filter.default.states.dropdown') }}", {country_id: country_id, state_id: state_id, _method: 'POST', _token: '{{ csrf_token() }}'})
                    .done(function (response) {
                        $('#default_state_dd').html(response);
                        filterDefaultCities(<?php echo old('city_id', (isset($job)) ? $job->city_id : 0); ?>);
                    });
        }
    }
    function filterDefaultCities(city_id)
    {
        var state_id = $('#state_id').val();
        if (state_id != '') {
            $.post("{{ route('filter.default.cities.dropdown') }}", {state_id: state_id, city_id: city_id, _method: 'POST', _token: '{{ csrf_token() }}'})
                    .done(function (response) {
                        $('#default_city_dd').html(response);
                    });
        }
    }
     function multiLocation(country_id)
    {  
        if (country_id != '') {
            $.post("{{ route('filter-lang-addressmulti-dropdown') }}", {country_id: country_id,  _method: 'POST', _token: '{{ csrf_token() }}'})
                    .done(function (response) { 
                        $('#locations_'+country_id).html(response);
                        //document.getElementById("work_location_"+country_id).style.visibility = "visible"; 
                        document.getElementById("work_location_"+country_id+"b").style.visibility = "visible";
                        document.getElementById("work_location_"+country_id+"b").style.height = "auto"; 

                    });
        }
    }
</script>
@endpush