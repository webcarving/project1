{!! APFrmErrHelp::showErrorsNotice($errors) !!}
@include('flash::message')
<div class="form-body">
    <h3>Drag and Drop to Sort Sliders</h3>
    {!! Form::select('lang', ['' => __('Select Language')]+$languages, config('default_lang'), array('class'=>'form-control select-multiple-language','style' => 'width:100%', 'id'=>'lang', 'onchange'=>'refreshSliderSortData();')) !!}
    <div id="sliderSortDataDiv"></div>
</div>
@push('scripts') 
<script>
    $(document).ready(function () {
        refreshSliderSortData();
        $('.select-multiple-language').select2({
            placeholder: "{{__('Select Language')}}",
//            allowClear: true
        });
    });
    function refreshSliderSortData() {
        var language = $('#lang').val();
        $.ajax({
            type: "GET",
            url: "{{ route('slider.sort.data') }}",
            data: {lang: language},
            success: function (responseData) {
                $("#sliderSortDataDiv").html('');
                $("#sliderSortDataDiv").html(responseData);
                /**************************/
                $('#sortable').sortable({
                    update: function (event, ui) {
                        var sliderOrder = $(this).sortable('toArray').toString();
                        $.post("{{ route('slider.sort.update') }}", {sliderOrder: sliderOrder, _method: 'PUT', _token: '{{ csrf_token() }}'})
                    }
                });
                $("#sortable").disableSelection();
                /***************************/
            }
        });
    }
</script> 
@endpush
