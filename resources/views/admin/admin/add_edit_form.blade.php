@if (count($errors) > 0)
<div class="alert alert-danger">
    There were some problems with your input.
</div>
@foreach ($errors->all() as $error)
{{ $error }}<br/>
@endforeach
@endif
@include('flash::message')
<div class="form-body">
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'name') !!}">
        {!! Form::label('name', __('Name'), ['class' => 'bold']) !!}
        {!! Form::text('name', null, array('required', 'class'=>'form-control', 'placeholder'=>__('Name').' '.__('(required)'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'name') !!}                                       
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'email') !!}">
        {!! Form::label('email', __('Email Address'), ['class' => 'bold']) !!}
        {!! Form::text('email', null, array('required', 'class'=>'form-control', 'placeholder'=>__('Email Address').' '.__('(required)'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'email') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'password') !!}">
        {!! Form::label('password', __('Password'), ['class' => 'bold']) !!}
        {!! Form::password('password', array('required', 'class'=>'form-control', 'placeholder'=>__('Password').' '.__('(required)'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'password') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'role_id') !!} select-single">
        {!! Form::label('role', __('Role'), ['class' => 'bold']) !!}
        {!! Form::select('role_id', ['' => __('Select Role').' '.__('(required)')]+$roles, null, ['class' => 'form-control select-multiple-role','style' => 'width:100%']) !!}
        {!! APFrmErrHelp::showErrors($errors, 'role_id') !!}
    </div>
    @push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.select-multiple-role').select2({
                placeholder: "{{__('Select Role').' '.__('(required)')}}",
//            allowClear: true
            });
        });
    </script>
    @endpush
</div>
<?php /*test*/?>