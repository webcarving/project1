<li class="nav-item  "> <a href="javascript:;" class="nav-link nav-toggle"> <i class="fa fa-area-chart" aria-hidden="true"></i> <span class="title">Job Benefits</span> <span class="arrow"></span> </a>
    <ul class="sub-menu">
        <li class="nav-item  "> <a href="{{ route('list.job.benefits') }}" class="nav-link "> <span class="title">List Job Benefits</span> </a> </li>
        <li class="nav-item  "> <a href="{{ route('create.job.benefit') }}" class="nav-link "> <span class="title">Add new Job Benefit</span> </a> </li>
        <li class="nav-item  "> <a href="{{ route('sort.job.benefits') }}" class="nav-link "> <span class="title">Sort Job Benefits</span> </a> </li>
    </ul>
</li>