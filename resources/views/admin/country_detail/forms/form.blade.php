{!! APFrmErrHelp::showErrorsNotice($errors) !!}
@include('flash::message')
<div class="form-body">        
    {!! Form::hidden('id', null) !!}
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'country_id') !!}">
        {!! Form::label('country_id', __('Country ID'), ['class' => 'bold']) !!}
        {!! Form::text('country_id', $country_id, array('class'=>'form-control', 'id'=>'country_id', 'placeholder'=>__('Country ID'), 'readonly'=>true)) !!}
        {!! APFrmErrHelp::showErrors($errors, 'country_id') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'country_name') !!}">
        {!! Form::label('country_name', __('Country Name'), ['class' => 'bold']) !!}
        {!! Form::text('country_name', $country_name, array('class'=>'form-control', 'id'=>'country_name', 'placeholder'=>__('Country Name'), 'readonly'=>true)) !!}
        {!! APFrmErrHelp::showErrors($errors, 'country_name') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'phone_code') !!}">
        {!! Form::label('phone_code', __('Phone Code'), ['class' => 'bold']) !!}
        {!! Form::text('phone_code', null, array('class'=>'form-control', 'id'=>'phone_code', 'placeholder'=>__('Phone Code'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'phone_code') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'currency') !!}">
        {!! Form::label('currency', __('Currency'), ['class' => 'bold']) !!}
        {!! Form::text('currency', null, array('class'=>'form-control', 'id'=>'currency', 'placeholder'=>__('Currency'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'currency') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'code') !!}">
        {!! Form::label('code', __('Code'), ['class' => 'bold']) !!}
        {!! Form::text('code', null, array('class'=>'form-control', 'id'=>'code', 'placeholder'=>__('Code'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'code') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'symbol') !!}">
        {!! Form::label('symbol', __('Symbol'), ['class' => 'bold']) !!}
        {!! Form::text('symbol', null, array('class'=>'form-control', 'id'=>'symbol', 'placeholder'=>__('Symbol'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'symbol') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'thousand_separator') !!}">
        {!! Form::label('thousand_separator', __('Thousand Separator'), ['class' => 'bold']) !!}
        {!! Form::text('thousand_separator', null, array('class'=>'form-control', 'id'=>'thousand_separator', 'placeholder'=>__('Thousand Separator'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'thousand_separator') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'decimal_separator') !!}">
        {!! Form::label('decimal_separator', __('Decimal Separator'), ['class' => 'bold']) !!}
        {!! Form::text('decimal_separator', null, array('class'=>'form-control', 'id'=>'decimal_separator', 'placeholder'=>__('Decimal Separator'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'decimal_separator') !!}
    </div>	
    <div class="form-actions">
        {!! Form::button(__('Update').' <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>', array('class'=>'btn btn-large btn-primary', 'type'=>'submit')) !!}
    </div>
</div>
