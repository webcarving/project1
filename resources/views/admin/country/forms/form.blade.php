<?php
$lang = config('default_lang');
if (isset($country))
    $lang = $country->lang;
$lang = MiscHelper::getLang($lang);
$direction = MiscHelper::getLangDirection($lang);
$queryString = MiscHelper::getLangQueryStr();
?>
{!! APFrmErrHelp::showErrorsNotice($errors) !!}
@include('flash::message')
<div class="form-body">
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'lang') !!}">
        {!! Form::label('lang', __('Language'), ['class' => 'bold']) !!}
        {!! Form::select('lang', ['' => __('Select Language').' '.__('(required)')]+$languages, $lang, ['class' => 'form-control select-multiple-language','style' => 'width:100%','onchange'=>'setLang(this.value)']) !!}
        {!! APFrmErrHelp::showErrors($errors, 'lang') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'country') !!}">
        {!! Form::label('country', __('Country'), ['class' => 'bold']) !!}
        {!! Form::text('country', null, array('class'=>'form-control', 'id'=>'country', 'placeholder'=>__('Country').' '.__('(required)'), 'dir'=>$direction)) !!}
        {!! APFrmErrHelp::showErrors($errors, 'country') !!}                                       
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'nationality') !!}">
        {!! Form::label('nationality', __('Nationality'), ['class' => 'bold']) !!}
        {!! Form::text('nationality', null, array('class'=>'form-control', 'id'=>'nationality', 'placeholder'=>__('Nationality'), 'dir'=>$direction)) !!}
        {!! APFrmErrHelp::showErrors($errors, 'nationality') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'code') !!}">
        {!! Form::label('code', __('Country Code'), ['class' => 'bold']) !!}
        {!! Form::text('code', null, array('class'=>'form-control', 'id'=>'code', 'placeholder'=>__('Country Code').' '.__('(required)'), 'dir'=>$direction)) !!}
        {!! APFrmErrHelp::showErrors($errors, 'code') !!}
    </div>
<?php /*    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'is_default') !!}">
        {!! Form::label('is_default', __('Is Default?'), ['class' => 'bold']) !!}
        <div class="radio-list">
            <?php
            $is_default_1 = 'checked="checked"';
            $is_default_2 = '';
            if (old('is_default', ((isset($country)) ? $country->is_default : 1)) == 0) {
                $is_default_1 = '';
                $is_default_2 = 'checked="checked"';
            }
            ?>
            <label class="radio-inline">
                <input id="default" name="is_default" type="radio" value="1" {{$is_default_1}} onchange="showHideCountryId();">
                {{__('Yes')}} </label>
            <label class="radio-inline">
                <input id="not_default" name="is_default" type="radio" value="0" {{$is_default_2}} onchange="showHideCountryId();">
                {{__('No')}} </label>
        </div>
        {!! APFrmErrHelp::showErrors($errors, 'is_default') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'country_id') !!}" id="country_id_div">
        {!! Form::label('country_id', __('Default Country'), ['class' => 'bold']) !!}
        {!! Form::select('country_id', ['' => __('Select Default Country').' '.__('(required)')]+$countries, null, array('class'=>'form-control select-multiple-default-country','style' => 'width:100%', 'id'=>'country_id')) !!}
        {!! APFrmErrHelp::showErrors($errors, 'country_id') !!}                                       
    </div> */ ?>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'is_active') !!}">
        {!! Form::label('is_active', __('Is Active?'), ['class' => 'bold']) !!}
        <div class="radio-list">
            <?php
            $is_active_1 = 'checked="checked"';
            $is_active_2 = '';
            if (old('is_active', ((isset($country)) ? $country->is_active : 1)) == 0) {
                $is_active_1 = '';
                $is_active_2 = 'checked="checked"';
            }
            ?>
            <label class="radio-inline">
                <input id="active" name="is_active" type="radio" value="1" {{$is_active_1}}>
                {{__('Yes')}} </label>
            <label class="radio-inline">
                <input id="not_active" name="is_active" type="radio" value="0" {{$is_active_2}}>
                {{__('No')}} </label>
        </div>
        {!! APFrmErrHelp::showErrors($errors, 'is_active') !!}
    </div>
    <div class="form-actions">
        {!! Form::button(__($action). ' <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>', array('class'=>'btn btn-large btn-primary', 'type'=>'submit')) !!}
    </div>
</div>
@push('scripts')
<script type="text/javascript">
    function setLang(lang) {
        window.location.href = "<?php echo url(Request::url()) . $queryString; ?>" + lang;
    }
<?php /*    function showHideCountryId() {
        $('#country_id_div').hide();
        var is_default = $("input[name='is_default']:checked").val();
        if (is_default == 0) {
            $('#country_id_div').show();
        }
    }
    showHideCountryId(); */?>
    $(document).ready(function () {
        $('.select-multiple-language').select2({
            placeholder: "{{__('Select Language').' '.__('(required)')}}",
//            allowClear: true
        });
		
		$('.select-multiple-default-country').select2({
			placeholder: "{{__('Select Default Country').' '.__('(required)')}}",
			//            allowClear: true
		});
		
    });
</script>
@endpush