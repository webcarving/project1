{!! APFrmErrHelp::showErrorsNotice($errors) !!}
@include('flash::message')
<div class="form-body">
    <h3>Drag and Drop to Sort Countries</h3>
    {!! Form::select('lang', ['' => __('Select Language')]+$languages, config('default_lang'), array('class'=>'form-control select-multiple-language','style' => 'width:100%', 'id'=>'lang', 'onchange'=>'refreshCountrySortData();')) !!}
    <div id="countrySortDataDiv"></div>
</div>
@push('scripts') 
<script>
    $(document).ready(function () {
        refreshCountrySortData();
        $('.select-multiple-language').select2({
            placeholder: "{{__('Select Language')}}",
//            allowClear: true
        });
    });
    function refreshCountrySortData() {
        var language = $('#lang').val();
        $.ajax({
            type: "GET",
            url: "{{ route('country.sort.data') }}",
            data: {lang: language},
            success: function (responseData) {
                $("#countrySortDataDiv").html('');
                $("#countrySortDataDiv").html(responseData);
                /**************************/
                $('#sortable').sortable({
                    update: function (event, ui) {
                        var countryOrder = $(this).sortable('toArray').toString();
                        $.post("{{ route('country.sort.update') }}", {countryOrder: countryOrder, _method: 'PUT', _token: '{{ csrf_token() }}'})
                    }
                });
                $("#sortable").disableSelection();
                /***************************/
            }
        });
    }
</script> 
@endpush
