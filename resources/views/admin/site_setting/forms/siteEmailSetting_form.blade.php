{!! APFrmErrHelp::showErrorsNotice($errors) !!}
@include('flash::message')
<div class="form-body">
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'mail_driver') !!}">
        {!! Form::label('mail_driver', __('Mail Driver'), ['class' => 'bold']) !!}
        {!! Form::select('mail_driver', ['' => __('Select Mail Driver').' '.__('(required)')]+$mail_drivers, null, ['class' => 'form-control select-multiple-mail-driver','style' => 'width:100%']) !!}
        {!! APFrmErrHelp::showErrors($errors, 'mail_driver') !!}
    </div>
    <br>
    <fieldset>
        <legend>SMTP Settings:</legend>    
        <div class="form-group {!! APFrmErrHelp::hasError($errors, 'mail_host') !!}">
            {!! Form::label('mail_host', __('Mail Host'), ['class' => 'bold']) !!}
            {!! Form::text('mail_host', null, array('class'=>'form-control', 'id'=>'mail_host', 'placeholder'=>__('Mail Host'))) !!}
            {!! APFrmErrHelp::showErrors($errors, 'mail_host') !!}                                       
        </div>    
        <div class="form-group {!! APFrmErrHelp::hasError($errors, 'mail_port') !!}">
            {!! Form::label('mail_port', __('Mail Port'), ['class' => 'bold']) !!}
            {!! Form::text('mail_port', null, array('class'=>'form-control', 'id'=>'mail_port', 'placeholder'=>__('Mail Port'))) !!}
            {!! APFrmErrHelp::showErrors($errors, 'mail_port') !!}                                       
        </div>    
        <div class="form-group {!! APFrmErrHelp::hasError($errors, 'mail_encryption') !!}">
            {!! Form::label('mail_encryption', __('Mail Encryption'), ['class' => 'bold']) !!}
            {!! Form::text('mail_encryption', null, array('class'=>'form-control', 'id'=>'mail_encryption', 'placeholder'=>__('Mail Encryption'))) !!}
            {!! APFrmErrHelp::showErrors($errors, 'mail_encryption') !!}                                       
        </div>
        <div class="form-group {!! APFrmErrHelp::hasError($errors, 'mail_username') !!}">
            {!! Form::label('mail_username', __('Mail Username'), ['class' => 'bold']) !!}
            {!! Form::text('mail_username', null, array('class'=>'form-control', 'id'=>'mail_username', 'placeholder'=>__('Mail Username'))) !!}
            {!! APFrmErrHelp::showErrors($errors, 'mail_username') !!}                                       
        </div>
        <div class="form-group {!! APFrmErrHelp::hasError($errors, 'mail_password') !!}">
            {!! Form::label('mail_password', __('Mail Password'), ['class' => 'bold']) !!}
            {!! Form::text('mail_password', null, array('class'=>'form-control', 'id'=>'mail_password', 'placeholder'=>__('Mail Password'))) !!}
            {!! APFrmErrHelp::showErrors($errors, 'mail_password') !!}                                       
        </div>
    </fieldset>
    <br> 
    <fieldset>
        <legend>SendMail - Pretend Settings:</legend>     
        <div class="form-group {!! APFrmErrHelp::hasError($errors, 'mail_sendmail') !!}">
            {!! Form::label('mail_sendmail', __('Mail Sendmail'), ['class' => 'bold']) !!}
            {!! Form::text('mail_sendmail', null, array('class'=>'form-control', 'id'=>'mail_sendmail', 'placeholder'=>__('Mail Sendmail'))) !!}
            {!! APFrmErrHelp::showErrors($errors, 'mail_sendmail') !!}                                       
        </div>
        <div class="form-group {!! APFrmErrHelp::hasError($errors, 'mail_pretend') !!}">
            {!! Form::label('mail_pretend', __('Mail Pretend'), ['class' => 'bold']) !!}
            {!! Form::text('mail_pretend', null, array('class'=>'form-control', 'id'=>'mail_pretend', 'placeholder'=>__('Mail Pretend'))) !!}
            {!! APFrmErrHelp::showErrors($errors, 'mail_pretend') !!}                                       
        </div>
    </fieldset>    
    <br>
    <fieldset>
        <legend>MailGun Settings:</legend>    
        <div class="form-group {!! APFrmErrHelp::hasError($errors, 'mailgun_domain') !!}">
            {!! Form::label('mailgun_domain', __('Mailgun Domain'), ['class' => 'bold']) !!}
            {!! Form::text('mailgun_domain', null, array('class'=>'form-control', 'id'=>'mailgun_domain', 'placeholder'=>__('Mailgun Domain'))) !!}
            {!! APFrmErrHelp::showErrors($errors, 'mailgun_domain') !!}                                       
        </div>
        <div class="form-group {!! APFrmErrHelp::hasError($errors, 'mailgun_secret') !!}">
            {!! Form::label('mailgun_secret', __('Mailgun Secret'), ['class' => 'bold']) !!}
            {!! Form::text('mailgun_secret', null, array('class'=>'form-control', 'id'=>'mailgun_secret', 'placeholder'=>__('Mailgun Secret'))) !!}
            {!! APFrmErrHelp::showErrors($errors, 'mailgun_secret') !!}                                       
        </div>
    </fieldset>
    <br>
    <fieldset>
        <legend>Mandrill Settings:</legend>    
        <div class="form-group {!! APFrmErrHelp::hasError($errors, 'mandrill_secret') !!}">
            {!! Form::label('mandrill_secret', __('Mandrill Secret'), ['class' => 'bold']) !!}
            {!! Form::text('mandrill_secret', null, array('class'=>'form-control', 'id'=>'mandrill_secret', 'placeholder'=>__('Mandrill Secret'))) !!}
            {!! APFrmErrHelp::showErrors($errors, 'mandrill_secret') !!}                                       
        </div>
    </fieldset>
    <br>
    <fieldset>
        <legend>Sparkpost Settings:</legend>    
        <div class="form-group {!! APFrmErrHelp::hasError($errors, 'sparkpost_secret') !!}">
            {!! Form::label('sparkpost_secret', __('Sparkpost Secret'), ['class' => 'bold']) !!}
            {!! Form::text('sparkpost_secret', null, array('class'=>'form-control', 'id'=>'sparkpost_secret', 'placeholder'=>__('Sparkpost Secret'))) !!}
            {!! APFrmErrHelp::showErrors($errors, 'sparkpost_secret') !!}                                       
        </div>
    </fieldset>
    <br>
    <fieldset>
        <legend>AMAZON SES Settings:</legend>        
        <div class="form-group {!! APFrmErrHelp::hasError($errors, 'ses_key') !!}">
            {!! Form::label('ses_key', __('SES Key'), ['class' => 'bold']) !!}
            {!! Form::text('ses_key', null, array('class'=>'form-control', 'id'=>'ses_key', 'placeholder'=>__('SES Key'))) !!}
            {!! APFrmErrHelp::showErrors($errors, 'ses_key') !!}                                       
        </div>
        <div class="form-group {!! APFrmErrHelp::hasError($errors, 'ses_secret') !!}">
            {!! Form::label('ses_secret', __('SES Secret'), ['class' => 'bold']) !!}
            {!! Form::text('ses_secret', null, array('class'=>'form-control', 'id'=>'ses_secret', 'placeholder'=>__('SES Secret'))) !!}
            {!! APFrmErrHelp::showErrors($errors, 'ses_secret') !!}                                       
        </div>
        <div class="form-group {!! APFrmErrHelp::hasError($errors, 'ses_region') !!}">
            {!! Form::label('ses_region', __('SES Region'), ['class' => 'bold']) !!}
            {!! Form::text('ses_region', null, array('class'=>'form-control', 'id'=>'ses_region', 'placeholder'=>__('SES Region'))) !!}
            {!! APFrmErrHelp::showErrors($errors, 'ses_region') !!}                                       
        </div>
    </fieldset>   
</div>
@push('scripts')
    <script type="text/javascript">
        $(document).ready(function () {
            $('.select-multiple-mail-driver').select2({
                placeholder: "{{__('Select Mail Driver').' '.__('(required)')}}",
//            allowClear: true
            });
        });
    </script>
@endpush
