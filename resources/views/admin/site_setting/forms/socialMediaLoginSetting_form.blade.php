{!! APFrmErrHelp::showErrorsNotice($errors) !!}
@include('flash::message')
<div class="form-body">        
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'facebook_app_id') !!}">
        {!! Form::label('facebook_app_id', __('Facebook App ID'), ['class' => 'bold']) !!}
        {!! Form::text('facebook_app_id', null, array('class'=>'form-control', 'id'=>'facebook_app_id', 'placeholder'=>__('Facebook App ID'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'facebook_app_id') !!}                                       
    </div>    
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'facebook_app_secret') !!}">
        {!! Form::label('facebook_app_secret', __('Facebook App Secret'), ['class' => 'bold']) !!}
        {!! Form::text('facebook_app_secret', null, array('class'=>'form-control', 'id'=>'facebook_app_secret', 'placeholder'=>__('facebook App Secret'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'facebook_app_secret') !!}
    </div>    
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'twitter_app_id') !!}">
        {!! Form::label('twitter_app_id', __('Twitter App ID'), ['class' => 'bold']) !!}
        {!! Form::text('twitter_app_id', null, array('class'=>'form-control', 'id'=>'twitter_app_id', 'placeholder'=>__('Twitter App ID'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'twitter_app_id') !!}                                       
    </div>    
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'twitter_app_secret') !!}">
        {!! Form::label('twitter_app_secret', __('Twitter App Secret'), ['class' => 'bold']) !!}
        {!! Form::text('twitter_app_secret', null, array('class'=>'form-control', 'id'=>'twitter_app_secret', 'placeholder'=>__('Twitter App Secret'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'twitter_app_secret') !!}                                       
    </div>
</div>
