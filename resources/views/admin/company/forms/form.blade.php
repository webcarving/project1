{!! APFrmErrHelp::showOnlyErrorsNotice($errors) !!}
@include('flash::message')
<div class="form-body">
    <div class="row">
        <div class="col-md-6">
            <div class="form-group {!! APFrmErrHelp::hasError($errors, 'logo') !!}">
                <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img src="{{ asset('/') }}admin_assets/no-image.png" alt="" /> </div>
                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                    <div> <span class="btn default btn-file"> <span class="fileinput-new"> Select Company logo (required)</span> <span class="fileinput-exists"> Change </span> {!! Form::file('logo', null, array('id'=>'logo')) !!} </span> <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> Remove </a> </div>
                </div>
                {!! APFrmErrHelp::showErrors($errors, 'logo') !!} </div>
        </div>
        @if(isset($company))
        <div class="col-md-6">
            {{ ImgUploader::print_image("company_logos/$company->logo") }}        
        </div>    
        @endif  
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'name') !!}"> 
		{!! Form::label('name', __('Company Name'), ['class' => 'bold']) !!}
        {!! Form::text('name', null, array('class'=>'form-control', 'id'=>'name', 'placeholder'=>__('Company Name').' '.__('(required)'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'name') !!} </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'email') !!}"> 
		{!! Form::label('email', __('Company Email'), ['class' => 'bold']) !!}
        {!! Form::text('email', null, array('class'=>'form-control', 'id'=>'email', 'placeholder'=>__('Company Email').' '.__('(required)'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'email') !!} </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'password') !!}"> 
		{!! Form::label('password', __('Password'), ['class' => 'bold']) !!}
        {!! Form::password('password', array('class'=>'form-control', 'id'=>'password', 'placeholder'=>__('Password').' '.__('(required)'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'password') !!} </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'ceo') !!}"> 
		{!! Form::label('ceo', __('Company CEO'), ['class' => 'bold']) !!}
        {!! Form::text('ceo', null, array('class'=>'form-control', 'id'=>'ceo', 'placeholder'=>__('Company CEO').' '.__('(required)'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'ceo') !!} </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'industry_id') !!}">
        {!! Form::label('industry_id', __('Industry'), ['class' => 'bold']) !!}
        {!! Form::select('industry_id', ['' => __('Select Industry').' '.__('(required)')]+$industries, null, ['class' => 'form-control select-multiple-industry-type','style' => 'width:100%']) !!}
        {!! APFrmErrHelp::showErrors($errors, 'industry_id') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'ownership_type_id') !!}">
        {!! Form::label('ownership_type_id', __('Ownership Type'), ['class' => 'bold']) !!}
        {!! Form::select('ownership_type_id', ['' => __('Select Ownership Type').' '.__('(required)')]+$ownershipTypes, null, ['class' => 'form-control select-multiple-ownership-type','style' => 'width:100%']) !!}
        {!! APFrmErrHelp::showErrors($errors, 'ownership_type_id') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'description') !!}"> 
		{!! Form::label('description', __('Company details'), ['class' => 'bold']) !!}
        {!! Form::textarea('description', null, array('class'=>'form-control', 'id'=>'description', 'placeholder'=>__('Company details').' '.__('(required)'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'description') !!} </div>
<?php /*    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'location') !!}">
		{!! Form::label('location', __('Location'), ['class' => 'bold']) !!}
        {!! Form::text('location', null, array('class'=>'form-control', 'id'=>'location', 'placeholder'=>__('Location').' '.__('(required)'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'location') !!} </div> */ ?>

<?php /*     <div class="form-group {!! APFrmErrHelp::hasError($errors, 'longitude') !!}"> {!! Form::label('longitude', 'Longitude', ['class' => 'bold']) !!}
        {!! Form::text('longitude', null, array('class'=>'form-control', 'id'=>'longitude', 'placeholder'=>'Longitude')) !!}
        {!! APFrmErrHelp::showErrors($errors, 'longitude') !!} </div> 

         <div class="form-group {!! APFrmErrHelp::hasError($errors, 'latitude') !!}"> {!! Form::label('latitude', 'Latitude', ['class' => 'bold']) !!}
        {!! Form::text('latitude', null, array('class'=>'form-control', 'id'=>'latitude', 'placeholder'=>'Latitude')) !!}
        {!! APFrmErrHelp::showErrors($errors, 'latitude') !!} </div> */ ?>

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'no_of_offices') !!}">
        {!! Form::label('no_of_offices', __('Number of Offices'), ['class' => 'bold']) !!}
        {!! Form::select('no_of_offices', ['' => __('Select Number of Offices').' '.__('(required)')]+MiscHelper::getNumOffices(), null, ['class' => 'form-control select-multiple-number-of-offices','style' => 'width:100%']) !!}
        {!! APFrmErrHelp::showErrors($errors, 'no_of_offices') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'website') !!}">
        {!! Form::label('website', __('Website'), ['class' => 'bold']) !!}
        {!! Form::text('website', null, array('class'=>'form-control', 'id'=>'website', 'placeholder'=>__('Website').' '.__('(required)'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'website') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'no_of_employees') !!}">
        {!! Form::label('no_of_employees', __('Number of Employees'), ['class' => 'bold']) !!}
        {!! Form::select('no_of_employees', ['' => __('Select Number of Employees').' '.__('(required)')]+MiscHelper::getNumEmployees(), null, ['class' => 'form-control select-multiple-number-of-employees','style' => 'width:100%']) !!}
        {!! APFrmErrHelp::showErrors($errors, 'no_of_employees') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'established_in') !!}">
        {!! Form::label('established_in', __('Established In'), ['class' => 'bold']) !!}
        {!! Form::select('established_in', ['' => __('Select Established In').' '.__('(required)')]+MiscHelper::getEstablishedIn(), null, ['class' => 'form-control select-multiple-established-in','style' => 'width:100%']) !!}
        {!! APFrmErrHelp::showErrors($errors, 'established_in') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'fax') !!}"> 
		{!! Form::label('fax', __('Fax'), ['class' => 'bold']) !!}
        {!! Form::text('fax', null, array('class'=>'form-control', 'id'=>'fax', 'placeholder'=>__('Fax').' '.__('(required)'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'fax') !!} </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'phone') !!}"> 
		{!! Form::label('phone', __('Phone'), ['class' => 'bold']) !!}
        {!! Form::text('phone', null, array('class'=>'form-control', 'id'=>'phone', 'placeholder'=>__('Phone').' '.__('(required)'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'phone') !!} </div>




    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'facebook') !!}"> 
		{!! Form::label('facebook', __('Facebook'), ['class' => 'bold']) !!}
        {!! Form::text('facebook', null, array('class'=>'form-control', 'id'=>'facebook', 'placeholder'=>__('Facebook'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'facebook') !!} </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'twitter') !!}"> 
		{!! Form::label('twitter', __('Twitter'), ['class' => 'bold']) !!}
        {!! Form::text('twitter', null, array('class'=>'form-control', 'id'=>'twitter', 'placeholder'=>__('Twitter'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'twitter') !!} </div>

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'linkedin') !!}"> 
		{!! Form::label('linkedin', __('Linkedin'), ['class' => 'bold']) !!}
        {!! Form::text('linkedin', null, array('class'=>'form-control', 'id'=>'linkedin', 'placeholder'=>__('Linkedin'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'linkedin') !!} </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'google_plus') !!}"> 
		{!! Form::label('google_plus', __('Google+'), ['class' => 'bold']) !!}
        {!! Form::text('google_plus', null, array('class'=>'form-control', 'id'=>'google_plus', 'placeholder'=>__('Google+'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'google_plus') !!} </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'pinterest') !!}"> 
		{!! Form::label('pinterest', __('Pinterest'), ['class' => 'bold']) !!}
        {!! Form::text('pinterest', null, array('class'=>'form-control', 'id'=>'pinterest', 'placeholder'=>__('Pinterest'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'pinterest') !!} </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'country_id') !!}">
        {!! Form::label('country_id', __('Country'), ['class' => 'bold']) !!}
        {!! Form::select('country_id', ['' => __('Select Country').' '.__('(required)')]+$countries, old('country_id', (isset($company))? $company->country_id:$siteSetting->default_country_id), ['class' => 'form-control select-multiple-country','style' => 'width:100%']) !!}
        {!! APFrmErrHelp::showErrors($errors, 'country_id') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'state_id') !!}">
        {!! Form::label('state_id', __('State'), ['class' => 'bold']) !!}
        <span id="default_state_dd">
        {!! Form::select('state_id', ['' => __('Select State').' '.__('(required)')], null, ['class' => 'form-control select-multiple-state','style' => 'width:100%']) !!}
        </span>
        {!! APFrmErrHelp::showErrors($errors, 'state_id') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'city_id') !!}">
        {!! Form::label('city_id', __('City'), ['class' => 'bold']) !!}
        <span id="default_city_dd">
        {!! Form::select('city_id', ['' => __('Select City').' '.__('(required)')], null, ['class' => 'form-control select-multiple-city','style' => 'width:100%']) !!}
        </span>
        {!! APFrmErrHelp::showErrors($errors, 'city_id') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'company_package_id') !!}">
        {!! Form::label('company_package_id', __('Package'), ['class' => 'bold']) !!}
        {!! Form::select('company_package_id', ['' => __('Select Package').' '.__('(required)')]+$packages, null, ['class' => 'form-control select-multiple-package','style' => 'width:100%']) !!}
        {!! APFrmErrHelp::showErrors($errors, 'company_package_id') !!}
    </div>
    @if(isset($company) && $company->package_id > 0)
    <div class="form-group">
        {!! Form::label('package', __('Package'), ['class' => 'bold']) !!}         
        <strong>{{$company->getPackage('package_title')}}</strong>
    </div>
    <div class="form-group">
        {!! Form::label('package_Duration', __('Package Duration'), ['class' => 'bold']) !!}
        <strong>{{$company->package_start_date->format('d M, Y')}}</strong> - <strong>{{$company->package_end_date->format('d M, Y')}}</strong>
    </div>
    <div class="form-group">
        {!! Form::label('package_quota', __('Availed quota'), ['class' => 'bold']) !!}
        <strong>{{$company->availed_jobs_quota}}</strong> / <strong>{{$company->jobs_quota}}</strong>
    </div>
    <hr/>
    @endif

    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'is_active') !!}">
        {!! Form::label('is_active', __('Is Active?'), ['class' => 'bold']) !!}
        <div class="radio-list">
            <?php
            $is_active_1 = 'checked="checked"';
            $is_active_2 = '';
            if (old('is_active', ((isset($company)) ? $company->is_active : 1)) == 0) {
                $is_active_1 = '';
                $is_active_2 = 'checked="checked"';
            }
            ?>
            <label class="radio-inline">
                <input id="active" name="is_active" type="radio" value="1" {{$is_active_1}}>
                {{__('Yes')}} </label>
            <label class="radio-inline">
                <input id="not_active" name="is_active" type="radio" value="0" {{$is_active_2}}>
                {{__('No')}} </label>
        </div>
        {!! APFrmErrHelp::showErrors($errors, 'is_active') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'is_featured') !!}">
        {!! Form::label('is_featured', __('Is Featured?'), ['class' => 'bold']) !!}
        <div class="radio-list">
            <?php
            $is_featured_1 = '';
            $is_featured_2 = 'checked="checked"';
            if (old('is_featured', ((isset($company)) ? $company->is_featured : 0)) == 1) {
                $is_featured_1 = 'checked="checked"';
                $is_featured_2 = '';
            }
            ?>
            <label class="radio-inline">
                <input id="featured" name="is_featured" type="radio" value="1" {{$is_featured_1}}>
                {{__('Yes')}} </label>
            <label class="radio-inline">
                <input id="not_featured" name="is_featured" type="radio" value="0" {{$is_featured_2}}>
                {{__('No')}} </label>
        </div>
        {!! APFrmErrHelp::showErrors($errors, 'is_featured') !!} 
    </div>
     <div class="form-group {!! APFrmErrHelp::hasError($errors, 'is_top_employer') !!}">
        {!! Form::label('is_top_employer', __('Is Top Employer?'), ['class' => 'bold']) !!}
        <div class="radio-list">
            <?php
            $is_top_employer_1 = '';
            $is_top_employer_2 = 'checked="checked"';
            if (old('is_top_employer', ((isset($company)) ? $company->is_top_employer : 0)) == 1) {
                $is_top_employer_1 = 'checked="checked"';
                $is_top_employer_2 = '';
            }
            ?>
            <label class="radio-inline">
                <input id="top_employer" name="is_top_employer" type="radio" value="1" {{$is_top_employer_1}}>
                {{__('Yes')}} </label>
            <label class="radio-inline">
                <input id="not_top_employer" name="is_top_employer" type="radio" value="0" {{$is_top_employer_2}}>
                {{__('No')}} </label>
        </div>
        {!! APFrmErrHelp::showErrors($errors, 'is_top_employer') !!}
    </div>
    <div class="form-actions"> 
		{!! Form::button(__($action).' <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>', array('class'=>'btn btn-large btn-primary', 'type'=>'submit')) !!} </div>
</div>
@push('scripts')
@include('admin.shared.tinyMCEFront') 
<script type="text/javascript">
    $(document).ready(function () {
        $('#country_id').on('change', function (e) {
            e.preventDefault();
            filterDefaultStates(0);
        });
        $(document).on('change', '#state_id', function (e) {
            e.preventDefault();
            filterDefaultCities(0);
        });
        filterDefaultStates(<?php echo old('state_id', (isset($company)) ? $company->state_id : 0); ?>);

        $('.select-multiple-industry-type').select2({
            placeholder: "{{__('Select Industry Type').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-ownership-type').select2({
            placeholder: "{{__('Select Ownership Type').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-number-of-offices').select2({
            placeholder: "{{__('Select Number of Offices').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-number-of-employees').select2({
            placeholder: "{{__('Select Number of Employees').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-established-in').select2({
            placeholder: "{{__('Select Established in').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-country').select2({
            placeholder: "{{__('Select Country').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-state').select2({
            placeholder: "{{__('Select State').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-city').select2({
            placeholder: "{{__('Select City').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-package').select2({
            placeholder: "{{__('Select Package').' '.__('(required)')}}",
//            allowClear: true
        });

    });
    function filterDefaultStates(state_id)
    {
        var country_id = $('#country_id').val();
        if (country_id != '') {
            $.post("{{ route('filter.default.states.dropdown') }}", {country_id: country_id, state_id: state_id, _method: 'POST', _token: '{{ csrf_token() }}'})
                    .done(function (response) {
                        $('#default_state_dd').html(response);
                        filterDefaultCities(<?php echo old('city_id', (isset($company)) ? $company->city_id : 0); ?>);
                        $('.select-multiple-state').select2({
                            placeholder: "{{__('Select State').' '.__('(required)')}}",
//            allowClear: true
                        });
                    });
        }
    }
    function filterDefaultCities(city_id)
    {
        var state_id = $('#state_id').val();
        if (state_id != '') {
            $.post("{{ route('filter.default.cities.dropdown') }}", {state_id: state_id, city_id: city_id, _method: 'POST', _token: '{{ csrf_token() }}'})
                    .done(function (response) {
                        $('#default_city_dd').html(response);
                        $('.select-multiple-city').select2({
                            placeholder: "{{__('Select City').' '.__('(required)')}}",
//            allowClear: true
                        });

                    });
        }
    }
</script>
@endpush