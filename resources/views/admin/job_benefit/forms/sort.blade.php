{!! APFrmErrHelp::showErrorsNotice($errors) !!}
@include('flash::message')
<div class="form-body">
    <h3>Drag and Drop to Sort Job Benefits</h3>
    {!! Form::select('lang', ['' => __('Select Language')]+$languages, config('default_lang'), array('class'=>'form-control select-multiple-language','style' => 'width:100%', 'id'=>'lang', 'onchange'=>'refreshJobBenefitSortData();')) !!}
    <div id="jobBenefitSortDataDiv"></div>
</div>
@push('scripts') 
<script>
    $(document).ready(function () {
        refreshJobBenefitSortData();
        $('.select-multiple-language').select2({
            placeholder: "{{__('Select Language')}}",
//            allowClear: true
        });
    });
    function refreshJobBenefitSortData() {
        var language = $('#lang').val();
        $.ajax({
            type: "GET",
            url: "{{ route('job.benefit.sort.data') }}",
            data: {lang: language},
            success: function (responseData) {
                $("#jobBenefitSortDataDiv").html('');
                $("#jobBenefitSortDataDiv").html(responseData);
                /**************************/
                $('#sortable').sortable({
                    update: function (event, ui) {
                        var jobBenefitOrder = $(this).sortable('toArray').toString();
                        $.post("{{ route('job.benefit.sort.update') }}", {jobBenefitOrder: jobBenefitOrder, _method: 'PUT', _token: '{{ csrf_token() }}'})
                    }
                });
                $("#sortable").disableSelection();
                /***************************/
            }
        });
    }
</script> 
@endpush
