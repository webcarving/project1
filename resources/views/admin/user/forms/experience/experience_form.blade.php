<div class="modal-body">
    <div class="form-body" id="experience">
        <div class="form-group" id="div_title">
            <label for="title" class="bold">Experience Title</label>
            <input class="form-control" id="title" placeholder="Experience Title (required)" name="title" type="text" value="{{(isset($profileExperience)? $profileExperience->title:'')}}">
            <span class="help-block title-error"></span> </div>

        <div class="form-group" id="div_company">
            <label for="company" class="bold">Company</label>
            <input class="form-control" id="company" placeholder="Company (required)" name="company" type="text" value="{{(isset($profileExperience)? $profileExperience->company:'')}}">
            <span class="help-block company-error"></span> </div>
 <div class="formrow" id="div_career_levels_id">
            <label for="level_id" class="bold"> Level</label>
            <?php
            $career_levels_id=(isset($profileExperience->career_levels_id)) ? $profileExperience->career_levels_id : '';
               ?>
           {!! Form::select('career_levels_id', [''=>__('Select Career Level').' '.__('(required)')]+$careerLevels, $career_levels_id, array('class'=>'form-control select-multiple-career-level','style' => 'width:100%', 'id'=>'career_levels_id')) !!}
            <span class="help-block career_levels_id-error"></span> </div>

<div class="form-group" id="div_industry_id">
            <label for="industry_id" class="bold">Industry</label>
            <?php
            $industry_id = (isset($profileExperience->industry_id) ? $profileExperience->industry_id : '');
            ?>
            {!! Form::select('industry_id', ['' => __('Select Industry').' '.__('(required)')]+$industries, $industry_id, array('class'=>'form-control select-multiple-industry','style' => 'width:100%', 'id'=>'experience_industry_id')) !!}
            <span class="help-block industry_id-error"></span> </div>
 <div class="formrow" id="div_functional_area_id">
            <label for="functional_id" class="bold" >Functional Area</label>
            <?php
            $functional_area_id=(isset($profileExperience->functional_area_id)) ? $profileExperience->functional_area_id : '';
               ?>
           {!! Form::select('functional_area_id', [''=>__('Select Functional Area').' '.__('(required)')]+$functionalAreas, $functional_area_id, array('class'=>'form-control select-multiple-functional-area','style' => 'width:100%', 'id'=>'functional_area_id')) !!}
            <span class="help-block functional_area_id-error"></span> </div>

        <div class="form-group" id="div_country_id">
            <label for="country_id" class="bold">Country</label>
            <?php
            $country_id = (isset($profileExperience) ? $profileExperience->country_id : $siteSetting->default_country_id);
            ?>
            {!! Form::select('country_id', ['' => __('Select Country').' '.__('(required)')]+$countries, $country_id, array('class'=>'form-control select-multiple-country','style' => 'width:100%', 'id'=>'experience_country_id')) !!}
            <span class="help-block country_id-error"></span> </div>

        <div class="form-group" id="div_state_id">
            <label for="state_id" class="bold">State</label>
			<span id="default_state_dd">
                {!! Form::select('state_id', [ '' => __('Select State').' '.__('(required)')], null, array('class'=>'form-control select-multiple-state','style' => 'width:100%', 'id'=>'experience_state_id')) !!}
            </span>
            <span class="help-block state_id-error"></span> </div>

        <div class="form-group" id="div_city_id">
            <label for="city_id" class="bold">City</label>
            <span id="default_city_dd">
                {!! Form::select('city_id', ['' => __('Select City').' '.__('(required)')], null, array('class'=>'form-control select-multiple-city','style' => 'width:100%', 'id'=>'city_id')) !!}
            </span>
            <span class="help-block city_id-error"></span> </div>

        <div class="form-group" id="div_date_start">
            <label for="date_start" class="bold">Experience Start Date</label>
            <input class="form-control datepicker"  autocomplete="off" id="date_start" placeholder="Experience Start Date (required)" name="date_start" type="text" value="{{(isset($profileExperience)? $profileExperience->date_start->format('Y-m-d'):'')}}">
            <span class="help-block date_start-error"></span> </div>
        <div class="form-group" id="div_date_end">
            <label for="date_end" class="bold">Experience End Date</label>
            <input class="form-control datepicker" autocomplete="off" id="date_end" placeholder="Experience End Date (required)" name="date_end" type="text" value="{{(isset($profileExperience)? $profileExperience->date_end->format('Y-m-d'):'')}}">
            <span class="help-block date_end-error"></span> </div>
        <div class="form-group" id="div_is_currently_working">
            <label for="is_currently_working" class="bold">{{__('Is Currently Working?')}}</label>
            <div class="radio-list" style="margin-left:22px;">
                <?php
                $val_1_checked = '';
                $val_2_checked = 'checked="checked"';

                if (isset($profileExperience) && $profileExperience->is_currently_working == 1) {
                    $val_1_checked = 'checked="checked"';
                    $val_2_checked = '';
                }
                ?> 
                <label class="radio-inline"><input id="currently_working" name="is_currently_working" type="radio" value="1" {{$val_1_checked}}> {{__('Yes')}} </label>
                <label class="radio-inline"><input id="not_currently_working" name="is_currently_working" type="radio" value="0" {{$val_2_checked}}> {{__('No')}} </label>
            </div>
            <span class="help-block is_currently_working-error"></span>
        </div>
        <div class="form-group" id="div_description">
            <label for="name" class="bold">Experience Description</label>
            <textarea name="description" class="form-control" id="description" placeholder="Experience description (required)">{{(isset($profileExperience)? $profileExperience->description:'')}}</textarea>
            <span class="help-block description-error"></span> </div>
    </div>
</div>
<script>
    $(document).ready(function(){
        $('div#experience #not_currently_working').click(function(){
            $('div#experience #date_end').removeAttr("disabled");
        });
        $('div#experience #currently_working').click(function(){
            $('div#experience #date_end').val('');
            $('div#experience #date_end').prop('disabled','disabled');
        });

		$('.select-multiple-career-level').select2({
			placeholder: "{{__('Select Career Level').' '.__('(required)')}}",
			//            allowClear: true
		});

		$('.select-multiple-industry').select2({
			placeholder: "{{__('Select Industry').' '.__('(required)')}}",
			//            allowClear: true
		});
		
		$('.select-multiple-functional-area').select2({
			placeholder: "{{__('Select Functional Area').' '.__('(required)')}}",
			//            allowClear: true
		});
		
		$('.select-multiple-country').select2({
			placeholder: "{{__('Select Country').' '.__('(required)')}}",
			//            allowClear: true
		});

		$('.select-multiple-state').select2({
			placeholder: "{{__('Select State').' '.__('(required)')}}",
			//            allowClear: true
		});
		

		$('.select-multiple-city').select2({
			placeholder: "{{__('Select City').' '.__('(required)')}}",
			//            allowClear: true
		});
		
        $('#country_id').on('change', function (e) {
            e.preventDefault();
            filterDefaultStates(0);
        });
        $(document).on('change', '#state_id', function (e) {
            e.preventDefault();
            filterDefaultCities(0);
        });
        filterDefaultStates(<?php echo old('state_id', (isset($user)) ? $user->state_id : 0); ?>);
		
    });
    function filterDefaultStates(state_id)
    {
        var country_id = $('#country_id').val();
        if (country_id != '') {
            $.post("{{ route('filter.default.states.dropdown') }}", {country_id: country_id, state_id: state_id, _method: 'POST', _token: '{{ csrf_token() }}'})
                    .done(function (response) {
                        $('#default_state_dd').html(response);
                        filterDefaultCities(<?php echo old('city_id', (isset($user)) ? $user->city_id : 0); ?>);
                        $('.select-multiple-state').select2({
                            placeholder: "{{__('Select State').' '.__('(required)')}}",
//            allowClear: true
                        });
                    });
        }
    }
    function filterDefaultCities(city_id)
    {
        var state_id = $('#state_id').val();
        if (state_id != '') {
            $.post("{{ route('filter.default.cities.dropdown') }}", {state_id: state_id, city_id: city_id, _method: 'POST', _token: '{{ csrf_token() }}'})
                    .done(function (response) {
                        $('#default_city_dd').html(response);
                        $('.select-multiple-city').select2({
                            placeholder: "{{__('Select City').' '.__('(required)')}}",
//            allowClear: true
                        });
                    });
        }
    }
	
</script>