{!! APFrmErrHelp::showErrorsNotice($errors) !!}
@include('flash::message')
@if(isset($user))
{!! Form::model($user, array('method' => 'put', 'route' => array('update.user', $user->id), 'class' => 'form', 'files'=>true)) !!}
{!! Form::hidden('id', $user->id) !!}
@else
{!! Form::open(array('method' => 'post', 'route' => 'store.user', 'class' => 'form', 'files'=>true)) !!}
@endif

<div class="form-body">    
    <input type="hidden" name="front_or_admin" value="admin" />
    <div class="row">
        <div class="col-md-6">
            <div class="form-group {!! APFrmErrHelp::hasError($errors, 'image') !!}">
                <div class="fileinput fileinput-new" data-provides="fileinput">
                    <div class="fileinput-new thumbnail" style="width: 200px; height: 150px;"> <img src="{{ asset('/') }}admin_assets/no-image.png" alt="" /> </div>
                    <div class="fileinput-preview fileinput-exists thumbnail" style="max-width: 200px; max-height: 150px;"> </div>
                    <div> <span class="btn default btn-file"> <span class="fileinput-new"> {{__('Select Profile Image')}} </span> <span class="fileinput-exists"> {{__('Change')}} </span> {!! Form::file('image', null, array('id'=>'image')) !!} </span> <a href="javascript:;" class="btn red fileinput-exists" data-dismiss="fileinput"> {{__('Remove')}} </a> </div>
                </div>
                {!! APFrmErrHelp::showErrors($errors, 'image') !!} </div>
        </div>
        @if(isset($user))
        <div class="col-md-6">
            {{ ImgUploader::print_image("user_images/$user->image") }}        
        </div>    
        @endif  
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'password') !!}">
        {!! Form::label('password', __('Password'), ['class' => 'bold']) !!}
        {!! Form::password('password', array('class'=>'form-control', 'id'=>'password', 'placeholder'=>__('Password').' '.__('(required)'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'password') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'first_name') !!}">
        {!! Form::label('first_name', __('First Name'), ['class' => 'bold']) !!}
        {!! Form::text('first_name', null, array('class'=>'form-control', 'id'=>'first_name', 'placeholder'=>__('First Name').' '.__('(required)'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'first_name') !!}                                       
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'middle_name') !!}">
        {!! Form::label('middle_name', __('Middle Name'), ['class' => 'bold']) !!}
        {!! Form::text('middle_name', null, array('class'=>'form-control', 'id'=>'middle_name', 'placeholder'=>__('Middle Name'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'middle_name') !!}                                       
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'last_name') !!}">
        {!! Form::label('last_name', __('Last Name'), ['class' => 'bold']) !!}
        {!! Form::text('last_name', null, array('class'=>'form-control', 'id'=>'last_name', 'placeholder'=>__('Last Name').' '.__('(required)'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'last_name') !!}                                       
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'email') !!}">
        {!! Form::label('email', __('Email'), ['class' => 'bold']) !!}
        {!! Form::text('email', null, array('class'=>'form-control', 'id'=>'email', 'placeholder'=>__('Email').' '.__('(required)'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'email') !!}                                       
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'gender_id') !!}">
        {!! Form::label('gender_id', __('Gender'), ['class' => 'bold']) !!}
        {!! Form::select('gender_id', ['' => __('Select Gender').' '.__('(required)')]+$genders, null, ['class' => 'form-control select-multiple-gender','style' => 'width:100%']) !!}
        {!! APFrmErrHelp::showErrors($errors, 'gender_id') !!}
    </div>
<?php //    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'father_name') !!}">
      //  {!! Form::label('father_name', 'Father Name', ['class' => 'bold']) !!}
      //  {!! Form::text('father_name', null, array('class'=>'form-control', 'id'=>'father_name', 'placeholder'=>'Father Name')) !!}
      //  {!! APFrmErrHelp::showErrors($errors, 'father_name') !!}
    // </div>
?>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'marital_status_id') !!}">
        {!! Form::label('marital_status_id', __('Marital Status'), ['class' => 'bold']) !!}
        {!! Form::select('marital_status_id', ['' => __('Select Marital Status').' '.__('(required)')]+$maritalStatuses, null, ['class' => 'form-control select-multiple-marital-status','style' => 'width:100%']) !!}
        {!! APFrmErrHelp::showErrors($errors, 'marital_status_id') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'country_id') !!}">
        {!! Form::label('country_id', __('Country'), ['class' => 'bold']) !!}
        {!! Form::select('country_id', ['' => __('Select Country').' '.__('(required)')]+$countries, old('country_id', (isset($user))? $user->country_id:$siteSetting->default_country_id), ['class' => 'form-control select-multiple-country','style' => 'width:100%']) !!}
        {!! APFrmErrHelp::showErrors($errors, 'country_id') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'state_id') !!}">
        {!! Form::label('state_id', __('State'), ['class' => 'bold']) !!}
        <span id="default_state_dd">
        {!! Form::select('state_id', ['' => __('Select State').' '.__('(required)')], null, ['class' => 'form-control select-multiple-state','style' => 'width:100%']) !!}
        </span>
        {!! APFrmErrHelp::showErrors($errors, 'state_id') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'city_id') !!}">
        {!! Form::label('city_id', __('City'), ['class' => 'bold']) !!}
        <span id="default_city_dd">
        {!! Form::select('city_id', ['' => __('Select City').' '.__('(required)')], null, ['class' => 'form-control select-multiple-city','style' => 'width:100%']) !!}
        </span>
        {!! APFrmErrHelp::showErrors($errors, 'city_id') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'nationality_id') !!}">
        {!! Form::label('nationality_id', __('Nationality'), ['class' => 'bold']) !!}
        {!! Form::select('nationality_id', ['' => __('Select Nationality').' '.__('(required)')]+$nationalities, null, ['class' => 'form-control select-multiple-nationality','style' => 'width:100%']) !!}
        {!! APFrmErrHelp::showErrors($errors, 'nationality_id') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'date_of_birth') !!}">
        {!! Form::label('date_of_birth', __('Date of Birth'), ['class' => 'bold']) !!}
        @php
            if(isset($user->date_of_birth)){
                $dob = date("M j, Y", strtotime($user->date_of_birth));
            }else{
                $dob = null;
            }
        @endphp
        {!! Form::text('date_of_birth', $dob, array('class'=>'form-control datepicker', 'id'=>'date_of_birth', 'placeholder'=>__('Date of Birth').' '.__('(required)'), 'autocomplete'=>'off')) !!}
        {!! APFrmErrHelp::showErrors($errors, 'date_of_birth') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'national_id_card_number') !!}">
        {!! Form::label('national_id_card_number', __('National ID Card Number'), ['class' => 'bold']) !!}
        {!! Form::text('national_id_card_number', null, array('class'=>'form-control', 'id'=>'national_id_card_number', 'placeholder'=>__('National ID Card Number').' '.__('(required)'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'national_id_card_number') !!}                                       
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'phone') !!}">
        {!! Form::label('phone', __('Phone No'), ['class' => 'bold']) !!}
        {!! Form::text('phone', null, array('class'=>'form-control', 'id'=>'phone', 'placeholder'=>__('Phone No'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'phone') !!}                                       
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'mobile_num') !!}">
        {!! Form::label('mobile_num', __('Mobile Number'), ['class' => 'bold']) !!}
        {!! Form::text('mobile_num', null, array('class'=>'form-control', 'id'=>'mobile_num', 'placeholder'=>__('Mobile Number').' '.__('(required)'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'mobile_num') !!}                                       
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'job_experience_id') !!}">
        {!! Form::label('job_experience_id', __('Experience'), ['class' => 'bold']) !!}
        {!! Form::select('job_experience_id', ['' => __('Select Experience').' '.__('(required)')]+$jobExperiences, null, ['class' => 'form-control select-multiple-experience','style' => 'width:100%']) !!}
        {!! APFrmErrHelp::showErrors($errors, 'job_experience_id') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'career_level_id') !!}">
        {!! Form::label('career_level_id', __('Career Level'), ['class' => 'bold']) !!}
        {!! Form::select('career_level_id', ['' => __('Select Career Level').' '.__('(required)')]+$careerLevels, null, ['class' => 'form-control select-multiple-career-level','style' => 'width:100%']) !!}
        {!! APFrmErrHelp::showErrors($errors, 'career_level_id') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'industry_id') !!}">
        {!! Form::label('industry_id', __('Industry'), ['class' => 'bold']) !!}
        {!! Form::select('industry_id', ['' => __('Select Industry').' '.__('(required)')]+$industries, null, ['class' => 'form-control select-multiple-industry','style' => 'width:100%']) !!}
        {!! APFrmErrHelp::showErrors($errors, 'industry_id') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'functional_area_id') !!}">
        {!! Form::label('functional_area_id', __('Functional Area'), ['class' => 'bold']) !!}
        {!! Form::select('functional_area_id', ['' => __('Select Functional Area').' '.__('(required)')]+$functionalAreas, null, ['class' => 'form-control select-multiple-functional-area','style' => 'width:100%']) !!}
        {!! APFrmErrHelp::showErrors($errors, 'functional_area_id') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'whatsapp') !!}">
        {!! Form::label('whatsapp', __('Whatsapp Number'), ['class' => 'bold']) !!}
        {!! Form::tel('whatsapp', null, array('class'=>'form-control', 'id'=>'whatsapp', 'placeholder'=>__('Whatsapp Number With Country Code'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'current_salary') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'telegram') !!}">
        {!! Form::label('telegram', __('Telegram ID'), ['class' => 'bold']) !!}
        {!! Form::text('telegram', null, array('class'=>'form-control', 'id'=>'telegram', 'placeholder'=>__('Telegram ID'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'telegram') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'skype') !!}">
        {!! Form::label('skype', __('Skype ID'), ['class' => 'bold']) !!}
        {!! Form::text('skype', null, array('class'=>'form-control', 'id'=>'skype', 'placeholder'=>__('Skype ID'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'skype') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'current_salary') !!}">
        {!! Form::label('current_salary', __('Current Salary'), ['class' => 'bold']) !!}
        {!! Form::text('current_salary', null, array('class'=>'form-control', 'id'=>'current_salary', 'placeholder'=>__('Current Salary').' '.__('(required)'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'current_salary') !!}                                       
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'expected_salary') !!}">
        {!! Form::label('expected_salary', __('Expected Salary'), ['class' => 'bold']) !!}
        {!! Form::text('expected_salary', null, array('class'=>'form-control', 'id'=>'expected_salary', 'placeholder'=>__('Expected Salary').' '.__('(required)'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'expected_salary') !!}                                       
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'salary_currency') !!}">
        {!! Form::label('salary_currency', __('Salary Currency'), ['class' => 'bold']) !!}
        @php
            $salary_currency = Request::get('salary_currency', (isset($user) && !empty($user->salary_currency))? $user->salary_currency:$siteSetting->default_currency_code);
        @endphp
        {!! Form::select('salary_currency', ['' => __('Select Salary Currency').' '.__('(required)')]+$currencies, $salary_currency, ['class' => 'form-control select-multiple-salary-currency','style' => 'width:100%']) !!}
        {!! APFrmErrHelp::showErrors($errors, 'salary_currency') !!}
    <?php /*        {!! Form::text('salary_currency', null, array('class'=>'form-control', 'id'=>'salary_currency', 'placeholder'=>'Salary Currency (required)', 'autocomplete'=>'off')) !!} */ ?>
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'street_address') !!}">
        {!! Form::label('street_address', __('Street Address'), ['class' => 'bold']) !!}
        {!! Form::textarea('street_address', null, array('class'=>'form-control', 'id'=>'street_address', 'placeholder'=>__('Street Address').' '.__('(required)'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'street_address') !!}                                       
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'profile_summary') !!}">
        {!! Form::label('profile_summary', __('Summary'), ['class' => 'bold']) !!}
        {!! Form::textarea('profile_summary', old('summary', (isset($user))? $user->getProfileSummary('summary'):''), array('class'=>'form-control', 'id'=>'profile_summary', 'placeholder'=>__('Summary'))) !!}
        {!! APFrmErrHelp::showErrors($errors, 'profile_summary') !!}
    </div>
    @if((bool)config('jobseeker.is_jobseeker_package_active'))
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'job_seeker_package_id') !!}">
        {!! Form::label('job_seeker_package_id', __('Package'), ['class' => 'bold']) !!}
        {!! Form::select('job_seeker_package_id', ['' => __('Select Package')]+$packages, null, array('class'=>'form-control select-multiple-package','style' => 'width:100%', 'id'=>'job_seeker_package_id')) !!}
        {!! APFrmErrHelp::showErrors($errors, 'job_seeker_package_id') !!} </div>
    @if(isset($user) && $user->package_id > 0)
    <div class="form-group">
        {!! Form::label('package', __('Package'), ['class' => 'bold']) !!}
        <strong>{{$user->getPackage('package_title')}}</strong>
    </div>
    <div class="form-group">
        {!! Form::label('package_Duration', __('Package Duration'), ['class' => 'bold']) !!}
        <strong>{{$user->package_start_date->format('d M, Y')}}</strong> - <strong>{{$user->package_end_date->format('d M, Y')}}</strong>
    </div>
    <div class="form-group">
        {!! Form::label('package_quota', __('Availed quota'), ['class' => 'bold']) !!}
        <strong>{{$user->availed_jobs_quota}}</strong> / <strong>{{$user->jobs_quota}}</strong>
    </div>
    <hr/>
    @endif
    @endif
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'is_immediate_available') !!}">
        {!! Form::label('is_immediate_available', __('Is Immediate Available?'), ['class' => 'bold']) !!}
        <div class="radio-list">
            <?php
            $is_immediate_available_1 = 'checked="checked"';
            $is_immediate_available_2 = '';
            if (old('is_immediate_available', ((isset($user)) ? $user->is_immediate_available : 1)) == 0) {
                $is_immediate_available_1 = '';
                $is_immediate_available_2 = 'checked="checked"';
            }
            ?>
            <label class="radio-inline">
                <input id="immediate_available" name="is_immediate_available" type="radio" value="1" {{$is_immediate_available_1}}>
                {{__('Yes')}} </label>
            <label class="radio-inline">
                <input id="not_immediate_available" name="is_immediate_available" type="radio" value="0" {{$is_immediate_available_2}}>
                {{__('No')}} </label>
        </div>
        {!! APFrmErrHelp::showErrors($errors, 'is_immediate_available') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'is_active') !!}">
        {!! Form::label('is_active', __('Is Active?'), ['class' => 'bold']) !!}
        <div class="radio-list">
            <?php
            $is_active_1 = 'checked="checked"';
            $is_active_2 = '';
            if (old('is_active', ((isset($user)) ? $user->is_active : 1)) == 0) {
                $is_active_1 = '';
                $is_active_2 = 'checked="checked"';
            }
            ?>
            <label class="radio-inline">
                <input id="active" name="is_active" type="radio" value="1" {{$is_active_1}}>
                {{__('Yes')}} </label>
            <label class="radio-inline">
                <input id="not_active" name="is_active" type="radio" value="0" {{$is_active_2}}>
                {{__('No')}} </label>
        </div>
        {!! APFrmErrHelp::showErrors($errors, 'is_active') !!}
    </div>
    <div class="form-group {!! APFrmErrHelp::hasError($errors, 'verified') !!}">
        {!! Form::label('verified', __('Is Verified?'), ['class' => 'bold']) !!}
        <div class="radio-list">
            <?php
            $verified_1 = 'checked="checked"';
            $verified_2 = '';
            if (old('verified', ((isset($user)) ? $user->verified : 1)) == 0) {
                $verified_1 = '';
                $verified_2 = 'checked="checked"';
            }
            ?>
            <label class="radio-inline">
                <input id="verified" name="verified" type="radio" value="1" {{$verified_1}}>
                {{__('Yes')}} </label>
            <label class="radio-inline">
                <input id="not_verified" name="verified" type="radio" value="0" {{$verified_2}}>
                {{__('No')}} </label>
        </div>
        {!! APFrmErrHelp::showErrors($errors, 'verified') !!}
    </div>
    {!! Form::button($action.' <i class="fa fa-arrow-circle-right" aria-hidden="true"></i>', array('class'=>'btn btn-large btn-primary', 'type'=>'submit')) !!}
</div>
{!! Form::close() !!}
@push('css')
<style type="text/css">
    .datepicker>div {
        display: block;
    }
</style>
@endpush
@push('scripts')
<script type="text/javascript">
    $(document).ready(function () {
        initdatepicker();
        $('#current_salary, #expected_salary').priceFormat({prefix:'',thousandsSeparator:',',limit:21,centsLimit:0,clearOnEmpty:true});
<?php  /*        $('#salary_currency').typeahead({
            source: function (query, process) {
                return $.get("{{ route('typeahead.currency_codes') }}", {query: query}, function (data) {
                    console.log(data);
                    data = $.parseJSON(data);
                    return process(data);
                });
            }
        });*/ ?>

        $('#country_id').on('change', function (e) {
            e.preventDefault();
            filterDefaultStates(0);
        });
        $(document).on('change', '#state_id', function (e) {
            e.preventDefault();
            filterDefaultCities(0);
        });
        filterDefaultStates(<?php echo old('state_id', (isset($user)) ? $user->state_id : 0); ?>);
        $('.select-multiple-gender').select2({
            placeholder: "{{__('Select Gender').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-marital-status').select2({
            placeholder: "{{__('Select Marital Status').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-country').select2({
            placeholder: "{{__('Select Country').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-state').select2({
            placeholder: "{{__('Select State').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-city').select2({
            placeholder: "{{__('Select City').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-nationality').select2({
            placeholder: "{{__('Select Nationality').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-experience').select2({
            placeholder: "{{__('Select Experience').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-career-level').select2({
            placeholder: "{{__('Select Career Level').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-industry').select2({
            placeholder: "{{__('Select Industry').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-functional-area').select2({
            placeholder: "{{__('Select Functional Area').' '.__('(required)')}}",
//            allowClear: true
        });
        $('.select-multiple-salary-currency').select2({
            placeholder: "{{__('Select Salary Currency').' '.__('(required)')}}",
//            allowClear: true
        });
    });

    function filterDefaultStates(state_id)
    {
        var country_id = $('#country_id').val();
        if (country_id != '') {
            $.post("{{ route('filter.default.states.dropdown') }}", {country_id: country_id, state_id: state_id, _method: 'POST', _token: '{{ csrf_token() }}'})
                    .done(function (response) {
                        $('#default_state_dd').html(response);
                        filterDefaultCities(<?php echo old('city_id', (isset($user)) ? $user->city_id : 0); ?>);
                        $('.select-multiple-state').select2({
                            placeholder: "{{__('Select State').' '.__('(required)')}}",
//            allowClear: true
                        });
                    });
        }
    }
    function filterDefaultCities(city_id)
    {
        var state_id = $('#state_id').val();
        if (state_id != '') {
            $.post("{{ route('filter.default.cities.dropdown') }}", {state_id: state_id, city_id: city_id, _method: 'POST', _token: '{{ csrf_token() }}'})
                    .done(function (response) {
                        $('#default_city_dd').html(response);
                        $('.select-multiple-city').select2({
                            placeholder: "{{__('Select City').' '.__('(required)')}}",
//            allowClear: true
                        });
                    });
        }
    }
    function initdatepicker() {
        $(".datepicker").datepicker({
            autoclose: true,
            format: 'M d, yyyy',
        });
    }
</script>
@endpush
