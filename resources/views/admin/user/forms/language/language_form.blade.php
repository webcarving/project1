<div class="modal-body">
    <div class="form-body">
        <div class="form-group" id="div_language_id">
            <label for="language_id" class="bold">Language</label>
            <?php
            $language_id = (isset($profileLanguage) ? $profileLanguage->language_id : null);
            ?>
            {!! Form::select('language_id', ['' => __('Select Language').' '.__('(required)')]+$languages, $language_id, array('class'=>'form-control select-multiple-language','style' => 'width:100%', 'id'=>'language_id')) !!} <span class="help-block language_id-error"></span> </div>
        <div class="form-group" id="div_language_level_id">
            <label for="language_level_id" class="bold">Language Level</label>
            <?php
            $language_level_id = (isset($profileLanguage) ? $profileLanguage->language_level_id : null);
            ?>
            {!! Form::select('language_level_id', ['' => __('Select Language Level').' '.__('(required)')]+$languageLevels, $language_level_id, array('class'=>'form-control select-multiple-language-level','style' => 'width:100%', 'id'=>'language_level_id')) !!} <span class="help-block language_level_id-error"></span> </div>
    </div>
</div>
@push('scripts') 
<script>
    $(document).ready(function () {

		$('.select-multiple-language').select2({
			placeholder: "{{__('Select Language').' '.__('(required)')}}",
			//            allowClear: true
		});
		

		$('.select-multiple-language-level').select2({
			placeholder: "{{__('Select Language Level').' '.__('(required)')}}",
			//            allowClear: true
		});
		
    });
</script> 
@endpush
