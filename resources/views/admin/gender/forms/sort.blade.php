{!! APFrmErrHelp::showErrorsNotice($errors) !!}
@include('flash::message')
<div class="form-body">
    <h3>Drag and Drop to Sort Genders</h3>
    {!! Form::select('lang', ['' => __('Select Language')]+$languages, config('default_lang'), array('class'=>'form-control select-multiple-language','style' => 'width:100%', 'id'=>'lang', 'onchange'=>'refreshGenderSortData();')) !!}
    <div id="genderSortDataDiv"></div>
</div>
@push('scripts') 
<script>
    $(document).ready(function () {
        refreshGenderSortData();
        $('.select-multiple-language').select2({
            placeholder: "{{__('Select Language')}}",
//            allowClear: true
        });
    });
    function refreshGenderSortData() {
        var language = $('#lang').val();
        $.ajax({
            type: "GET",
            url: "{{ route('gender.sort.data') }}",
            data: {lang: language},
            success: function (responseData) {
                $("#genderSortDataDiv").html('');
                $("#genderSortDataDiv").html(responseData);
                /**************************/
                $('#sortable').sortable({
                    update: function (event, ui) {
                        var genderOrder = $(this).sortable('toArray').toString();
                        $.post("{{ route('gender.sort.update') }}", {genderOrder: genderOrder, _method: 'PUT', _token: '{{ csrf_token() }}'})
                    }
                });
                $("#sortable").disableSelection();
                /***************************/
            }
        });
    }
</script> 
@endpush
