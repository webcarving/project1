<?php


Route::post('filter-default-cities-dropdown', 'AjaxController@filterDefaultCities')->name('filter.default.cities.dropdown');
Route::post('filter-default-states-dropdown', 'AjaxController@filterDefaultStates')->name('filter.default.states.dropdown');
Route::post('filter-lang-cities-dropdown', 'AjaxController@filterLangCities')->name('filter.lang.cities.dropdown');
Route::post('filter-lang-states-dropdown', 'AjaxController@filterLangStates')->name('filter.lang.states.dropdown');
Route::post('filter-cities-dropdown', 'AjaxController@filterCities')->name('filter.cities.dropdown');
Route::post('filter-states-dropdown', 'AjaxController@filterStates')->name('filter.states.dropdown');
Route::post('filter-degree-types-dropdown', 'AjaxController@filterDegreeTypes')->name('filter.degree.types.dropdown');
Route::post('filter-lang-addressmulti-dropdown', 'AjaxController@filterMultiAddress')->name('filter-lang-addressmulti-dropdown');

Route::get('filter-default-cities-dropdown', function () {
    return Redirect::back();
});
Route::get('filter-default-states-dropdown', function () {
    return Redirect::back();
});
Route::get('filter-lang-cities-dropdown', function () {
    return Redirect::back();
});
Route::get('filter-lang-states-dropdown', function () {
    return Redirect::back();
});
Route::get('filter-cities-dropdown', function () {
    return Redirect::back();
});
Route::get('filter-states-dropdown', function () {
    return Redirect::back();
});
Route::get('filter-degree-types-dropdown', function () {
   return Redirect::back();
});
Route::get('filter-lang-addressmulti-dropdown', function () {
    return Redirect::back();
});
