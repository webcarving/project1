<?php

return [
    'email_rule' => array(
        'required',
        'max:100',
        'email'
    ),
    'email_rule_optional' => array(
        'max:100',
        'email'
    ),
    'name_rule' => array(
        'required',
        'max:150'
    ),
    'first_name_rule' => array(
        'required',
        'max:50'
    ),
    'middle_name_rule_optional' => array(
        'max:50'
    ),
    'last_name_rule' => array(
        'required',
        'max:50'
    ),

    'password_rule' => array(
        'required',
        'min:8',
        'max:100'
    ),
    'token_rule' => array(
        'required',
        'max:100'
    ),
    'id_rule' => array(
        'required',
        'integer',
        'min:0',
        'max:4294967295'
    ),
    'id_rule_optional' => array(
        'integer',
        'min:0',
        'max:4294967295'
    ),
    'idbig_rule' => array(
        'required',
        'integer',
        'min:0',
        'max:18446744073709551615'
    ),
    'idbig_rule_optional' => array(
        'integer',
        'min:0',
        'max:18446744073709551615'
    ),
    'bool_rule' => array(
        'required',
        'boolean'
    ),
    'lang_rule' => array(
        'required',
        'max:2'
    ),
    'slug_rule' => array(
        'required',
        'alpha_dash',
        'max:100'
    ),
    'phone_rule' => array(
        'required',
        'max:20'
    ),
    'phone_rule_optional' => array(
        'max:20'
    ),
    'currency' => array(
        'required',
        'max:30'
    ),
    'url_rule' => array(
        'required',
        'url',
        'max:150'
    ),
    'url_rule_optional' => array(
        'url',
        'max:150'
    ),
    'captcha_rule' => array(
        'required',
        'captcha'
    ),
    'text_rule' => array(
        'required',
    ),
    'salary_rule' => array(
        'required',
        'regex:/^(?:0|([1-9]\d{0,2}(,\d{3}){0,12}))$/m',
    ),
    'price_rule' => array(
        'required',
        'regex:/^(?:0|([1-9]\d{0,2}(,\d{3}){0,12}))(.[0-9][0-9])+$/m',
    ),
    'date_rule' => array(
        'required',
        'date_format:"M j, Y"',
    ),
    'date_end_rule' => array(
        'required',
        'date_format:"M j, Y"',
        'after_or_equal:date_start',
    ),
    'package_enum_rule' => array(
        'required',
        'in:job_seeker,employer',
    ),
    'mail_driver_rule' => array(
        'required',
        'in:array,log,sparkpost,ses,mandrill,mailgun,sendmail,smtp,mail',
    ),
    'paypal_live_sandbox_rule' => array(
        'required',
        'in:live,sandbox',
    ),
    'cv_file_rule' => array(
        'required',
        'mimes:doc,docx,pdf',
        'max: 1000',
    ),
    'cv_file_rule_optional' => array (
        'mimes:doc,docx,pdf',
        'max: 1000',
    ),
    'image_rule' => array(
        'required',
        'mimes:jpeg,png',
        'max:50',
        'dimensions:width=150,height=150'
    ),
    'image_rule_optional' => array(
        'mimes:jpeg,png',
        'max:50',
        'dimensions:width=150,height=150'
    ),
    'logo_rule' => array(
        'required',
        'mimes:jpeg,png',
        'max:50',
        'dimensions:min_width=0,max_width=300,min_height=0,max_height=100'
    ),
    'logo_rule_optional' => array(
        'mimes:jpeg,png',
        'max:50',
        'dimensions:min_width=0,max_width=300,min_height=0,max_height=100'
    ),

    'array_rule' => array(
        'required',
        'array'
    ),
    'array_rule_optional' => array(
        'array'
    ),
    'array_integer_star_rule' => array(
        'integer',
        'min:0',
        'max:4294967295',
    ),

    //alnum
    'alnum2_rule' => array(
        'required',
        'max:2'
    ),
    'alnum3_rule' => array(
        'required',
        'max:3'
    ),
    'alnum3_rule_optional' => array(
        'max:3'
    ),
    'alnum5_rule' => array(
        'required',
        'max:5'
    ),
    'alnum5_rule_optional' => array(
        'max:5'
    ),
    'alnum10_rule' => array(
        'required',
        'max:10'
    ),
    'alnum10_rule_optional' => array(
        'max:10'
    ),
    'alnum15_rule' => array(
        'required',
        'max:15'
    ),
    'alnum20_rule' => array(
        'required',
        'max:20'
    ),
    'alnum30_rule' => array(
        'required',
        'max:30'
    ),
    'alnum40_rule' => array(
        'required',
        'max:40'
    ),
    'alnum50_rule' => array(
        'required',
        'max:50'
    ),
    'alnum50_rule_optional' => array(
        'max:50'
    ),
    'alnum100_rule' => array(
        'required',
        'max:100'
    ),
    'alnum100_rule_optional' => array(
        'max:100'
    ),
    'alnum150_rule' => array(
        'required',
        'max:150'
    ),
    'alnum150_rule_optional' => array(
        'max:150'
    ),
    'alnum200_rule' => array(
        'required',
        'max:200'
    ),
    'alnum200_rule_optional' => array(
        'max:200'
    ),
    'alnum250_rule' => array(
        'required',
        'max:250'
    ),
    'alnum300_rule' => array(
        'required',
        'max:300'
    ),
    'alnum300_rule_optional' => array(
        'max:300'
    ),
    'alnum350_rule' => array(
        'required',
        'max:350'
    ),
    'alnum400_rule' => array(
        'required',
        'max:400'
    ),
    'alnum450_rule' => array(
        'required',
        'max:450'
    ),
    'alnum500_rule' => array(
        'required',
        'max:500'
    ),
    'alnum500_rule_optional' => array(
        'max:500'
    ),
    'alpha_rule' => array(
        'required',
        'alpha'
    ),
    'numbertill_rule' => array(
        'required',
        'regex:/^[\d]+-[\d]+$/m',
    )

];
