<?php

namespace App;

use App;
use App\Traits\Lang;
use App\Traits\IsDefault;
use App\Traits\Active;
use App\Traits\Sorted;
use App\Traits\CountryStateCity;
use App\Helpers\MiscHelper;
use App\Helpers\DataArrayHelper;
use Illuminate\Database\Eloquent\Model;

class State extends Model
{

    use Lang;
    use IsDefault;
    use Active;
    use Sorted;
    use CountryStateCity;

    protected $table = 'states';
    public $timestamps = true;
    protected $guarded = ['id'];
    //protected $dateFormat = 'U';
    protected $dates = ['created_at', 'updated_at'];

    public function cities()
    {
        return $this->hasMany('App\City', 'state_id', 'id');
    }
    public static function StateID($id)
    {
        $state = self::where('states.code', '=', $id)->lang()->active()->first();
        if (null === $state) {
            $state = self::where('states.code', '=', $id)->active()->first();
        } 
        return (isset($state->state_id))?$state->state_id:0;
    }
    public static function getjobstatename($id)
    {
        $city = self::where('states.id', '=', $id)->lang()->active()->first();

        if (null === $city) {
            $city = self::where('states.id', '=', $id)->active()->first();
        }
        return (isset($city->state))?$city->state:'';
    }
    public static function getjobstatecode($id)
    {
        $city = self::where('states.id', '=', $id)->lang()->active()->first();

        if (null === $city) {
            $city = self::where('states.id', '=', $id)->active()->first();
        }
        return (isset($city->code))?$city->code:'';
    }

}
