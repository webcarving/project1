<?php

namespace App;

use App;
use App\Traits\Lang;
use App\Traits\IsDefault;
use App\Traits\Active;
use App\Traits\Sorted;
use Illuminate\Database\Eloquent\Model;

class JobBenefit extends Model
{

    use Lang;
    use IsDefault;
    use Active;
    use Sorted;

    protected $table = 'job_benefits';
    public $timestamps = true;
    protected $guarded = ['id'];
    //protected $dateFormat = 'U';
    protected $dates = ['created_at', 'updated_at'];
    public static function getjobbenefitname($id)
    {
        $city = self::where('job_benefits.id', '=', $id)->lang()->active()->first();
 		if (null === $city) {
            $city = self::where('job_benefits.id', '=', $id)->active()->first();
        }
        return (isset($city->job_benefit))?$city->job_benefit:'';
    }

    public static function getLangLastid()
    {
        $active_lang = app()->getLocale();
        $order = self::whereRaw('id = (select max(`id`) from job_benefits where lang="'.$active_lang.'")')->first();
        return $order;
    }
}
