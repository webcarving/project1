<?php

namespace App;

use App;
use Illuminate\Database\Eloquent\Model;

class JobLanguageManager extends Model
{
    protected $table = 'manage_job_language';
    public $timestamps = true;
    protected $guarded = ['id'];
    //protected $dateFormat = 'U';
    protected $dates = ['created_at', 'updated_at'];

    public function job()
    {
        return $this->belongsTo('App\Job', 'job_id', 'id');
    }

    public function getJob($field = '')
    {
        if (null !== $job = $this->job()->first()) {
            if (!empty($field)) {
                return $job->$field;
            } else {
                return $job;
            }
        }
    }

    public function jobCity()
    {
        return $this->belongsTo('App\City', 'city_id', 'city_id');
    }
    public function Country()
    {
        return $this->belongsTo('App\Country', 'country_id', 'country_id');
    }

    public function getJobLanguage($field = '')
    {
        $jobSkill = $this->jobCity()->lang()->first();
        if (null === $jobSkill) {
            $jobSkill = $this->jobSkill()->first();
        }
        if (null !== $jobSkill) {
            if (!empty($field)) {
                return $jobSkill->$field;
            } else {
                return $jobSkill;
            }
        }
    }

}
