<?php

namespace App;

use App;
use App\Traits\Lang;
use App\Traits\IsDefault;
use App\Traits\Active;
use App\Traits\Sorted;
use Illuminate\Database\Eloquent\Model;

class JobType extends Model
{

    use Lang;
    use IsDefault;
    use Active;
    use Sorted;

    protected $table = 'job_types';
    public $timestamps = true;
    protected $guarded = ['id'];
    //protected $dateFormat = 'U';
    protected $dates = ['created_at', 'updated_at'];
     public static function gettypeName($id)
    {
        $city = self::where('job_types.id', '=', $id)->lang()->active()->first();

        if (null === $city) {
            $city = self::where('job_types.id', '=', $id)->active()->first();
        }
        return (isset($city->job_type))?$city->job_type:'';
    }
}
