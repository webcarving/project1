<?php

namespace App\Traits;

use DB;
use App\Country;
use App\Languages;
use App\State;
use App\City;
use App\JobSkillManager;
use App\Http\Requests;
use Illuminate\Http\Request;

trait CountryStateCity
{ 
    public function deleteCountry(Request $request)
    {
        $id = $request->input('id');
        try {
            $this->delCountry($id);
            return 'ok';
        } catch (ModelNotFoundException $e) {
            return 'notok';
        }
    }

    private function delCountry($id)
    {
        $country = Country::findOrFail($id);
        $states = State::select('states.id')->where('states.country_id', '=', $country->country_id)->pluck('states.id')->toArray();
        foreach ($states as $state_id) {
            $this->delState($state_id);
        }
        if ((bool) $country->is_default) {
            Country::where('country_id', '=', $country->country_id)->delete();
        } else {
            $country->delete();
        }
    }

    public function deleteState(Request $request)
    {
        $id = $request->input('id');
        try {
            $this->delState($id);
            return 'ok';
        } catch (ModelNotFoundException $e) {
            return 'notok';
        }
    }

    private function delState($id)
    {
        $state = State::findOrFail($id);
        $cities = City::select('cities.id')->where('cities.state_id', '=', $state->state_id)->pluck('cities.id')->toArray();
        foreach ($cities as $city_id) {
            $this->delCity($city_id);
        }
        if ((bool) $state->is_default) {
            State::where('state_id', '=', $state->state_id)->delete();
        } else {
            $state->delete();
        }
    }

    public function deleteCity(Request $request)
    {
        $id = $request->input('id');
        try {
            $this->delCity($id);
            return 'ok';
        } catch (ModelNotFoundException $e) {
            return 'notok';
        }
    }

    private function delCity($id)
    {
        $city = City::findOrFail($id);
        if ((bool) $city->is_default) {
            City::where('city_id', '=', $city->city_id)->delete();
        } else {
            $city->delete();
        }
    }

    public function country()
    {
        return $this->belongsTo('App\Country', 'country_id', 'country_id');
    }
     
    public function getCountry($field = '')
    {
        $country = $this->country()->lang()->first();
        if (null === $country) {
            $country = $this->country()->first();
        }
        if (null !== $country) {
            if (!empty($field)) {
                return $country->$field;
            } else {
                return $country;
            }
        }
    }

    public function state()
    {
        return $this->belongsTo('App\State', 'state_id', 'state_id');
    }

    public function getState($field = '')
    {
        $state = $this->state()->lang()->first();
        if (null === $state) {
            $state = $this->state()->first();
        }
        if (null !== $state) {
            if (!empty($field)) {
                return $state->$field;
            } else {
                return $state;
            }
        }
    }

    public function city()
    {
        return $this->belongsTo('App\City', 'city_id', 'city_id');
    }
    public function getCity($field = '')
    {
        $city = $this->city()->lang()->first();
        if (null === $city) {
            $city = $this->city()->first();
        }
        if (null !== $city) {
            if (!empty($field)) {
                return $city->$field;
            } else {
                return $city;
            }
        }
    }
    public function getCitynew($id = '')
    {
         
        $cityes = City::where('id', $id)  
             ->get(['city','state_id'])->toArray();  
             $state= State::where('id', $cityes[0]['state_id'])  
             ->get(['state'])->toArray();

            return ($cityes[0]['city'].", ".$state[0]['state']);
    }
    public function getCitynewList($id = '')
    {

        $cityes = City::where('id', $id)
            ->get(['city','state_id'])->toArray();
        $state= State::where('id', $cityes[0]['state_id'])
            ->get(['state'])->toArray();

        return ($cityes[0]['city']." (".$state[0]['state'].")");
    }
    public function getCitynamednew($id = '')
    {
         
        $cityes = City::where('id', $id)  
             ->get(['city'])->toArray();  
            return ($cityes[0]['city']); 
    } 
    public function getStatenamenew($id='')
    {
        $state = State::where('id', $id)  
             ->get(['state'])->toArray();  
            return ($state[0]['state']);
    }
    public function getCountrynew($id = '')
     {
        $country = Country::where('id', $id)  
             ->get(['country'])->toArray(); 

            return $country[0]['country'];
    }

    public function getLocation()
    {
        $country = $this->getCountry('country');
        $state = $this->getState('state');
        $city = $this->getCity('city');

        $str = '';
        if (!empty($city))
            $str .= $city;
        if (!empty($state))
            $str .= ', ' . $state;
        if (!empty($country))
            $str .= ', ' . $country;

        return $str;
    }
    public function getLocationAll($jobLocation)
    { 
        $str='';    
        foreach ($jobLocation as $jobSkillManager) {
              $local=($jobSkillManager->is_city==1)?$this->getCitynew($jobSkillManager->city_id).", ":'';
            $country=$this->getCountrynew($jobSkillManager->country_id); 
             $location=$local.$country; 
              //$str .= '<li><a>'.$location . '</a></li>'; 
              $str .= $location . '<br>';
            }
        return $str;
    }
    public function getLocationAllList($jobLocation)
    {
        $str='';
        $counter_location = 0;
        foreach ($jobLocation as $jobSkillManager) {
            if($counter_location++ < 3){
                $local=$this->getCitynewList($jobSkillManager->city_id);
                $location=$local;
                //$str .= '<li><a>'.$location . '</a></li>';
                $str .= $location . (isset($jobLocation[$counter_location])?', ':'');
            }
        }
        $str .= ((count($jobLocation)>3)?'...':'');
        return $str;
    }

}
