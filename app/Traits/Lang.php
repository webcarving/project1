<?php

namespace App\Traits;

use DB;
use App\Language;

trait Lang
{

    public function scopeLang($query)
    {
        return $query->where('lang', 'like', \App::getLocale());
    }

    public function language()
    {
        return $this->hasOne('App\Language', 'iso_code', 'lang');
    }

    public function isLangRTL()
    {
        return $this->language()->first()->is_rtl;
    }
    public function getLanguageName(){
        $Lang = App\Language::where('id', $id)  
              ->get(['lang'])->toArray(); 
              return ($Lang[0]['lang']);  
    }
    public function getLanguage($id = '')
    {
        $country = Language::where('id', $id)  
             ->get(['lang'])->toArray();   
            return $country[0]['lang'];
    }
    public function getLalnguageAll($jobLanguage)
    {  
        $str='';
        foreach ($jobLanguage as $jobLangManager) { 
            $lang=$this->getLanguage($jobLangManager->job_lang_id); 
            $str .=  $lang . '</br>';  
        }
        return $str;
    }
}
