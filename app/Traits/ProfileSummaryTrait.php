<?php

namespace App\Traits;

use App\ProfileSummary;
use App\Http\Requests\ProfileSummaryFormRequest;
use App\User;
use Illuminate\Support\Facades\Gate;

trait ProfileSummaryTrait
{

    public function updateProfileSummary($user_id, ProfileSummaryFormRequest $request)
    {
        $user = $post = User::findOrFail($user_id);

        if (!Gate::allows('check-user', $post)) {
            return response()->json(array('success' => false, 'html' => view('errors.404')->render()),404);
        }

        ProfileSummary::where('user_id', '=', $user_id)->delete();

        $summary = $request->input('summary');
        $ProfileSummary = new ProfileSummary();
        $ProfileSummary->user_id = $user_id;
        $ProfileSummary->summary = $summary;
        $ProfileSummary->save();
        /*         * ************************************ */
        return response()->json(array('success' => true, 'status' => 200), 200);
    }

}
