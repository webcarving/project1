<?php

namespace App\Traits;

use App\JobBenefitManager;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\JobBenefit;
use Illuminate\Support\Facades\Validator;

trait Benefits
{

    private function storeJobBenefits($request, $job_id)
    {
        if ($request->has('benefits')) {
            JobBenefitManager::where('job_id', '=', $job_id)->delete();
            $benefits = $request->input('benefits');
            foreach ($benefits as $job_benefit_id) {
                $jobBenefitManager = new JobBenefitManager();
                $jobBenefitManager->job_id = $job_id;
                $jobBenefitManager->job_benefit_id = $job_benefit_id;
                
                $jobBenefitManager->save();
            }
        }
    }

    public function addBenefitFrontJob(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'benefit.*' => 'required|max:200',
        ]);

        if ($validator->fails()) {
            $code = 412;
            $errors = $validator->errors();

            $message = $errors->all();
            $data = array();
            return response()->json([
                'data' => $data,
                'message' => $message
            ], $code);
            exit();
        }

        $job_benefits = $request->input('benefit');
        $job_benefit_lang_lastid = JobBenefit::getLangLastid();

        DB::beginTransaction();

        $code = 0;
        $data = array();
        $message = array();
        try{
            foreach($job_benefits as $job_benefit) {
                if($job_benefit != ''){

                    $JobBenefit = new JobBenefit();
                    $JobBenefit->job_benefit = $job_benefit;
                    $JobBenefit->is_active = '0';
                    $JobBenefit->lang = app()->getLocale();
                    $JobBenefit->save();

                    $JobBenefit->sort_order = $JobBenefit->id;
                    $JobBenefit->job_benefit_id = $JobBenefit->id;
                    $JobBenefit->update();

                    $code = 200;
                    $message = array('Success to add benefits. Please Wait for our confirmation');
                    $data = array();
                }
            }
            DB::commit();
        } catch (\Exception $e){
            DB::rollback();
            $code = 500;
            $message = array('Failed to add benefits');
            $data = array();
        }
        return response()->json([
            'data' => $data,
            'message' => $message
        ], $code);

    }

}
