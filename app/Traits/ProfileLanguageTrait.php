<?php

namespace App\Traits;

use Auth;
use DB;
use Illuminate\Support\Facades\Gate;
use Input;
use Carbon\Carbon;
use Redirect;
use App\User;
use App\ProfileLanguage;
use App\Language;
use App\LanguageLevel;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use App\Http\Requests\ProfileLanguageFormRequest;
use App\Helpers\DataArrayHelper;

trait ProfileLanguageTrait
{

    public function showProfileLanguages(Request $request, $user_id)
    {
        $user = $post = User::find($user_id);
        if (!Gate::allows('check-user', $post) && !Auth::guard('admin')->user()) {
            echo view('errors.404')->render();
            return;
        }

        $html = '<div class="col-mid-12"><table class="table table-bordered table-condensed">';
        if (isset($user) && count($user->profileLanguages)):
            foreach ($user->profileLanguages as $language):


                $html .= '<tr id="language_' . $language->id . '">
						<td><span class="text text-success">' . $language->getLanguage('lang') . '</span></td>
						<td><span class="text text-success">' . $language->getLanguageLevel('language_level') . '</span></td>
						<td><a href="javascripr:;" onclick="showProfileLanguageEditModal(' . $language->id . ');" class="text text-warning">' . __('Edit') . '</a>&nbsp;|&nbsp;<a href="javascripr:;" onclick="delete_profile_language(' . $language->id . ');" class="text text-danger">' . __('Delete') . '</a></td>
								</tr>';
            endforeach;
        endif;

        echo $html . '</table></div>';
    }

    public function showApplicantProfileLanguages(Request $request, $user_id)
    {
        $user = $post = User::find($user_id);
        if (!Gate::allows('check-user', $post) && !Auth::guard('admin')->user()) {
            echo view('errors.404')->render();
            return;
        }
        $html = '<div class="col-mid-12"><table class="table table-bordered table-condensed">';
        if (isset($user) && count($user->profileLanguages)):
            foreach ($user->profileLanguages as $language):


                $html .= '<tr id="language_' . $language->id . '">
						<td><span class="text text-success">' . $language->getLanguage('lang') . '</span></td>
						<td><span class="text text-success">' . $language->getLanguageLevel('language_level') . '</span></td></tr>';
            endforeach;
             else:
           $html .='<li><center><h5>No Data Found</h5></center></li>';
        endif;

        echo $html . '</table></div>';
    }

    public function getProfileLanguageForm(Request $request, $user_id)
    {
        $user = $post = User::find($user_id);
        if (!Gate::allows('check-user', $post) && !Auth::guard('admin')->user()) {
            return response()->json(array('success' => false, 'html' => view('errors.404')->render()),404);
        }

        $languages = DataArrayHelper::languagesArray();
        $languageLevels = DataArrayHelper::defaultLanguageLevelsArray();

        $returnHTML = view('admin.user.forms.language.language_modal')
                ->with('user', $user)
                ->with('languages', $languages)
                ->with('languageLevels', $languageLevels)
                ->render();
        return response()->json(array('success' => true, 'html' => $returnHTML));
    }

    public function getFrontProfileLanguageForm(Request $request, $user_id)
    {
        $user = $post = User::find($user_id);
        if (!Gate::allows('check-user', $post)) {
            return response()->json(array('success' => false, 'html' => view('errors.404')->render()),404);
        }

        $languages = DataArrayHelper::languagesArray();
        $languageLevels = DataArrayHelper::langLanguageLevelsArray();

        $returnHTML = view('user.forms.language.language_modal')
                ->with('user', $user)
                ->with('languages', $languages)
                ->with('languageLevels', $languageLevels)
                ->render();
        return response()->json(array('success' => true, 'html' => $returnHTML));
    }

    public function storeProfileLanguage(ProfileLanguageFormRequest $request, $user_id)
    {
        $user = $post = User::find($user_id);
        if (!Gate::allows('check-user', $post) && !Auth::guard('admin')->user()) {
            return response()->json(array('success' => false, 'html' => view('errors.404')->render()),404);
        }

        $profileLanguage = new ProfileLanguage();
        $profileLanguage = $this->assignLanguageValues($profileLanguage, $request, $user_id);
        $profileLanguage->save();
        /*         * ************************************ */
        $returnHTML = view('admin.user.forms.language.language_thanks')->render();
        return response()->json(array('success' => true, 'status' => 200, 'html' => $returnHTML), 200);
    }

    public function storeFrontProfileLanguage(ProfileLanguageFormRequest $request, $user_id)
    {
        $user = $post = User::find($user_id);
        if (!Gate::allows('check-user', $post)) {
            return response()->json(array('success' => false, 'html' => view('errors.404')->render()),404);
        }

        $profileLanguage = new ProfileLanguage();
        $profileLanguage = $this->assignLanguageValues($profileLanguage, $request, $user_id);
        $profileLanguage->save();
        /*         * ************************************ */
        $returnHTML = view('user.forms.language.language_thanks')->render();
        return response()->json(array('success' => true, 'status' => 200, 'html' => $returnHTML), 200);
    }

    public function getProfileLanguageEditForm(Request $request, $user_id)
    {
        $user = $post = User::find($user_id);
        if (!Gate::allows('check-user', $post) && !Auth::guard('admin')->user()) {
            return response()->json(array('success' => false, 'html' => view('errors.404')->render()),404);
        }

        $profile_language_id = $request->input('profile_language_id');

        $languages = DataArrayHelper::languagesArray();
        $languageLevels = DataArrayHelper::defaultLanguageLevelsArray();

        $profileLanguage = ProfileLanguage::find($profile_language_id);

        $returnHTML = view('admin.user.forms.language.language_edit_modal')
                ->with('user', $user)
                ->with('profileLanguage', $profileLanguage)
                ->with('languages', $languages)
                ->with('languageLevels', $languageLevels)
                ->render();
        return response()->json(array('success' => true, 'html' => $returnHTML));
    }

    public function getFrontProfileLanguageEditForm(Request $request, $user_id)
    {
        $user = $post = User::find($user_id);
        if (!Gate::allows('check-user', $post)) {
            return response()->json(array('success' => false, 'html' => view('errors.404')->render()),404);
        }

        $profile_language_id = $request->input('profile_language_id');

        $languages = DataArrayHelper::languagesArray();
        $languageLevels = DataArrayHelper::langLanguageLevelsArray();

        $profileLanguage = ProfileLanguage::find($profile_language_id);

        $returnHTML = view('user.forms.language.language_edit_modal')
                ->with('user', $user)
                ->with('profileLanguage', $profileLanguage)
                ->with('languages', $languages)
                ->with('languageLevels', $languageLevels)
                ->render();
        return response()->json(array('success' => true, 'html' => $returnHTML));
    }

    public function updateProfileLanguage(ProfileLanguageFormRequest $request, $profile_language_id, $user_id)
    {

        $profileLanguage = $post = ProfileLanguage::find($profile_language_id);
        if (!Gate::allows('check-user-with-user-id', $post) && !Auth::guard('admin')->user()) {
            return response()->json(array('success' => false, 'html' => view('errors.404')->render()),404);
        }

        $profileLanguage = $this->assignLanguageValues($profileLanguage, $request, $user_id);
        $profileLanguage->update();
        /*         * ************************************ */

        $returnHTML = view('admin.user.forms.language.language_edit_thanks')->render();
        return response()->json(array('success' => true, 'status' => 200, 'html' => $returnHTML), 200);
    }

    public function updateFrontProfileLanguage(ProfileLanguageFormRequest $request, $profile_language_id, $user_id)
    {

        $profileLanguage = $post = ProfileLanguage::find($profile_language_id);
        if (!Gate::allows('check-user-with-user-id', $post)) {
            return response()->json(array('success' => false, 'html' => view('errors.404')->render()),404);
        }

        $profileLanguage = $this->assignLanguageValues($profileLanguage, $request, $user_id);
        $profileLanguage->update();
        /*         * ************************************ */

        $returnHTML = view('user.forms.language.language_edit_thanks')->render();
        return response()->json(array('success' => true, 'status' => 200, 'html' => $returnHTML), 200);
    }

    public function assignLanguageValues($profileLanguage, $request, $user_id)
    {
        $profileLanguage->user_id = $user_id;
        $profileLanguage->language_id = $request->input('language_id');
        $profileLanguage->language_level_id = $request->input('language_level_id');
        return $profileLanguage;
    }

    public function deleteProfileLanguage(Request $request)
    {
        $id = $request->input('id');
        try {
            $profileLanguage = $post = ProfileLanguage::findOrFail($id);
            if (!Gate::allows('check-user-with-user-id', $post) && !Auth::guard('admin')->user()) {
                echo 'notok';
                return;
            }
            $profileLanguage->delete();
            echo 'ok';
        } catch (ModelNotFoundException $e) {
            echo 'notok';
        }
    }

}
