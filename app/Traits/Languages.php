<?php

namespace App\Traits;

use App\JobLanguageManager;

trait Languages
{
    private function storeJobLlanguage($request, $job_id)
    {
         JobLanguageManager::where('job_id', '=', $job_id)->delete(); 
        if ($request->has('language')) { 
            $locations = $request->input('language');
            foreach ($locations as $key => $value) {
                $JobLanguageManager = new JobLanguageManager();
                $JobLanguageManager->job_id = $job_id;
                $JobLanguageManager->job_lang_id = $value;  
                $JobLanguageManager->save();
            }
        }   
    }
    private function getJobLanguage($id)
    { 
        $array = array(); 
            $cityes = JobLanguageManager::where('job_id', $id)  
                ->get(['job_lang_id']);
            foreach ($cityes as $key => $value) { 
                $array[$key] = $value->job_lang_id;
            }  
        return $array;
    } 
}
