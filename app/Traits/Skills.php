<?php

namespace App\Traits;


use App\JobSkillManager;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\DB;
use App\JobSkill;
use Illuminate\Support\Facades\Validator;

trait Skills
{

    private function storeJobSkills($request, $job_id)
    {
        if ($request->has('skills')) {
            JobSkillManager::where('job_id', '=', $job_id)->delete();
            $skills = $request->input('skills');
            foreach ($skills as $job_skill_id) {
                $jobSkillManager = new JobSkillManager();
                $jobSkillManager->job_id = $job_id;
                $jobSkillManager->job_skill_id = $job_skill_id;
                
                $jobSkillManager->save();
            }
        }
    }

    public function addSkillFrontJob(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'skill.*' => 'required|max:200',
        ]);

        if ($validator->fails()) {
            $code = 412;
            $errors = $validator->errors();

            $message = $errors->all();
            $data = array();
            return response()->json([
                'data' => $data,
                'message' => $message
            ], $code);
            exit();
        }

        $job_skills = $request->input('skill');
        $job_skill_lang_lastid = JobSkill::getLangLastid();

        DB::beginTransaction();

        $code = 0;
        $data = array();
        $message = array();
        try{
            foreach($job_skills as $job_skill) {
                if($job_skill != ''){

                    $JobSkill = new JobSkill();
                    $JobSkill->job_skill = $job_skill;
                    $JobSkill->is_active = '0';
                    $JobSkill->lang = app()->getLocale();
                    $JobSkill->save();

                    $JobSkill->job_skill_id = $job_skill_lang_lastid->id;
                    $JobSkill->sort_order = $job_skill_lang_lastid->id;
                    $JobSkill->update();

                    $code = 200;
                    $message = array('Success to add skills. Please Wait for our confirmation');
                    $data = array();
                }
            }
            DB::commit();
        } catch (\Exception $e){
            DB::rollback();
            $code = 500;
            $message = array('Failed to add skills');
            $data = array();
        }
        return response()->json([
            'data' => $data,
            'message' => $message
        ], $code);

    }

}
