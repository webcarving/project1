<?php

namespace App;

use App\Traits\CountryStateCity;
use App\CareerLevel;
use App\Industry;
use App\FunctionalArea;
use Illuminate\Database\Eloquent\Model;

class ProfileExperience extends Model
{

    use CountryStateCity;

    protected $table = 'profile_experiences';
    public $timestamps = true;
    protected $guarded = ['id'];
    //protected $dateFormat = 'U';
    protected $dates = ['created_at', 'updated_at', 'date_start', 'date_end'];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function getUser($field = '')
    {
        if (null !== $user = $this->user()->first()) {
            if (empty($field))
                return $user;
            else
                return $user->$field;
        } else {
            return '';
        }
    }
    public function getCareerLevel($field = '')
    {
        $flight = CareerLevel::find($field);
       return $flight['career_level']; 
    }
    public function getIndustry($field = '')
    {
        $flight = Industry::find($field);
       return $flight['industry']; 
    }
    public function getFunctionalArea($field = '')
    {
        $flight = FunctionalArea::find($field);
       return $flight['functional_area']; 
    }

}
