<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Carbon\Carbon;
use Illuminate\Support\Facades\Validator;

class ProfileProjectFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
            case 'POST': {
                    $id = (int) $this->input('id', 0);
                    //name
                    $name = config('rules.alnum100_rule');
                    //url
                    $url = config('rules.url_rule_optional');
                    //date_start
                    $date_start = config('rules.date_rule');
                    //date end
                    $date_end = array();
                    $is_on_going = (int) $this->input('is_on_going', 0);
                    if($is_on_going == '0'){
                        $date_end = config('rules.date_end_rule');
                    }
                    //is_on_going
                    $is_on_going = config('rules.bool_rule');
                    //description
                    $description = config('rules.alnum200_rule');

                    return [
                        "name" => $name,
                        //"image" => "required",
                        "url" => $url,
                        "date_start" => $date_start,
                        "date_end" => $date_end,
                        "is_on_going" => $is_on_going,
                        "description" => $description,

//                        "name" => "required|max:100",
                        //"image" => "required",
//                        "url" => "max:500",
//                        "date_start" => 'required|date_format:"M j, Y"',
//                        "is_on_going" => "required|boolean",
//                        "description" => "required",
                    ];
                }
            default:break;
        }
    }

    public function messages()
    {
        return [
/*            'name.required' => 'Please enter project name.',
            'image.required' => 'Only images can be uploaded.',
            'url.required' => 'Please enter project URL.',
            'date_start.required' => 'Please set start date.',
            'date_end.required_if' => 'Please set end date.',
            'is_on_going.required' => 'Is this project ongoing?',
            'description.required' => 'Please enter project description.',*/
        ];
    }

}
