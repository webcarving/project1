<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class VideoFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
            case 'POST': {
                    $id = (int) $this->input('id', 0);
                    $video_unique = '';
                    if ($id > 0) {
                        $video_unique = ',id,' . $id;
                    }


            //video_title
            $video_title = config('rules.alnum100_rule');
            //video_text
            $video_text = config('rules.alnum300_rule');
            //video_link
            $video_link = config('rules.url_rule');
            //is_active
            $is_active = config('rules.bool_rule');
            //video_id
            $video_id = array();
            $is_default = (int) $this->input('is_default', 0);
            if ($is_default == 0) {
                $video_id = config('rules.id_rule');
            }
            //is_default
            $is_default = config('rules.bool_rule');
            //lang
            $lang = config('rules.lang_rule');

            $id=array();

                    return [
                        "id" => $id,
                        "video_title" => $video_title,
                        "video_text" => $video_text,
                        "video_link" => $video_link,
                        "video_id" => $video_id,
                        "is_default" => $is_default,
                        "is_active" => $is_active,
                        "lang" => $lang,


//                        "id" => "",
//                        "lang" => "required|max:10",
//                        "video_title" => "required|unique:videos$video_unique",
//                        "video_text" => "required",
//                        "video_link" => "required",
//                        "is_default" => "required|boolean",
//                        "video_id" => "required_if:is_default,0",
//                        "is_active" => "required|boolean",
                    ];
                }
            default:break;
        }
    }

    public function messages()
    {
        return [
//            'lang.required' => 'Please select language.', 'video.required' => 'Video required.', 'is_default.required' => 'Is this Video default?', 'video_id.required_if' => 'Please select default/fallback Video.', 'is_active.required' => 'Is this Video active?',
        ];
    }

}
