<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class LanguageFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
            case 'POST': {
                    $id = (int) $this->input('id', 0);
                    $language_unique = '';
                    if ($id > 0) {
                        $language_unique = ',id,' . $id;
                    }
            //lang
            $lang = config('rules.alnum100_rule');
            //native
            $native = config('rules.alnum100_rule');
            //iso_code
            $iso_code = config('rules.alnum2_rule');
            //is_active
            $is_active = config('rules.bool_rule');
            //is_rtl
            $is_rtl = config('rules.bool_rule');
            //is_default
            $is_default = config('rules.bool_rule');

                    $id = array();
                    return [
                        "id" => $id,
                        "lang" => $lang,
                        "native" => $native,
                        "iso_code" => $iso_code,
                        "is_active" => $is_active,
                        "is_rtl" => $is_rtl,
                        "is_default" => $is_default,

//                        "id" => "",
//                        "lang" => "required|max:250",
//                        "native" => "required|max:250",
//                        "iso_code" => "required|max:10",
//                        "is_active" => "required|boolean",
//                        "is_rtl" => "required|boolean",
//                        "is_default" => "required|boolean",
                    ];
                }
            default:break;
        }
    }

    public function messages()
    {
        return [
//            'lang.required' => 'Please enter language.', 'native.required' => 'Native required.', 'iso_code.required' => 'ISO Code required.', 'is_active.required' => 'Is this Language active?', 'is_rtl.required' => 'Is this Language RTL?', 'is_default.required' => 'Is this language default?',
        ];
    }

}
