<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SiteSettingFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //site
        //site_name
        $site_name = config('rules.alnum150_rule');
        //site_slogan
        $site_slogan = config('rules.alnum100_rule');
        //site_phone_primary
        $site_phone_primary = config('rules.phone_rule');
        //site_phone_secondary
        $site_phone_secondary = config('rules.phone_rule_optional');
        //mail_from_address
        $mail_from_address = config('rules.alnum100_rule');
        //mail_from_name
        $mail_from_name = config('rules.alnum150_rule');
        //mail_to_address
        $mail_to_address = config('rules.alnum100_rule');
        //mail_to_name
        $mail_to_name = config('rules.alnum150_rule');
        //default_country_id
        $default_country_id = config('rules.id_rule');
        //country_specific_site
        $country_specific_site = config('rules.bool_rule');
        //default_currency_code
        $default_currency_code = config('rules.alnum3_rule');
        //site_street_address
        $site_street_address = config('rules.alnum300_rule');
        //site_google_map
        $site_google_map = config('rules.alnum300_rule_optional');
        //image
        $image = config('rules.logo_rule_optional');

        //email
        //mail_driver
        $mail_driver = config('rules.mail_driver_rule');
        //mail_host
        $mail_host = config('rules.alnum150_rule_optional');
        //mail_port
        $mail_port = config('rules.id_rule_optional');
        //mail_encryption
        $mail_encryption = config('rules.alnum3_rule_optional');
        //mail_username
        $mail_username = config('rules.email_rule_optional');
        //mail_password
        $mail_password = config('rules.alnum100_rule_optional');
        //mail_sendmail
        $mail_sendmail = config('rules.alnum50_rule_optional');
        //mail_pretend
        $mail_pretend = config('rules.alnum5_rule_optional');
        //mailgun_domain
        $mailgun_domain = config('rules.alnum10_rule_optional');
        //mailgun_secret
        $mailgun_secret = config('rules.alnum10_rule_optional');
        //mandrill_secret
        $mandrill_secret = config('rules.alnum10_rule_optional');
        //sparkpost_secret
        $sparkpost_secret = config('rules.alnum10_rule');
        //ses_key
        $ses_key = config('rules.alnum10_rule_optional');
        //ses_secret
        $ses_secret = config('rules.alnum10_rule_optional');
        //ses_region
        $ses_region = config('rules.alnum10_rule_optional');

        //Social Networks
        //facebook_address
        $facebook_address = config('rules.alnum150_rule_optional');
        //google_plus_address
        $google_plus_address = config('rules.alnum150_rule_optional');
        //pinterest_address
        $pinterest_address = config('rules.alnum150_rule_optional');
        //twitter_address
        $twitter_address = config('rules.alnum150_rule_optional');
        //instagram_address
        $instagram_address = config('rules.alnum150_rule_optional');
        //linkedin_address
        $linkedin_address = config('rules.alnum150_rule_optional');
        //youtube_address
        $youtube_address = config('rules.alnum150_rule_optional');
        //tumblr_address
        $tumblr_address = config('rules.alnum150_rule_optional');
        //flickr_address
        $flickr_address = config('rules.alnum150_rule_optional');

        //Manage Ads
        $index_page_below_top_employes_ad = config('rules.alnum200_rule_optional');
        $above_footer_ad = config('rules.alnum200_rule_optional');
        $dashboard_page_ad = config('rules.alnum200_rule_optional');
        $cms_page_ad = config('rules.alnum200_rule_optional');
        $listing_page_vertical_ad = config('rules.alnum200_rule_optional');
        $listing_page_horizontal_ad = config('rules.alnum200_rule_optional');

        //Captcha
        $nocaptcha_sitekey = config('rules.alnum50_rule_optional');
        $nocaptcha_secret = config('rules.alnum50_rule_optional');

        //Social Media Login
        $facebook_app_id = config('rules.alnum50_rule_optional');
        $facebook_app_secret = config('rules.alnum50_rule_optional');
        $twitter_app_id = config('rules.alnum50_rule_optional');
        $twitter_app_secret = config('rules.alnum50_rule_optional');

        //Payment Gateways
        $paypal_account = config('rules.alnum50_rule_optional');
        $paypal_client_id = config('rules.alnum100_rule_optional');
        $paypal_secret = config('rules.alnum100_rule_optional');
        $paypal_live_sandbox = config('rules.paypal_live_sandbox_rule');
        $is_paypal_active = config('rules.bool_rule');
        $stripe_key = config('rules.alnum50_rule_optional');
        $stripe_secret = config('rules.alnum50_rule_optional');
        $is_stripe_active = config('rules.bool_rule');
        $is_jobseeker_package_active = config('rules.bool_rule');
        $is_company_package_active = config('rules.bool_rule');

        //Home Page Slider
        $is_slider_active = config('rules.bool_rule');

        //Mail Chimp
        $mailchimp_api_key = config('rules.alnum50_rule_optional');
        $mailchimp_list_name = config('rules.alnum50_rule_optional');
        $mailchimp_list_id = config('rules.alnum50_rule_optional');


        return [
        //site
        'site_name' => $site_name,
        'site_slogan'=> $site_slogan,
        'site_phone_primary' => $site_phone_primary,
        'site_phone_secondary' => $site_phone_secondary,
        'mail_from_address' => $mail_from_address,
        'mail_from_name' => $mail_from_name,
        'mail_to_address' => $mail_to_address,
        'mail_to_name' => $mail_to_name,
        'default_country_id' => $default_country_id,
        'country_specific_site' => $country_specific_site,
        'default_currency_code' => $default_currency_code,
        'site_street_address' => $site_street_address,
        'site_google_map' => $site_google_map,
        'image' => $image,

        //email
        'mail_driver' => $mail_driver,
        'mail_host' => $mail_host,
        'mail_port' => $mail_port,
        'mail_encryption' => $mail_encryption,
        'mail_username' => $mail_username,
        'mail_password' => $mail_password,
        'mail_sendmail' => $mail_sendmail,
        'mail_pretend' => $mail_pretend,
        'mailgun_domain' => $mailgun_domain,
        'mailgun_secret' => $mailgun_secret,
        'mandrill_secret' => $mandrill_secret,
        'sparkpost_secret' => $sparkpost_secret,
        'ses_key' => $ses_key,
        'ses_secret' => $ses_secret,
        'ses_region' => $ses_region,

        //Social Networks
        'facebook_address' => $facebook_address,
        'google_plus_address' => $google_plus_address,
        'pinterest_address' => $pinterest_address,
        'twitter_address' => $twitter_address,
        'instagram_address' => $instagram_address,
        'linkedin_address' => $linkedin_address,
        'youtube_address' => $youtube_address,
        'tumblr_address' => $tumblr_address,
        'flickr_address' => $flickr_address,

        //Manage Ads
        'index_page_below_top_employes_ad' => $index_page_below_top_employes_ad,
        'above_footer_ad' => $above_footer_ad,
        'dashboard_page_ad' => $dashboard_page_ad,
        'cms_page_ad' => $cms_page_ad,
        'listing_page_vertical_ad' => $listing_page_vertical_ad,
        'listing_page_horizontal_ad' => $listing_page_horizontal_ad,

        //Captcha
        'nocaptcha_sitekey' => $nocaptcha_sitekey,
        'nocaptcha_secret' => $nocaptcha_secret,

        //Social Media Login
        'facebook_app_id' => $facebook_app_id,
        'facebook_app_secret' => $facebook_app_secret,
        'twitter_app_id' => $twitter_app_id,
        'twitter_app_secret' => $twitter_app_secret,

        //Payment Gateways
        'paypal_account' => $paypal_account,
        'paypal_client_id' => $paypal_client_id,
        'paypal_secret' => $paypal_secret,
        'paypal_live_sandbox' => $paypal_live_sandbox,
        'is_paypal_active' => $is_paypal_active,
        'stripe_key' => $stripe_key,
        'stripe_secret' => $stripe_secret,
        'is_stripe_active' => $is_stripe_active,
        'is_jobseeker_package_active' => $is_jobseeker_package_active,
        'is_company_package_active' => $is_company_package_active,

        //Home Page Slider
        'is_slider_active' => $is_slider_active,

        //Mail Chimp
        'mailchimp_api_key' => $mailchimp_api_key,
        'mailchimp_list_name' => $mailchimp_list_name,
        'mailchimp_list_id' => $mailchimp_list_id,


//            'site_name' => 'required|max:100',
//            'site_slogan' => 'required|max:150',
//            'image' => 'image',
//            'site_phone_primary' => 'required|max:20',
//            'site_phone_secondary' => 'max:20',
//            'mail_host' => 'max:100',
//            'mail_port' => 'max:5',
//            'mail_from_address' => 'required|max:100',
//            'mail_from_name' => 'required|max:100',
//            'mail_to_address' => 'required|max:100',
//            'mail_to_name' => 'required|max:100',
//            'default_country_id' => 'required|max:150',
//            'default_currency_code' => 'required|max:4',
//            'site_street_address' => 'required|max:250',
//            'mail_encryption' => 'max:10',
//            'mail_username' => 'max:100',
//            'mail_password' => 'max:100',
//            'mail_sendmail' => 'max:50',
//            'mail_pretend' => 'max:50',
//            'mailgun_domain' => 'max:100',
//            'mailgun_secret' => 'max:100',
//            'mandrill_secret' => 'max:100',
//            'sparkpost_secret' => 'max:100',
//            'ses_key' => 'max:100',
//            'ses_secret' => 'max:100',
//            'ses_region' => 'max:100',
        ];
    }

    public function messages()
    {
        return [
/*            'site_name.required' => 'Please enter site name.',
            'site_slogan.required' => 'Please enter site slogan.',
            'image.required' => 'Please select image.',
            'site_phone_primary.required' => 'Please enter site primary phone number.',
            'mail_from_address.required' => 'Please enter site email from address.',
            'mail_from_name.required' => 'Please enter site email from name.',
            'mail_to_address.required' => 'Please enter site email to address.',
            'mail_to_name.required' => 'Please enter site email to name.',
            'default_country_id.required' => 'Please enter site default country.',
            'default_currency_code.required' => 'Please enter site default currency code.',
            'site_street_address.required' => 'Please enter site street address.',
            'image.image' => 'Only images can be uploaded.',
            'site_name.max' => 'The site name may not be more than 100 characters.',
            'site_slogan.max' => 'The site slogan may not be more than 150 characters.',
            'site_phone_primary.max' => 'The site primary phone may not be more than 20 characters.',
            'site_phone_secondary.max' => 'The site secondary phone may not be more than 20 characters.',
            'default_country.max' => 'The site default country may not be more than 150 characters.',
            'default_currency_code.max' => 'The site default currency code may not be more than 4 characters.',
            'site_street_address.max' => 'The site street address may not be more than 250 characters.',
            'mail_host.max' => 'The mail host may not be more than 100 characters.',
            'mail_port.max' => 'The mail port may not be more than 5 characters.',
            'mail_from_address.max' => 'The mail from address may not be more than 100 characters.',
            'mail_from_name.max' => 'The mail from name may not be more than 100 characters.',
            'mail_to_address.max' => 'The mail to address may not be more than 100 characters.',
            'mail_to_name.max' => 'The mail to name may not be more than 100 characters.',
            'mail_encryption.max' => 'The mail encryption may not be more than 10 characters.',
            'mail_username.max' => 'The mail username may not be more than 100 characters.',
            'mail_password.max' => 'The mail password may not be more than 100 characters.',
            'mail_sendmail.max' => 'The mail sendmail may not be more than 100 characters.',
            'mail_pretend.max' => 'The mail pretend may not be more than 50 characters.',
            'mailgun_domain.max' => 'The mailgun domain may not be more than 100 characters.',
            'mailgun_secret.max' => 'The mailgun secret may not be more than 100 characters.',
            'mandrill_secret.max' => 'The mandrill secret may not be more than 100 characters.',
            'sparkpost_secret.max' => 'The sparkpost secret may not be more than 100 characters.',
            'ses_key.max' => 'The AMAZON SES key may not be more than 100 characters.',
            'ses_secret.max' => 'The AMAZON SES secret may not be more than 100 characters.',
            'ses_region.max' => 'The AMAZON SES region may not be more than 100 characters.',*/
        ];
    }

}
