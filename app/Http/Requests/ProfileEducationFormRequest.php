<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProfileEducationFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
            case 'POST': {
                    $id = (int) $this->input('id', 0);
            //degree_level_id
            $degree_level_id = config('rules.id_rule');
            //degree_title
            $degree_title = config('rules.alnum100_rule');
            //major_subjects
            $major_subjects = config('rules.array_rule');
            $major_subjects_star = config('rules.array_integer_star_rule');
            //country_id
            $country_id = config('rules.id_rule');
            //state_id
            $state_id = config('rules.id_rule');
            //city_id
            $city_id = config('rules.id_rule');
            //institution
            $institution = config('rules.alnum100_rule');
            //date_completion
            $date_completion = config('rules.id_rule');
            //degree_result
            $degree_result = config('rules.alnum20_rule');
            //result_type_id
            $result_type_id = config('rules.id_rule');

                    return [
                        "degree_level_id" => $degree_level_id,
                        //"degree_type_id" => "required",
                        "degree_title" => $degree_title,
                        "major_subjects" => $major_subjects,
                        "major_subjects.*" => $major_subjects_star,
                        "country_id" => $country_id,
                        "state_id" => $state_id,
                        "city_id" => $city_id,
                        "institution" => $institution,
                        "date_completion" => $date_completion,
                        "degree_result" => $degree_result,
                        "result_type_id" => $result_type_id,

//                        "degree_level_id" => "required|integer",
                        //"degree_type_id" => "required",
//                        "degree_title" => "required|max:150",
//                        "major_subjects" => "required|array",
//                        "major_subjects.*" => "integer",
//                        "country_id" => "required|integer",
//                        "state_id" => "required|integer",
//                        "city_id" => "required|integer",
//                        "institution" => "required|max:150",
//                        "date_completion" => "required|max:15",
//                        "degree_result" => "required|max:20",
//                        "result_type_id" => "required|integer",
                    ];
                }
            default:break;
        }
    }

    public function messages()
    {
        return [
/*            'degree_level_id.required' => 'Please select degree level.',
            'degree_type_id.required' => 'Please select degree type.',
            'degree_title.required' => 'Please enter degree title.',
            'major_subjects.required' => 'Please select major subjects.',
            'country_id.required' => 'Please select country.',
            'state_id.required' => 'Please select state.',
            'city_id.required' => 'Please select city.',
            'institution.required' => 'Please enter institution.',
            'date_completion.required' => 'Please set completion date.',
            'degree_result.required' => 'Please enter result.',
            'result_type_id.required' => 'Please select result type.',*/
        ];
    }

}
