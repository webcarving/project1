<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CmsFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = (int) $this->input('id', 0);
        $id_str = '';
        if ($id > 0) {
            $id_str = ',' . $id;
        }
        $page_slug = config('rules.slug_rule');
        array_push($page_slug,
            'unique:cms,page_slug' . $id_str
        );
        $seo_title = config('rules.alnum100_rule');
        $seo_description = config('rules.alnum500_rule');
        $seo_keywords = config('rules.alnum500_rule');
        $seo_other = config('rules.alnum500_rule_optional');

        return [
            'page_slug' => $page_slug,
            'seo_title' => $seo_title,
            'seo_description' => $seo_description,
            'seo_keywords' => $seo_keywords,
            'seo_other' => $seo_other,

//            'page_slug' => 'required|alpha_dash|unique:cms,page_slug' . $id_str,
//            'seo_title' => 'required',
//            'seo_description' => 'required',
//            'seo_keywords' => 'required'
        ];
    }

    public function messages()
    {
        return [
/*            'page_slug.required' => 'Please enter page slug.',
            'page_slug.alpha_dash' => 'The page slug may have alpha-numeric characters, as well as dashes and underscores.',
            'page_slug.unique' => 'Some other C.M.S page already has this Slug (Page SlUG should be unique).',
            'seo_title.required' => 'Please enter SEO title.',
            'seo_description.required' => 'Please enter SEO description.',
            'seo_keywords.required' => 'Please enter SEO keywords.',*/
        ];
    }

}
