<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class JobExperienceFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
            case 'POST': {
                    $id = (int) $this->input('id', 0);
                    $job_experience_unique = '';
                    if ($id > 0) {
                        $job_experience_unique = ',id,' . $id;
                    }
            //job_experience
            $job_experience = config('rules.alnum20_rule');
            array_push($job_experience,
                'unique:job_experiences' . $job_experience_unique
            );
            //job_experience_id
            $job_experience_id = array();
            $is_default = (int) $this->input('is_default', 0);
            if ($is_default == 0) {
                $job_experience_id = config('rules.id_rule');
            }
            //is_active
            $is_active = config('rules.bool_rule');
            //is_default
            $is_default = config('rules.bool_rule');
            //lang
            $lang = config('rules.lang_rule');


                    return [
                        'job_experience' => $job_experience,
                        'job_experience_id' => $job_experience_id,
                        'is_active' => $is_active,
                        'is_default' => $is_default,
                        'lang' => $lang,

//                        'job_experience' => 'required|max:200|unique:job_experiences' . $job_experience_unique,
//                        'job_experience_id' => 'required_if:is_default,0',
//                        'is_active' => 'required|boolean',
//                        'is_default' => 'required|boolean',
//                        'lang' => 'required|max:10',
                    ];
                }
            default:break;
        }
    }

    public function messages()
    {
        return [
/*            'job_experience.required' => 'Please enter Job Experience.',
            'job_experience_id.required_if' => 'Please select default/fallback Job Experience.',
            'is_default.required' => 'Is this Job Experience default?',
            'is_active.required' => 'Please select status.',
            'lang.required' => 'Please select language.',*/
        ];
    }

}
