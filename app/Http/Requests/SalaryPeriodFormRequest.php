<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SalaryPeriodFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
            case 'POST': {
                    $id = (int) $this->input('id', 0);
                    $salary_period_unique = '';
                    if ($id > 0) {
                        $salary_period_unique = ',id,' . $id;
                    }
            //lang
            $lang = config('rules.lang_rule');
            //salary_period
            $salary_period = config('rules.alnum50_rule');
            //salary_period_id
            $salary_period_id = array();
            $is_default = (int) $this->input('is_default', 0);
            if ($is_default == 0) {
                $salary_period_id = config('rules.id_rule');
            }
            //is_active
            $is_active = config('rules.bool_rule');
            //is_default
            $is_default = config('rules.bool_rule');

                    return [
                        'salary_period' => $salary_period,
                        'salary_period_id' => $salary_period_id,
                        'is_active' => $is_active,
                        'is_default' => $is_default,
                        'lang' => $lang,

//                        'salary_period' => 'required|unique:salary_periods' . $salary_period_unique,
//                        'salary_period_id' => 'required_if:is_default,0',
//                        'is_active' => 'required|boolean',
//                        'is_default' => 'required|boolean',
//                        'lang' => 'required|max:10',
                    ];
                }
            default:break;
        }
    }

    public function messages()
    {
        return [
/*            'salary_period.required' => 'Please enter salary period.',
            'salary_period_id.required_if' => 'Please select default/fallback salary period.',
            'is_default.required' => 'Is this salary period default?',
            'is_active.required' => 'Please select status.',
            'lang.required' => 'Please select language.',*/
        ];
    }

}
