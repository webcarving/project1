<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProfileProjectImageFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
            case 'POST': {
                    $id = (int) $this->input('id', 0);
                    $image = config('rules.image_rule_optional') ;
                    return [
                        "image" => $image
//                        "image" => 'mimes:jpeg,png|max:50|dimensions:width=150,height=150',
                    ];
                }
            default:break;
        }
    }

    public function messages()
    {
        return [
//            'image.image' => 'Only images can be uploaded.',
        ];
    }

}
