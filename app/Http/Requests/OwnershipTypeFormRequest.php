<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class OwnershipTypeFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
            case 'POST': {
                    $id = (int) $this->input('id', 0);
                    $ownership_type_unique = '';
                    if ($id > 0) {
                        $ownership_type_unique = ',id,' . $id;
                    }
            //lang
            $lang = config('rules.lang_rule');
            //ownership_type
            $ownership_type = config('rules.alnum50_rule');
            //is_active
            $is_active = config('rules.bool_rule');
            //ownership_type_id
            $ownership_type_id = array();
            $is_default = (int) $this->input('is_default', 0);
            if ($is_default == 0) {
                $ownership_type_id = config('rules.id_rule');
            }
            //is_default
            $is_default = config('rules.bool_rule');
                    $id = array();
                    return [
                        "id" => $id,
                        "lang" => $lang,
                        "ownership_type" => $ownership_type,
                        "is_default" => $is_default,
                        "ownership_type_id" => $ownership_type_id,
                        "is_active" => $is_active,

//                        "id" => "",
//                        "lang" => "required|max:10",
//                        "ownership_type" => "required|max:200",
//                        "is_default" => "required|boolean",
//                        "ownership_type_id" => "required_if:is_default,0",
//                        "is_active" => "required|boolean",
                    ];
                }
            default:break;
        }
    }

    public function messages()
    {
        return [
//            'lang.required' => 'Please select language.', 'ownership_type.required' => 'Please enter Ownership Type.', 'is_default.required' => 'Is this Ownership Type default/fallback ?.', 'ownership_type_id.required_if' => 'Please select default/fallback Ownership Type.', 'is_active.required' => 'Is this Ownership Type active?',
        ];
    }

}
