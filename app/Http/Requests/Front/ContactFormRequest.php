<?php

namespace App\Http\Requests\Front;

use App\Http\Requests\Request;

class ContactFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //full_name
        $full_name = config('rules.name_rule');
        //email
        $email = config('rules.email_rule');
        //phone
        $phone = config('rules.phone_rule_optional');
        //subject
        $subject = config('rules.alnum100_rule');
        //message_txt
        $message_txt = config('rules.alnum300_rule');
        //g_recaptcha_response
        $g_recaptcha_response = config('rules.captcha_rule');

        return [
            'full_name' => $full_name,
            'email' => $email,
            'phone' => $phone,
            'subject' => $subject,
            'message_txt' => $message_txt,
            'g-recaptcha-response' => $g_recaptcha_response,


//            'full_name' => 'required|max:100',
//            'email' => 'required|email|max:100',
//            'phone' => 'max:20',
//            'subject' => 'required|max:200',
//            'message_txt' => 'required',
//            'g-recaptcha-response' => 'required|captcha',
        ];
    }

    public function messages()
    {
        return [
/*            'full_name.required' => __('Name is required'),
            'email.required' => __('E-mail address is required'),
            'email.email' => __('Valid e-mail address is required'),
            'subject.required' => __('Subject is required'),
            'message_txt.required' => __('Message is required'),*/
        ];
    }

}
