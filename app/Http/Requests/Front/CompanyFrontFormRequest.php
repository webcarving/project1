<?php

namespace App\Http\Requests\Front;

use Auth;
use App\Http\Requests\Request;

class CompanyFrontFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
            case 'POST': {
                    $id = (int) Auth::guard('company')->user()->id;
                    //name
                    $name = config('rules.name_rule');
                    //email
                    $unique_id = ($id > 0) ? ',' . $id : '';
                    $email = config('rules.email_rule');
                    array_push($email,
                        'unique:companies,email' . $unique_id
                    );
                    //logo
                    $logo = config('rules.image_rule_optional');
                    //ceo
                    $ceo = config('rules.alnum150_rule');
                    //industry_id
                    $industry_id = config('rules.id_rule');
                    //ownership_type_id
                    $ownership_type_id = config('rules.id_rule');
                    //description
                    $description = config('rules.alnum200_rule');
                    //no_of_offices
                    $no_of_offices = config('rules.numbertill_rule');
                    //website
                    $website = config('rules.alnum150_rule');
                    //no_of_employees
                    $no_of_employees = config('rules.id_rule');
                    //established_in
                    $established_in = config('rules.id_rule');
                    //fax
                    $fax = config('rules.phone_rule_optional');
                    //phone
                    $phone = config('rules.phone_rule');

                    //facebook
                    $facebook = config('rules.alnum100_rule_optional');
                    //twitter
                    $twitter = config('rules.alnum100_rule_optional');
                    //linkedin
                    $linkedin = config('rules.alnum100_rule_optional');
                    //google_plus
                    $google_plus = config('rules.alnum100_rule_optional');
                    //pinterest
                    $pinterest = config('rules.alnum100_rule_optional');

                    //country_id
                    $country_id = config('rules.id_rule');
                    //state_id
                    $state_id = config('rules.id_rule');
                    //city_id
                    $city_id = config('rules.id_rule');
                    //is_subscribed
                    $is_subcribed = config('rules.bool_rule');
                    $id = array();

                    return [
                        "id" => $id,
                        "name" => $name,
                        'email' => $email,
                        "logo" => $logo,

                        "ceo" => $ceo,
                        "industry_id" => $industry_id,
                        "ownership_type_id" => $ownership_type_id,

                        "description" => $description,

                        "no_of_offices" => $no_of_offices,
                        "website" => $website,
                        "no_of_employees" => $no_of_employees,

                        "established_in" => $established_in,
                        "fax" => $fax,
                        "phone" => $phone,

                        "facebook" => $facebook,
                        "twitter" => $twitter,

                        "linkedin" => $linkedin,
                        "google_plus" => $google_plus,
                        "pinterest" => $pinterest,

                        "country_id" => $country_id,
                        "state_id" => $state_id,
                        "city_id" => $city_id,
                        "is_subscribed" => $is_subcribed


//                        "id" => "",
//                        "name" => "required|max:155",
//                        'email' => 'required|unique:companies,email' . $unique_id . '|email|max:100',
//                        "logo" => 'mimes:jpeg,png|max:50|dimensions:width=150,height=150',

//                        "ceo" => "required|max:60",
//                        "industry_id" => "required|integer",
//                        "ownership_type_id" => "required|integer",

//                        "description" => "required",
                        //"location" => "required|max:150",
                        //"map" => "required",

//                        "no_of_offices" => "required|integer",
//                        "website" => "required|url|max:155",
//                        "no_of_employees" => "required|max:15",

//                        "established_in" => "required|max:12",
//                        "fax" => "max:30",
//                        "phone" => "required|max:30",

//                        "facebook" => "max:250",
//                        "twitter" => "max:250",

//                        "linkedin" => "max:250",
//                        "google_plus" => "max:250",
//                        "pinterest" => "max:250",

//                        "country_id" => "required|integer",
//                        "state_id" => "required|integer",
//                        "city_id" => "required|integer",
//                        "is_subscribed" => "int"
                    ];
                }
            default:break;
        }
    }

    public function messages()
    {
        return [
/*            'name.required' => __('Name is required'),
            'email.required' => __('Email is required'),
            'email.email' => __('The email must be a valid email address'),
            'email.unique' => __('This Email has already been taken'),
//            'password.required' => __('Password is required'),
            'ceo.required' => __('CEO name is required'),
            'industry_id.required' => __('Please select Industry'),
            'ownership_type_id.required' => __('Please select Ownership Type'),
            'description.required' => __('Description required'),
//            'location.required' => __('Location required'),
            'map.required' => __('Google Map required'),
            'no_of_offices.required' => __('Number of offices required'),
            'website.required' => __('Website required'),
            'website.url' => __('Complete url of website required'),
            'no_of_employees.required' => __('Number of employees required'),
            'established_in.required' => __('Established in year required'),
            'fax.required' => __('Fax number required'),
            'phone.required' => __('Phone number required'),
            'logo.image' => __('Only Images can be used as logo'),
            'country_id.required' => __('Please select country'),
            'state_id.required' => __('Please select state'),
            'city_id.required' => __('Please select city'),
//            "latitude.between" =>__('Please Enter Correct Latitude'),
//            "longitude.between" =>__('Please Enter Correct Longitude'),
            'logo.mimes' => __('The image must be a file of type: jpeg, png'),
            'logo.max' => __('The image may not be greater than 50 kilobytes'),
            'logo.dimensions' => __('The image dimension should be : 150x150 pixels')*/
        ];
    }

}
