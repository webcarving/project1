<?php

namespace App\Http\Requests\Front;

use Auth;
use App\Http\Requests\Request;

class CompanyFrontRegisterFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id_str = '';
        //name
        $name = config('rules.name_rule');
        //email
        $email = config('rules.email_rule');
        array_push($email,
            'unique:companies,email' . $id_str
        );
        //password
        $password = config('rules.password_rule');
        //terms_of_use
        $terms_of_use = config('rules.bool_rule');
        //g_recaptcha_response
        $g_recaptcha_response = config('rules.captcha_rule');

        return [
            'name' => $name,
            'email' => $email,
            'password' => $password,
            'terms_of_use' => $terms_of_use,
            'g-recaptcha-response' => $g_recaptcha_response,

//            'name' => 'required|max:150',
//            'email' => 'required|unique:companies,email|email|max:100',
//            'password' => 'required|confirmed|min:6|max:50',
//            'terms_of_use' => 'required',
//            'g-recaptcha-response' => 'required|captcha',
        ];
    }

    public function messages()
    {
        return [
/*            'name.required' => __('Name is required'),
            'email.required' => __('Email is required'),
            'email.email' => __('The email must be a valid email address'),
            'email.unique' => __('This Email has already been taken'),
            'password.required' => __('Password is required'),
            'password.min' => __('The password should be more than 6 characters long'),
            'terms_of_use.required' => __('Please accept terms of use'),
            'g-recaptcha-response.required' => __('Please verify that you are not a robot'),
            'g-recaptcha-response.captcha' => __('Captcha error! try again later or contact site admin'),*/
        ];
    }

}
