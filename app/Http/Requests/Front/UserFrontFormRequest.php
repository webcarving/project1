<?php

namespace App\Http\Requests\Front;

use Auth;
use App\Http\Requests\Request;

class UserFrontFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = Auth::user()->id;
        $id_str = ',' . $id;

        //first name
        $first_name = config('rules.first_name_rule');
        //middle name
        $middle_name = config('rules.middle_name_rule_optional');
        //last name
        $last_name = config('rules.last_name_rule');
        //email
        $email = config('rules.email_rule');
        array_push($email,
            'unique:users,email' . $id_str
        );
        //date_of_birth
        $date_of_birth = config('rules.date_rule');
        //gender_id
        $gender_id = config('rules.id_rule');
        //marital_status_id
        $marital_status_id = config('rules.id_rule');
        //nationality_id
        $nationality_id = config('rules.id_rule');
        //national_id_card_number
        $national_id_card_number = config('rules.idbig_rule');
        //country_id
        $country_id = config('rules.id_rule');
        //state_id
        $state_id = config('rules.id_rule');
        //city_id
        $city_id = config('rules.id_rule');
        //phone
        $phone = config('rules.phone_rule');
        //mobile_num
        $mobile_num = config('rules.phone_rule');
        //whatsapp
        $whatsapp = config('rules.phone_rule');
        //telegram
        $telegram = config('rules.alnum50_rule');
        //skype
        $skype = config('rules.alnum50_rule');
        //job_experience_id
        $job_experience_id = config('rules.id_rule');
        //career_level_id
        $career_level_id = config('rules.id_rule');
        //industry_id
        $industry_id = config('rules.id_rule');
        //functional_area_id
        $functional_area_id = config('rules.id_rule');
        //current_salary
        $current_salary = config('rules.salary_rule');
        //expected_salary
        $expected_salary = config('rules.salary_rule');
        //salary_currency
        $salary_currency = config('rules.alnum3_rule');
        //street_address
        $street_address = config('rules.alnum300_rule');
        //profile summary
        $profile_summary = config('rules.alnum200_rule_optional');
        //image
        $image = config('rules.image_rule_optional');

        return [
            'first_name' => $first_name,
            'middle_name' => $middle_name,
            'last_name' => $last_name,
            'email' => $email,
            //'father_name' => 'required',
            'date_of_birth' => $date_of_birth,
            'gender_id' => $gender_id,
            'marital_status_id' => $marital_status_id,
            'nationality_id' => $nationality_id,
            'national_id_card_number' => $national_id_card_number,
            'country_id' => $country_id,
            'state_id' => $state_id,
            'city_id' => $city_id,
            'phone' => $phone,
            'mobile_num' => $mobile_num,
            'whatsapp' => $whatsapp,
            'telegram' => $telegram,
            'skype' => $skype,
            'job_experience_id' => $job_experience_id,
            'career_level_id' => $career_level_id,
            'industry_id' => $industry_id,
            'functional_area_id' => $functional_area_id,
            //'current_salary' => 'required|max:100',
            //'expected_salary' => 'required|max:100',
            'current_salary' => $current_salary,
            'expected_salary' => $expected_salary,
            'salary_currency' => $salary_currency,
            'street_address' => $street_address,
            'profile_summary' => $profile_summary,
            'image' => $image,
        ];


//        $id = Auth::user()->id;
//        $id_str = ',' . $id;
//        $password = '';
//        return [
//            'first_name' => 'required|max:100',
//            'middle_name' => 'max:100',
//            'last_name' => 'required|max:100',
//            'email' => 'required|unique:users,email' . $id_str . '|email|max:100',
//            'gender_id' => 'required|integer',
//            'marital_status_id' => 'required|integer',
//            'country_id' => 'required|max:50',
//            'state_id' => 'required|max:50',
//            'city_id' => 'required|max:50',
//            'nationality_id' => 'required|integer',
//            'date_of_birth' => 'required|date_format:"M j, Y"',
//            'national_id_card_number' => 'required|integer',
            //'password' => 'max:50',
            //'father_name' => 'required|max:80',
//            'phone' => 'max:18',
//            'mobile_num' => 'required|max:25',
//            'job_experience_id' => 'required|integer',
//            'career_level_id' => 'required|integer',
//            'industry_id' => 'required|integer',
//            'functional_area_id' => 'required|integer',
//            'whatsapp' => 'max:50',
//            'telegram' => 'max:50',
//            'skype' => 'max:50',
            //'whatsapp'=>'required',
//            'current_salary' => array(
//                            'required',
//                            'regex:/^(?:0|([1-9]\d{0,2}(,\d{3}){0,12}))$/m',
//            ),
//            'expected_salary' => array(
//                'required',
//                'regex:/^(?:0|([1-9]\d{0,2}(,\d{3}){0,12}))$/m',
//            ),
//            'salary_currency' => 'required|max:10',
//            'street_address' => 'required|max:230',
//            'is_subscribed' => 'required|boolean|max:1',
//            'image' => 'image',
//            'image_new' => 'mimes:jpeg,png|max:50|dimensions:width=150,height=150'
//        ];
    }

    public function messages()
    {
        return [
/*            'first_name.required' => __('First Name is required'),
            'middle_name.required' => __('Middle Name is required'),
            'last_name.required' => __('Last Name is required'),
            'email.required' => __('Email is required'),
            'email.email' => __('The email must be a valid email address'),
            'email.unique' => __('This Email has already been taken'),
            'password.required' => __('Password is required'),
            'password.min' => __('The password should be more than 6 characters long'),
//            'father_name.required' => __('Father Name is required'),
            'date_of_birth.required' => __('Date of birth is required'),
            'gender_id.required' => __('Please select gender'),
            'marital_status_id.required' => __('Please select marital status'),
            'nationality_id.required' => __('Please select nationality'),
            'national_id_card_number.required' => __('National ID card# required'),
            'country_id.required' => __('Please select country'),
            'state_id.required' => __('Please select state'),
            'city_id.required' => __('Please select city'),
//            'phone.required' => __('Please enter phone'),
            'mobile_num.required' => __('Please enter mobile number'),
            'job_experience_id.required' => __('Please select experience'),
            'career_level_id.required' => __('Please select career level'),
            'industry_id.required' => __('Please select industry'),
            'functional_area_id.required' => __('Please select functional area'),
            'current_salary.required' => __('Please enter current salary'),
            'expected_salary.required' => __('Please enter expected salary'),
            'salary_currency.required' => __('Please select salary currency'),
            'street_address.required' => __('Please enter street address'),
//            'whatsapp.required'=>  __('Please enter whatsapp No'),
            'image.image' => __('Only images can be uploaded'),
            'image_new.mimes' => __('The image must be a file of type: jpeg, png'),
            'image_new.max' => __('The image may not be greater than 50 kilobytes'),
            'image_new.dimensions' => __('The image dimension should be : 150x150 pixels')*/
        ];
    }

}
