<?php

namespace App\Http\Requests\Front;

use App\Http\Requests\Request;
use Illuminate\Support\Str;

class JobFrontFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
            case 'POST': {
                    $id = (int) $this->input('id', 0);

            //title
            $title = config('rules.alnum150_rule');
            //description
            $description = config('rules.text_rule');
            //skills
            $skills = config('rules.array_rule');
            //skills_star
            $skills_star = config('rules.array_integer_star_rule');
            //benefits
            $benefits = config('rules.array_rule_optional');
            //benefits_star
            $benefits_star = config('rules.array_integer_star_rule');
            //salary_from
            $salary_from = config('rules.salary_rule');
            //salary_to
            $salary_to = config('rules.salary_rule');
            //salary_currency
            $salary_currency = config('rules.alnum3_rule');
            //salary_period_id
            $salary_period_id = config('rules.id_rule');
            //hide_salary
            $hide_salary = config('rules.id_rule');
            //career_level_id
            $career_level_id = config('rules.id_rule');
            //functional_area_id
            $functional_area_id = config('rules.id_rule');
            //job_type_id
            $job_type_id = config('rules.id_rule');
            //job_shift_id
            $job_shift_id = config('rules.id_rule');
            //num_of_positions
            $num_of_positions = config('rules.id_rule');
            //gender_id
            $gender_id = config('rules.id_rule');
            //expiry_date
            $expiry_date = config('rules.date_rule');
            //degree_level_id
            $degree_level_id = config('rules.id_rule');
            //job_experience_id
            $job_experience_id = config('rules.id_rule');
            //is_freelance
            $is_freelance = config('rules.bool_rule');
            //language
            $language = config('rules.array_rule');
            //language_star
            $language_star = config('rules.array_integer_star_rule');

/*            foreach ($request->except('_token') as $key => $part) {
                // $key gives you the key. 2 and 7 in your case.
            }*/
$locations_rules = array();
foreach($this->input() as $key => $value){
    if(Str::startsWith($key, 'locations_') && $key != ''){
        $locations_rules[$key] = config('rules.array_rule');
        $locations_rules[$key.'.*'] = config('rules.array_integer_star_rule');
//        $locations_rules[] = array($key => config('rules.array_rule'));
    }
};

//job_location_id
$job_location_id = array();
if(count($locations_rules) == 0){
    $job_location_id = config('rules.id_rule');
}
                    return ([
                        "title" => $title,
                        "description" => $description,
                        "skills" => $skills,
                        "skills.*" => $skills_star,
                        "benefits" => $benefits,
                        "benefits.*" => $benefits_star,
                        'salary_from' => $salary_from,
                        'salary_to' => $salary_to,
                        "salary_currency" => $salary_currency,
                        "salary_period_id" => $salary_period_id,
                        "hide_salary" => $hide_salary,
                        "career_level_id" => $career_level_id,
                        "functional_area_id" => $functional_area_id,
                        "job_type_id" => $job_type_id,
                        "job_shift_id" => $job_shift_id,
                        "num_of_positions" => $num_of_positions,
                        "gender_id" => $gender_id,
                        "expiry_date" => $expiry_date,
                        "degree_level_id" => $degree_level_id,
                        "job_experience_id" => $job_experience_id,
                        "is_freelance" => $is_freelance,
                        "language" => $language,
                        "language.*" => $language_star,
                        //"country_id" => "required",
                        //"state_id" => "required",
                        //"city_id" => "required",
                        "job_location_id" => $job_location_id,

//                        "title" => "required|max:200",
//                        "description" => "required",
//                        "skills" => "required",
//                        "skills.*" => "required|integer",
//                        "benefits.*" => "integer",
//                        'salary_from' => array(
//                            'required',
//                            'regex:/^(?:0|([1-9]\d{0,2}(,\d{3}){0,12}))$/m',
//                        ),
//                        'salary_to' => array(
//                            'required',
//                            'regex:/^(?:0|([1-9]\d{0,2}(,\d{3}){0,12}))$/m',
//                        ),
//                        "salary_currency" => "required|max:5",
//                        "salary_period_id" => "required|integer",
//                        "hide_salary" => "required|boolean",
//                        "career_level_id" => "required|integer",
//                        "functional_area_id" => "required|integer",
//                        "job_type_id" => "required|integer",
//                        "job_shift_id" => "required|integer",
//                        "num_of_positions" => "required|integer",
//                        "gender_id" => "integer|nullable",
//                        "expiry_date" => 'required|date_format:"M j, Y"',
//                        "degree_level_id" => "required|integer",
//                        "job_experience_id" => "required|integer",
//                        "is_freelance" => "required|boolean",
//                        "language" => "required",
//                        "language.*" => "required|integer",
                        //"country_id" => "required",
                        //"state_id" => "required",
                        //"city_id" => "required",
//                        "job_location_id" => "required",
//                        "locations_*"
                    ] + $locations_rules);                }
            default:break;
        }
    }

    public function messages()
    {
        return [
/*            'title.required' => __('Please enter Job title'),
            'description.required' => __('Please enter Job description'),
            'skills.required' => __('Please enter Job skills'),
            'country_id.required' => __('Please select Country'),
            'state_id.required' => __('Please select State'),
            'city_id.required' => __('Please select City'),
            'is_freelance.required' => __('Is this freelance Job?'),
            'career_level_id.required' => __('Please select Career level'),
            'salary_from.required' => __('Please select salary from'),
            'salary_to.required' => __('Please select salary to'),
            'salary_currency.required' => __('Please select salary currency'),
            'salary_period_id.required' => __('Please select salary period'),
            'hide_salary.required' => __('Is salary hidden?'),
            'functional_area_id.required' => __('Please select functional area'),
            'job_type_id.required' => __('Please select job type'),
            'job_shift_id.required' => __('Please select job shift'),
            'num_of_positions.required' => __('Please select number of positions'),
            'gender_id.required' => __('Please select gender'),
            'expiry_date.required' => __('Please enter Job expiry date'), 
            'degree_level_id.required' => __('Please select degree level'),
            'job_experience_id.required' => __('Please select job experience'),
            'job_location_id.required' => __('Please select job location'),
            'language.required' => __('Please select language'),*/
        ];
    }

}
