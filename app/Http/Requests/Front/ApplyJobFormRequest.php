<?php

namespace App\Http\Requests\Front;

use App\Http\Requests\Request;

class ApplyJobFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
            case 'POST': {
                //cv_id
                $cv_id = config('rules.id_rule');
                //current_salary
                $current_salary = config('rules.salary_rule');
                //expected_salary
                $expected_salary = config('rules.salary_rule');
                //salary_currency
                $salary_currency = config('rules.alnum3_rule');

                    return [
                        "cv_id" => $cv_id,
                        'current_salary' => $current_salary,
                        'expected_salary' => $expected_salary,
                        "salary_currency" => $salary_currency,

//                        "cv_id" => "required",
//                        'current_salary' => array(
//                            'required',
//                            'regex:/^(?:0|([1-9]\d{0,2}(,\d{3}){0,12}))$/m',
//                        ),
//                        'expected_salary' => array(
//                            'required',
//                            'regex:/^(?:0|([1-9]\d{0,2}(,\d{3}){0,12}))$/m',
//                        ),
//                        "salary_currency" => "required|max:5",
                    ];
                }
            default:break;
        }
    }

    public function messages()
    {
        return [
/*            'cv_id.required' => __('Please select CV'),
            'current_salary.required' => __('Please enter current salary'),
            'expected_salary.required' => __('Please enter expected salary'),
            'salary_currency.required' => __('Please enter salary currency'),*/
        ];
    }

}
