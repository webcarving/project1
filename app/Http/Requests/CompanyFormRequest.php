<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class CompanyFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
            case 'POST': {
                    $id = (int) $this->input('id', 0);
                    //name
                    $name = config('rules.name_rule');
                    //email
                    $email = config('rules.email_rule');
                    //password
                    $password = config('rules.password_rule');
                    //ceo
                    $ceo = config('rules.alnum150_rule');
                    //industry_id
                    $industry_id = config('rules.id_rule');
                    //ownership_type_id
                    $ownership_type_id = config('rules.id_rule');
                    //description
                    $description = config('rules.alnum200_rule');
                    //no_of_offices
                    $no_of_offices = config('rules.id_rule');
                    //website
                    $website = config('rules.alnum150_rule');
                    //no_of_employees
                    $no_of_employees = config('rules.numbertill_rule');
                    //established_in
                    $established_in = config('rules.id_rule');
                    //fax
                    $fax = config('rules.phone_rule');
                    //phone
                    $phone = config('rules.phone_rule');

                    //facebook
                    $facebook = config('rules.alnum100_rule_optional');
                    //twitter
                    $twitter = config('rules.alnum100_rule_optional');
                    //linkedin
                    $linkedin = config('rules.alnum100_rule_optional');
                    //google_plus
                    $google_plus = config('rules.alnum100_rule_optional');
                    //pinterest
                    $pinterest = config('rules.alnum100_rule_optional');

                    //logo
                    $logo = config('rules.image_rule_optional');
                    //country_id
                    $country_id = config('rules.id_rule');
                    //state_id
                    $state_id = config('rules.id_rule');
                    //city_id
                    $city_id = config('rules.id_rule');
                    //company_package_id
                    $company_package_id = config('rules.id_rule');
                    //is_active
                    $is_active = config('rules.bool_rule');
                    //is_featured
                    $is_featured = config('rules.bool_rule');
                    //is_top_employer
                    $is_top_employer = config('rules.bool_rule');

//                    $unique_id = ($id > 0) ? ',' . $id : '';
//                    $password = ($id > 0) ? "" : "required";
//                    $logo = ($id > 0) ? "" : "required";
                    $id = array();
                    return [
                        "id" => $id,
                        "name" => $name,
                        "email" => $email,
                        "password" => $password,
                        "ceo" => $ceo,
                        "industry_id" => $industry_id,
                        "ownership_type_id" => $ownership_type_id,
                        "description" => $description,
                        "no_of_offices" => $no_of_offices,
                        "website" => $website,
                        "no_of_employees" => $no_of_employees,
                        "established_in" => $established_in,
                        "fax" => $fax,
                        "phone" => $phone,
                        "facebook" => $facebook,
                        "twitter" => $twitter,
                        "linkedin" => $linkedin,
                        "google_plus" => $google_plus,
                        "pinterest" => $pinterest,
                        "logo" => $logo,
                        "country_id" => $country_id,
                        "state_id" => $state_id,
                        "city_id" => $city_id,
                        "company_package_id" => $company_package_id,
                        "is_active" => $is_active,
                        "is_featured" => $is_featured,
                        "is_top_employer" => $is_top_employer,

//                        "id" => "",
//                        "name" => "required|max:155",
//                        "email" => "required|max:100",
//                        "password" => $password,
//                        "ceo" => "required|max:60",
//                        "industry_id" => "required|integer",
//                        "ownership_type_id" => "required|integer",
//                        "description" => "required",
//                        "location" => "required|max:155",
                        //"map" => "required",
//                        "no_of_offices" => "required|integer",
//                        "website" => "required|url",
//                        "no_of_employees" => "required|max:15",
//                        "established_in" => "required|max:12",
//                        "fax" => "required|max:30",
//                        "phone" => "required|max:30",
//                        "logo" => $logo,
//                        "country_id" => "required|integer",
//                        "state_id" => "required|integer",
//                        "city_id" => "required|integer",
//                        "is_active" => "required|boolean",
//
//                        "is_featured" => "required|boolean",
                    ];
                }
            default:break;
        }
    }

    public function messages()
    {
        return [
/*            'name.required' => 'Company Name is required',
            'email.required' => 'Company Email is required',
            'password.required' => 'Password is required',
            'ceo.required' => 'Company\'s CEO name is required',
            'industry_id.required' => 'Please select Industry',
            'ownership_type_id.required' => 'Please select Ownership Type',
            'description.required' => 'Company Details required',
            'location.required' => 'Company location required',
            'map.required' => 'Company Google Map location required',
            'no_of_offices.required' => 'Number of offices required',
            'website.required' => 'Company website required',
            'website.url' => 'Complete url of company website required',
            'no_of_employees.required' => 'Number of employees required',
            'established_in.required' => 'Company established in year required',
            'fax.required' => 'Fax number required',
            'phone.required' => 'Phone number required',
            'logo.required' => 'Company logo is required',
            'country_id.required' => 'Please select country',
            'state_id.required' => 'Please select state',
            'city_id.required' => 'Please select city',
            'is_active.required' => 'Is this Company Acive?',
            'is_featured.required' => 'Is this Company featured?',*/
        ];
    }

}
