<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProfileCvFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
            case 'POST': {
                    $id = (int) $this->input('id', 0);
            //title
            $title = config('rules.alnum100_rule');
            //is_default
            $is_default = config('rules.bool_rule');

//                    $cv_file = ($id > 0) ? '' : 'required|';
                    $cv_file = config('rules.cv_file_rule_optional');
                    if($id > 0){
                        array_push($cv_file,
                            'required'
                        );
                    }

                    return [
                        "title" => $title,
                        "is_default" => $is_default,
                        //  "cv_file" => $cv_file . 'mimetypes:application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword,application/pdf|max: 1000',
                        "cv_file" => $cv_file,

//                        "title" => "required|max:100",
//                        "is_default" => "required",
                    //  "cv_file" => $cv_file . 'mimetypes:application/vnd.openxmlformats-officedocument.wordprocessingml.document,application/msword,application/pdf|max: 1000',
//                        "cv_file" => $cv_file . 'mimes:pdf,doc,docx,zip|max: 1000',
                    ];
                }
            default:break;
        }
    }

    public function messages()
    {
        return [
/*            'title.required' => 'Please enter CV title.',
            'is_default.required' => 'Is this CV default?',
            'cv_file.required' => 'Please select CV file.',
//            'cv_file.mimetypes' => 'Only PDF and DOC and DOCX files can be uploaded.',
            'cv_file.mimes' => 'Only PDF and DOC and DOCX files can be uploaded.',
            'cv_file.max' => 'The cv file may not be greater than 1 MB.',*/
        ];
    }

}
