<?php

namespace App\Http\Requests;

use Auth;
use App\Http\Requests\Request;

class UserFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $id = (int) $this->input('id', 0);
        $pass_required = config('rules.password_rule');
        $id_str = '';
        if ($id > 0) {
            $id_str = ',' . $id;
            $pass_required = '';
        }

        //first name
        $first_name = config('rules.first_name_rule');
        //middle name
        $middle_name = config('rules.middle_name_rule_optional');
        //last name
        $last_name = config('rules.last_name_rule');
        //email
        $email = config('rules.email_rule');
        array_push($email,
            'unique:users,email' . $id_str
        );
        //password
        $password = $pass_required;
        //date_of_birth
        $date_of_birth = config('rules.date_rule');
        //gender_id
        $gender_id = config('rules.id_rule');
        //marital_status_id
        $marital_status_id = config('rules.id_rule');
        //nationality_id
        $nationality_id = config('rules.id_rule');
        //national_id_card_number
        $national_id_card_number = config('rules.idbig_rule');
        //country_id
        $country_id = config('rules.id_rule');
        //state_id
        $state_id = config('rules.id_rule');
        //city_id
        $city_id = config('rules.id_rule');
        //phone
        $phone = config('rules.phone_rule');
        //mobile_num
        $mobile_num = config('rules.phone_rule');
        //whatsapp
        $whatsapp = config('rules.phone_rule');
        //telegram
        $telegram = config('rules.alnum50_rule');
        //skype
        $skype = config('rules.alnum50_rule');
        //job_experience_id
        $job_experience_id = config('rules.id_rule');
        //career_level_id
        $career_level_id = config('rules.id_rule');
        //industry_id
        $industry_id = config('rules.id_rule');
        //functional_area_id
        $functional_area_id = config('rules.id_rule');
        //current_salary
        $current_salary = config('rules.salary_rule');
        //expected_salary
        $expected_salary = config('rules.salary_rule');
        //salary_currency
        $salary_currency = config('rules.alnum3_rule');
        //street_address
        $street_address = config('rules.alnum300_rule');
        //profile summary
        $profile_summary = config('rules.alnum200_rule_optional');
        //image
        $image = config('rules.image_rule_optional');


        return [
            'first_name' => $first_name,
            'middle_name' => $middle_name,
            'last_name' => $last_name,
            'email' => $email,
            'password' => $password,
            //'father_name' => 'required',
            'date_of_birth' => $date_of_birth,
            'gender_id' => $gender_id,
            'marital_status_id' => $marital_status_id,
            'nationality_id' => $nationality_id,
            'national_id_card_number' => $national_id_card_number,
            'country_id' => $country_id,
            'state_id' => $state_id,
            'city_id' => $city_id,
            'phone' => $phone,
            'mobile_num' => $mobile_num,
            'whatsapp' => $whatsapp,
            'telegram' => $telegram,
            'skype' => $skype,
            'job_experience_id' => $job_experience_id,
            'career_level_id' => $career_level_id,
            'industry_id' => $industry_id,
            'functional_area_id' => $functional_area_id,
            //'current_salary' => 'required|max:100',
            //'expected_salary' => 'required|max:100',
            'current_salary' => $current_salary,
            'expected_salary' => $expected_salary,
            'salary_currency' => $salary_currency,
            'street_address' => $street_address,
            'profile_summary' => $profile_summary,
            'image' => $image,



//            'first_name' => 'required|max:100',
            //'middle_name' => 'required',
//            'last_name' => 'required|max:100',
//            'email' => 'required|unique:users,email' . $id_str . '|email',
//            'password' => $password,
            //'father_name' => 'required',
//            'date_of_birth' => 'required|date_format:"M j, Y"',
//            'gender_id' => 'required|integer',
//            'marital_status_id' => 'required|integer',
//            'nationality_id' => 'required|integer',
//            'national_id_card_number' => 'required|max:100',
//            'country_id' => 'required|max:50',
//            'state_id' => 'required|max:50',
//            'city_id' => 'required|max:50',
//            'phone' => 'required|max:20',
//            'mobile_num' => 'required|max:25',
//            'job_experience_id' => 'required|integer',
//            'career_level_id' => 'required|integer',
//            'industry_id' => 'required|integer',
//            'functional_area_id' => 'required|integer',
            //'current_salary' => 'required|max:100',
            //'expected_salary' => 'required|max:100',
//            'current_salary' => array(
//                'required',
//                'regex:/^(?:0|([1-9]\d{0,2}(,\d{3}){0,12}))$/m',
//            ),
//            'expected_salary' => array(
//                'required',
//                'regex:/^(?:0|([1-9]\d{0,2}(,\d{3}){0,12}))$/m',
//            ),
//            'salary_currency' => 'required|max:5',
//            'street_address' => 'required',
//            'image' => 'image',
        ];
    }

    public function messages()
    {
        return [
/*            'first_name.required' => 'First Name is required',
            'middle_name.required' => 'Middle Name is required',
            'last_name.required' => 'Last Name is required',
            'email.required' => 'Email is required',
            'email.email' => 'The email must be a valid email address.',
            'email.unique' => 'This Email has already been taken.',
            'password.required' => 'Password is required',
            'password.min' => 'The password should be more than 6 characters long.',
//            'father_name.required' => 'Father Name is required',
            'date_of_birth.required' => 'Date of birth is required',
            'gender_id.required' => 'Please select gender',
            'marital_status_id.required' => 'Please select marital status',
            'nationality_id.required' => 'Please select nationality',
            'national_id_card_number.required' => 'national ID card# required',
            'country_id.required' => 'Please select country',
            'state_id.required' => 'Please select state',
            'city_id.required' => 'Please select city',
            'phone.required' => 'Please enter phone',
            'mobile_num.required' => 'Please enter mobile number',
            'job_experience_id.required' => 'Please select experience',
            'career_level_id.required' => 'Please select career level',
            'industry_id.required' => 'Please select industry',
            'functional_area_id.required' => 'Please select functional area',
            'current_salary.required' => 'Please enter current salary',
            'expected_salary.required' => 'Please enter expected salary',
            'salary_currency.required' => 'Please select salary currency',
            'street_address.required' => 'Please enter street address',
            'image.image' => 'Only images can be uploaded.',*/
        ];
    }

}
