<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class MajorSubjectFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
            case 'POST': {
                    $id = (int) $this->input('id', 0);
                    $major_subject_unique = '';
                    if ($id > 0) {
                        $major_subject_unique = ',id,' . $id;
                    }
            //lang
            $lang = config('rules.lang_rule');
            //major_subject
            $major_subject = config('rules.alnum150_rule');
            //is_active
            $is_active = config('rules.bool_rule');
            //major_subject_id
            $major_subject_id = array();
            $is_default = (int) $this->input('is_default', 0);
            if ($is_default == 0) {
                $major_subject_id = config('rules.id_rule');
            }
            //is_default
            $is_default = config('rules.bool_rule');

                    $id = array();
                    return [
                        "id" => $id,
                        "lang" => $lang,
                        "major_subject" => $major_subject,
                        "is_default" => $is_default,
                        "major_subject_id" => $major_subject_id,
                        "is_active" => $is_active,

//                        "id" => "",
//                        "lang" => "required|max:10",
//                        "major_subject" => "required|max:200|unique:major_subjects$major_subject_unique",
//                        "is_default" => "required|boolean",
//                        "major_subject_id" => "required_if:is_default,0",
//                        "is_active" => "required|boolean",
                    ];
                }
            default:break;
        }
    }

    public function messages()
    {
        return [
//            'lang.required' => 'Please select language.', 'major_subject.required' => 'Major Subject required.', 'is_default.required' => 'Is this Major Subject default?', 'major_subject_id.required_if' => 'Please select default/fallback Major Subject.', 'is_active.required' => 'Is this Major Subject active?',
        ];
    }

}
