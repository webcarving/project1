<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProfileLanguageFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
            case 'POST': {
                    $id = (int) $this->input('id', 0);
                    $language_id = config('rules.id_rule');
                    $language_level_id = config('rules.id_rule');
                    return [
                        "language_id" => $language_id,
                        "language_level_id" => $language_level_id,

//                        "language_id" => "required|integer",
//                        "language_level_id" => "required|integer",
                    ];
                }
            default:break;
        }
    }

    public function messages()
    {
        return [
/*            'language_id.required' => 'Please select language.',
            'language_level_id.required' => 'Please select language level.',*/
        ];
    }

}
