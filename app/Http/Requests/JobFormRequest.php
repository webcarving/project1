<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;
use Illuminate\Support\Str;

class JobFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
            case 'POST': {
                    $id = (int) $this->input('id', 0);
                    $job_unique = '';
                    if ($id > 0) {
                        $job_unique = ',id,' . $id;
                    }
            //company_id
            $company_id = config('rules.id_rule');
            //title
            $title = config('rules.alnum150_rule');
            //description
            $description = config('rules.text_rule');
            //skills
            $skills = config('rules.array_rule');
            //skills_star
            $skills_star = config('rules.array_integer_star_rule');
            //benefits
            $benefits = config('rules.array_rule_optional');
            //benefits_star
            $benefits_star = config('rules.array_integer_star_rule');
            //salary_from
            $salary_from = config('rules.salary_rule');
            //salary_to
            $salary_to = config('rules.salary_rule');
            //salary_currency
            $salary_currency = config('rules.alnum3_rule');
            //salary_period_id
            $salary_period_id = config('rules.id_rule');
            //hide_salary
            $hide_salary = config('rules.id_rule');
            //career_level_id
            $career_level_id = config('rules.id_rule');
            //functional_area_id
            $functional_area_id = config('rules.id_rule');
            //job_type_id
            $job_type_id = config('rules.id_rule');
            //job_shift_id
            $job_shift_id = config('rules.id_rule');
            //num_of_positions
            $num_of_positions = config('rules.id_rule');
            //gender_id
            $gender_id = config('rules.id_rule');
            //expiry_date
            $expiry_date = config('rules.date_rule');
            //degree_level_id
            $degree_level_id = config('rules.id_rule');
            //job_experience_id
            $job_experience_id = config('rules.id_rule');
            //is_freelance
            $is_freelance = config('rules.bool_rule');
            //language
            $language = config('rules.array_rule');
            //language_star
            $language_star = config('rules.array_integer_star_rule');


            $locations_rules = array();
            foreach($this->input() as $key => $value){
                if(Str::startsWith($key, 'locations_') && $key != ''){
                    $locations_rules[$key] = config('rules.array_rule');
                    $locations_rules[$key.'.*'] = config('rules.array_integer_star_rule');
//        $locations_rules[] = array($key => config('rules.array_rule'));
                }
            };

//job_location_id
            $job_location_id = array();
            if(count($locations_rules) == 0){
                $job_location_id = config('rules.id_rule');
            }



            //is_active
            $is_active = config('rules.bool_rule');
            //is_featured
            $is_featured = config('rules.bool_rule');

                    $id = array();
                    return [
                        "id" => $id,
                        "company_id" => $company_id,
                        "title" => $title,
                        "description" => $description,
                        "skills" => $skills,
                        "skills.*" => $skills_star,
                        "benefits" => $benefits,
                        "benefits.*" => $benefits_star,
                        'salary_from' => $salary_from,
                        'salary_to' => $salary_to,
                        "salary_currency" => $salary_currency,
                        "salary_period_id" => $salary_period_id,
                        "hide_salary" => $hide_salary,
                        "career_level_id" => $career_level_id,
                        "functional_area_id" => $functional_area_id,
                        "job_type_id" => $job_type_id,
                        "job_shift_id" => $job_shift_id,
                        "num_of_positions" => $num_of_positions,
                        "gender_id" => $gender_id,
                        "expiry_date" => $expiry_date,
                        "degree_level_id" => $degree_level_id,
                        "job_experience_id" => $job_experience_id,
                        "is_freelance" => $is_freelance,
                        "language" => $language,
                        "language.*" => $language_star,
                        "job_location_id" => $job_location_id,
                        "is_active" => $is_active,
                        "is_featured" => $is_featured,


//                        "id" => "",
//                        "company_id" => "required|integer",
//                        "title" => "required|max:200",
//                        "description" => "required",
//                        "skills" => "required",
                        //"country_id" => "required",
                        //"state_id" => "required",
                        //"city_id" => "required",
//                        "is_freelance" => "required|boolean",
//                        "career_level_id" => "required|integer",
//                        'salary_from' => array(
//                            'required',
//                            'regex:/^(?:0|([1-9]\d{0,2}(,\d{3}){0,12}))$/m',
//                        ),
//                        'salary_to' => array(
//                            'required',
//                            'regex:/^(?:0|([1-9]\d{0,2}(,\d{3}){0,12}))$/m',
//                        ),
//                        "salary_currency" => "required|max:5",
//                        "salary_period_id" => "required|integer",
//                        "hide_salary" => "required|boolean",
//                        "functional_area_id" => "required|integer",
//                        "job_type_id" => "required|integer",
//                        "job_shift_id" => "required|integer",
//                        "num_of_positions" => "required|integer",
                        //"gender_id" => "required",
//                        "expiry_date" => 'required|date_format:"M j, Y"',
//                        "degree_level_id" => "required|integer",
//                        "job_experience_id" => "required|integer",
//                        "is_active" => "required|boolean",
//                        "is_featured" => "required|boolean",
                    ];
                }
            default:break;
        }
    }

    public function messages()
    {
        return [
/*            'company_id.required' => 'Please select Company.',
            'title.required' => 'Please enter Job title.',
            'description.required' => 'Please enter Job description.',
            'skills.required' => 'Please enter Job skills.',
            'country_id.required' => 'Please select Country.',
            'state_id.required' => 'Please select State.',
            'city_id.required' => 'Please select City.',
            'is_freelance.required' => 'Is this freelance Job?',
            'career_level_id.required' => 'Please select Career level.',
            'salary_from.required' => 'Please select salary from.',
            'salary_to.required' => 'Please select salary to.',
            'salary_currency.required' => 'Please select salary currency.',
            'salary_period_id.required' => 'Please select salary period.',
            'hide_salary.required' => 'Is salary hidden?',
            'functional_area_id.required' => 'Please select functional area.',
            'job_type_id.required' => 'Please select job type.',
            'job_shift_id.required' => 'Please select job shift.',
            'num_of_positions.required' => 'Please select number of positions.',
            'gender_id.required' => 'Please select gender.',
            'expiry_date.required' => 'Please enter Job expiry date.',
            'degree_level_id.required' => 'Please select degree level.',
            'job_experience_id.required' => 'Please select job experience.',
            'is_active.required' => 'Is this Job active?',
            'is_featured.required' => 'Is this Job featured?', */
        ];
    }

}
