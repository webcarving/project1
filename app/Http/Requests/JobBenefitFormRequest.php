<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class JobBenefitFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
            case 'POST': {
                    $id = (int) $this->input('id', 0);
                    $job_benefit_unique = '';
                    if ($id > 0) {
                        $job_benefit_unique = ',id,' . $id;
                    }

                    //job_benefit
                    $job_benefit = config('rules.alnum100_rule');
                    array_push($job_benefit,
                        'unique:job_benefits' . $job_benefit_unique
                    );
                    //job_benefit_id
                    $job_benefit_id = array();
                    $is_default = (int) $this->input('is_default', 0);
                    if ($is_default == 0) {
                        $job_benefit_id = config('rules.id_rule');
                    }
                    //is_active
                    $is_active = config('rules.bool_rule');
                    //is_default
                    $is_default = config('rules.bool_rule');
                    //lang
                    $lang = config('rules.lang_rule');


                    return [
                        'job_benefit' => $job_benefit,
                        'job_benefit_id' => $job_benefit_id,
                        'is_active' => $is_active,
                        'is_default' => $is_default,
                        'lang' => $lang,

//                        'job_benefit' => 'required|max:200|unique:job_benefits' . $job_benefit_unique,
//                        'job_benefit_id' => 'required_if:is_default,0',
//                        'is_active' => 'required|boolean',
//                        'is_default' => 'required|boolean',
//                        'lang' => 'required|max:10',
                    ];
                }
            default:break;
        }
    }

    public function messages()
    {
        return [
/*            'job_benefit.required' => 'Please enter Job Benefit.',
            'job_benefit_id.required_if' => 'Please select default/fallback Job Benefit.',
            'is_default.required' => 'Is this Job Benefit default?',
            'is_active.required' => 'Please select status.',
            'lang.required' => 'Please select language.',*/
        ];
    }

}
