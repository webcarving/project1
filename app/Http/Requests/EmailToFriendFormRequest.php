<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class EmailToFriendFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        //friend_name
        $friend_name = config('rules.name_rule');
        //friend_email
        $friend_email = config('rules.email_rule');
        //your_name
        $your_name = config('rules.name_rule');
        //your_email
        $your_email = config('rules.email_rule');
        //listing_url
        $job_url = config('rules.url_rule');
        //g_recaptcha_response
        $g_recaptcha_response = config('rules.captcha_rule');

        return [
            'friend_name' => $friend_name,
            'friend_email' => $friend_email,
            'your_name' => $your_name,
            'your_email' => $your_email,
            'job_url' => $job_url,
            'g-recaptcha-response' => $g_recaptcha_response,

//            'friend_name' => 'required|max:100',
//            'friend_email' => 'required|email|max:100',
//            'your_name' => 'required|max:100',
//            'your_email' => 'required|email|max:100',
//            'listing_url' => 'required|url',
//            'g-recaptcha-response' => 'required|captcha',
        ];
    }

    public function messages()
    {
        return [
/*            'friend_name.required' => 'Friend\'s Name is required',
            'friend_email.required' => 'Friend\'s E-mail address is required',
            'friend_email.email' => 'Friend\'s Valid e-mail address is required',
            'your_name.required' => 'Your name is required',
            'your_email.required' => 'Your email address is required',
            'your_email.email' => 'Your Valid e-mail address is required',
            'listing_url.required' => 'Listing url is required',
            'listing_url.url' => 'Listing url must be a valid URL',*/
        ];
    }

}
