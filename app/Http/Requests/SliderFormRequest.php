<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class SliderFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
            case 'POST': {
                    $id = (int) $this->input('id', 0);
                    $slider_unique = '';
                    //slider image
                    $slider_image = config('rules.alnum150_rule');
                    if ($id > 0) {
                        $slider_unique = ',id,' . $id;
						$slider_image = '';
                    }

            //slider_heading
            $slider_heading = config('rules.alnum100_rule');
            //slider_description
            $slider_description = config('rules.text_rule');
            //slider_link
            $slider_link = config('rules.alnum50_rule');
            //slider_link_text
            $slider_link_text = config('rules.alnum50_rule');
            //slider_id
            $slider_id = array();
            $is_default = (int) $this->input('is_default', 0);
            if ($is_default == 0) {
                $slider_id = config('rules.id_rule');
            }
            //is_active
            $is_active = config('rules.bool_rule');
            //is_default
            $is_default = config('rules.bool_rule');
            //lang
            $lang = config('rules.lang_rule');

                    $id = array();
                    return [
                        "id" => $id,
						"slider_heading" => $slider_heading,
						"slider_description" => $slider_description,
						"slider_image" => $slider_image,
						"slider_link" => $slider_link,
						"slider_link_text" => $slider_link_text,
						"is_default" => $is_default,
						"slider_id" => $slider_id,
						"is_active" => $is_active,
                        "lang" => $lang,


//                        "id" => "",
//						"lang" => "required|max:10",
//						"slider_heading" => "required|unique:sliders$slider_unique|max:250",
//						"slider_description" => "required",
//						"slider_image" => $slider_image,
//						"slider_link" => "required",
//						"slider_link_text" => "required|max:100",
//						"is_default" => "required|boolean",
//						"slider_id" => "required_if:is_default,0",
//						"is_active" => "required|boolean",
                    ];
                }
            default:break;
        }
    }

    public function messages()
    {
        return [
//            'lang.required' => 'Please select language.', 'slider.required' => 'Slider required.', 'is_default.required' => 'Is this Slider default?', 'slider_id.required_if' => 'Please select default/fallback Slider.', 'is_active.required' => 'Is this Slider active?',
        ];
    }

}
