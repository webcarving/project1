<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class GenderFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
            case 'POST': {
                    $id = (int) $this->input('id', 0);
                    $gender_unique = '';
                    if ($id > 0) {
                        $gender_unique = ',id,' . $id;
                    }

                    //gender
                    $gender = config('rules.alnum20_rule');
                    array_push($gender,
                        'unique:genders' . $gender_unique
                    );
                    //gender_id
                    $gender_id = array();
                    $is_default = (int) $this->input('is_default', 0);
                    if ($is_default == 0) {
                        $gender_id = config('rules.id_rule');
                    }
                    //is_active
                    $is_active = config('rules.bool_rule');
                    //is_default
                    $is_default = config('rules.bool_rule');
                    //lang
                    $lang = config('rules.lang_rule');

                    return [
                        'gender' => $gender,
                        'gender_id' => $gender_id,
                        'is_active' => $is_active,
                        'is_default' => $is_default,
                        'lang' => $lang,

//                        'gender' => 'required|max:30|unique:genders' . $gender_unique,
//                        'gender_id' => 'required_if:is_default,0',
//                        'is_active' => 'required|boolean',
//                        'is_default' => 'required|boolean',
//                        'lang' => 'required|max:10',
                    ];
                }
            default:break;
        }
    }

    public function messages()
    {
        return [
/*            'gender.required' => 'Please enter Gender.',
            'gender_id.required_if' => 'Please select default/fallback Gender.',
            'is_default.required' => 'Is this Gender default?',
            'is_active.required' => 'Please select status.',
            'lang.required' => 'Please select language.',*/
        ];
    }

}
