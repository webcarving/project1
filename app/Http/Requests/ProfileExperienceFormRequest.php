<?php

namespace App\Http\Requests;

use App\Http\Requests\Request;

class ProfileExperienceFormRequest extends Request
{

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        switch ($this->method()) {
            case 'PUT':
            case 'POST': {
                    $id = (int) $this->input('id', 0);

                    //title
                    $title = config('rules.alnum150_rule');
                    //company
                    $company = config('rules.alnum150_rule');
                    //career_levels_id
                    $career_levels_id = config('rules.id_rule');
                    //industry_id
                    $industry_id = config('rules.id_rule');
                    //functional_area_id
                    $functional_area_id = config('rules.id_rule');
                    //country_id
                    $country_id = config('rules.id_rule');
                    //state_id
                    $state_id = config('rules.id_rule');
                    //city_id
                    $city_id = config('rules.id_rule');
                    //date_start
                    $date_start = config('rules.date_rule');
                    //date end
                    $date_end = array();
                    $is_on_going = (int) $this->input('is_on_going', 0);
                    if($is_on_going == '0'){
                        $date_end = config('rules.date_end_rule');
                    }
                    //is_currently_working
                    $is_currently_working = config('rules.bool_rule');
                    //description
                    $description = config('rules.alnum200_rule');

                    return [
                        "title" => $title,
                        "company" => $company,
                        "career_levels_id" => $career_levels_id,
                        "industry_id" => $industry_id,
                        "functional_area_id" => $functional_area_id,
                        "country_id" => $country_id,
                        "state_id" => $state_id,
                        "city_id" => $city_id,
                        "date_start" => $date_start,
                        "date_end" => $date_end,
                        "is_currently_working" => $is_currently_working,
                        "description" => $description,

//                        "title" => "required|max:100",
//                        "company" => "required|max:120",
//                        "career_levels_id" => "required|integer",
//                        "industry_id" => "required|integer",
//                        "functional_area_id" => "required|integer",
//                        "country_id" => "required|integer",
//                        "state_id" => "required|integer",
//                        "city_id" => "required|integer",
//                        "date_start" => 'required|date_format:"M j, Y"',
//                        "is_currently_working" => "required|boolean",
//                        "description" => "required",
                    ];
                }
            default:break;
        }
    }

    public function messages()
    {
        return [
/*            'title.required' => 'Please enter title.',
            'company.required' => 'Please enter company.',
            'description.required' => 'Please enter description.',
            'country_id.required' => 'Please select country.',
            'state_id.required' => 'Please select state.',
            'city_id.required' => 'Please select city.',
            'date_start.required' => 'Please set start date.',
            'date_end.required_if' => 'Please set end date.',
            'is_currently_working.required' => 'Are you currently working here?',
            'description.required' => 'Please enter experience description.',
            'functional_area_id.required' => 'Please select Functional Area.',
            'industry_id.required' => 'Please select Industry.',
            'career_levels_id.required' => 'Please select Career Levels.',*/
        ];
    }

}
