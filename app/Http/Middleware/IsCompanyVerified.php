<?php

namespace App\Http\Middleware;

use Closure;
use Session;

class IsCompanyVerified
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(!auth('company')->user()->verified){
            Session::flush();
            Session::flash('_old_input.candidate_or_employer', 'employer');
            Session::flash('_old_input.email', auth('company')->user()->email);
            return redirect()->to('login')->withErrors(['email'=>'Please verify your email before login.']);
        }
        return $next($request);
    }
}
