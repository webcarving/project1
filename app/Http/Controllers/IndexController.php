<?php

namespace App\Http\Controllers;

use App;
use App\Seo;
use App\Job;
use App\Company;
use App\FunctionalArea;
use App\Country;
use App\Video;
use App\Testimonial;
use App\Slider;
use Illuminate\Http\Request;
use Redirect;
use App\Traits\CompanyTrait;
use App\Traits\FunctionalAreaTrait;
use App\Traits\CityTrait;
use App\Traits\JobTrait;
use App\Traits\Active;
use App\Helpers\DataArrayHelper;

class IndexController extends Controller
{

    use CompanyTrait;
    use FunctionalAreaTrait;
    use CityTrait;
    use JobTrait;
    use Active;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {  
        $ip=geoip()->getClientIP(); 
        $location=geoip($ip); 
        $country_code=$location->iso_code;
        $country_ids="".Country::countryID($country_code).""; 
        $country_id=Country::countryID($country_code);
        $topCompanyIds = $this->getCompanyIdsAndNumJobs(16,$country_id);
        $topFunctionalAreaIds = $this->getFunctionalAreaIdsAndNumJobs(32,$country_id);
        $topIndustryIds = $this->getIndustryIdsFromCompaniesnew(32,$country_id);
        $topCityIds = $this->getCityIdsAndNumJobs(32,$country_id);
       // dd($topIndustryIds);
        //$featuredJobs = Job::active()->featured()->notExpire()->Company($country_ids)->limit(12)->get();
        //$latestJobs = Job::active()->notExpire()->Company($country_ids)->orderBy('id', 'desc')->limit(12)->get();
        $video = Video::getVideo();
        $testimonials = Testimonial::langTestimonials();

        $functionalAreas = DataArrayHelper::langFunctionalAreasArray();
        $countries = DataArrayHelper::langCountriesArray();
		$sliders = Slider::langSliders();
        $time=date("Y-m-d H:i:s");
//         $featuredJobs=Job::join('manage_job_location','manage_job_location.country_id','=','jobs.country_id')
        $featuredJobs=Job::join('manage_job_location','manage_job_location.job_id','=','jobs.id')
        ->where('manage_job_location.country_id','=',$country_ids)
        ->where('jobs.is_active','=',1)
        ->where('jobs.is_featured','=',1)
        ->where('jobs.expiry_date','>',$time)
        ->limit(12)->get();
        if(count($featuredJobs) < 1){
            $featuredJobs=Job::join('manage_job_location','manage_job_location.job_id','=','jobs.id')
                ->where('jobs.is_active','=',1)
                ->where('jobs.is_featured','=',1)
                ->where('jobs.expiry_date','>',$time)
                ->limit(12)->get();
        }

        $latestJobs=Job::join('manage_job_location','manage_job_location.job_id','=','jobs.id')
        ->where('manage_job_location.country_id','=',$country_ids)
        ->where('jobs.is_active','=',1)
        ->where('jobs.expiry_date','>',$time)
        ->orderBy('jobs.id', 'desc')
        ->limit(12)->get();
        if(count($latestJobs) < 1){
            $latestJobs=Job::join('manage_job_location','manage_job_location.job_id','=','jobs.id')
                ->where('jobs.is_active','=',1)
                ->where('jobs.expiry_date','>',$time)
                ->orderBy('jobs.id', 'desc')
                ->limit(12)->get();
        }
        $seo = SEO::where('seo.page_title', 'like', 'front_index_page')->first();
        return view('welcome')
                        ->with('topCompanyIds', $topCompanyIds)
                        ->with('topFunctionalAreaIds', $topFunctionalAreaIds)
                        ->with('topCityIds', $topCityIds)
                        ->with('topIndustryIds', $topIndustryIds)
                        ->with('featuredJobs', $featuredJobs)
                        ->with('latestJobs', $latestJobs)
                        ->with('functionalAreas', $functionalAreas)
                        ->with('countries', $countries)
						->with('sliders', $sliders)
                        ->with('video', $video)
                        ->with('testimonials', $testimonials)
                        ->with('seo', $seo)
                        ->with('country_id',$country_id);
    }

    public function setLocale(Request $request)
    {
        $locale = $request->input('locale');
        $return_url = $request->input('return_url');
        $is_rtl = $request->input('is_rtl');
        $localeDir = ((bool) $is_rtl) ? 'rtl' : 'ltr';

        session(['locale' => $locale]);
        session(['localeDir' => $localeDir]);

        return Redirect::to($return_url);
    }

}
