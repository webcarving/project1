<?php

namespace App\Http\Controllers\Admin;

use Auth;
use DB;
use Input;
use File;
use Carbon\Carbon;
use ImgUploader;
use Redirect;
use App\JobBenefit;
use App\Language;
use App\Helpers\MiscHelper;
use App\Helpers\DataArrayHelper;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use DataTables;
use App\Http\Requests\JobBenefitFormRequest;
use App\Http\Controllers\Controller;

class JobBenefitController extends Controller
{

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //
    }

    public function indexJobBenefits()
    {
        $languages = DataArrayHelper::languagesNativeCodeArray();
        return view('admin.job_benefit.index')
            ->with('action', '')
            ->with('languages', $languages);
    }

    public function createJobBenefit()
    {
        $languages = DataArrayHelper::languagesNativeCodeArray();
        $jobBenefits = DataArrayHelper::defaultJobBenefitsArray();
        return view('admin.job_benefit.add')
                        ->with('languages', $languages)
                        ->with('action', 'Create')
                        ->with('jobBenefits', $jobBenefits);
    }

    public function storeJobBenefit(JobBenefitFormRequest $request)
    {
        $jobBenefit = new JobBenefit();
        $jobBenefit->job_benefit = $request->input('job_benefit');
        $jobBenefit->is_active = $request->input('is_active');
        $jobBenefit->lang = $request->input('lang');
        $jobBenefit->is_default = $request->input('is_default');
        $jobBenefit->save();
        /*         * ************************************ */
        $jobBenefit->sort_order = $jobBenefit->id;
        if ((int) $request->input('is_default') == 1) {
            $jobBenefit->job_benefit_id = $jobBenefit->id;
        } else {
            $jobBenefit->job_benefit_id = $request->input('job_benefit_id');
        }
        $jobBenefit->update();
        flash('Job Benefit has been added!')->success();
        return \Redirect::route('edit.job.benefit', array($jobBenefit->id));
    }

    public function editJobBenefit($id)
    {
        $languages = DataArrayHelper::languagesNativeCodeArray();
        $jobBenefits = DataArrayHelper::defaultJobBenefitsArray();
        $jobBenefit = JobBenefit::findOrFail($id);
        return view('admin.job_benefit.edit')
                        ->with('languages', $languages)
                        ->with('jobBenefit', $jobBenefit)
                        ->with('action', 'Update')
                        ->with('jobBenefits', $jobBenefits);
    }

    public function updateJobBenefit($id, JobBenefitFormRequest $request)
    {
        $jobBenefit = JobBenefit::findOrFail($id);
        $jobBenefit->job_benefit = $request->input('job_benefit');
        $jobBenefit->is_active = $request->input('is_active');
        $jobBenefit->lang = $request->input('lang');
        $jobBenefit->is_default = $request->input('is_default');
        if ((int) $request->input('is_default') == 1) {
            $jobBenefit->job_benefit_id = $jobBenefit->id;
        } else {
            $jobBenefit->job_benefit_id = $request->input('job_benefit_id');
        }
        $jobBenefit->update();
        flash('Job Benefit has been updated!')->success();
        return \Redirect::route('edit.job.benefit', array($jobBenefit->id));
    }

    public function deleteJobBenefit(Request $request)
    {
        $id = $request->input('id');
        try {
            $jobBenefit = JobBenefit::findOrFail($id);
            if ((bool) $jobBenefit->is_default) {
                JobBenefit::where('job_benefit_id', '=', $jobBenefit->job_benefit_id)->delete();
            } else {
                $jobBenefit->delete();
            }
            return 'ok';
        } catch (ModelNotFoundException $e) {
            return 'notok';
        }
    }

    public function fetchJobBenefitsData(Request $request)
    {
        $jobBenefits = JobBenefit::select(['job_benefits.id', 'job_benefits.job_benefit', 'job_benefits.is_active', 'job_benefits.lang', 'job_benefits.is_default', 'job_benefits.created_at', 'job_benefits.updated_at'])->sorted();
        return Datatables::of($jobBenefits)
                        ->filter(function ($query) use ($request) {
                            if ($request->has('job_benefit') && !empty($request->job_benefit)) {
                                $query->where('job_benefits.job_benefit', 'like', "%{$request->get('job_benefit')}%");
                            }
                            if ($request->has('lang') && !empty($request->get('lang'))) {
                                $query->where('job_benefits.lang', 'like', "%{$request->get('lang')}%");
                            }
                            if ($request->has('is_active') && $request->get('is_active') != -1) {
                                $query->where('job_benefits.is_active', '=', "{$request->get('is_active')}");
                            }
                        })
                        ->addColumn('job_benefit', function ($jobBenefits) {
                            $jobBenefit = str_limit($jobBenefits->job_benefit, 100, '...');
                            $direction = MiscHelper::getLangDirection($jobBenefits->lang);
                            return '<span dir="' . $direction . '">' . $jobBenefit . '</span>';
                        })
                        ->addColumn('action', function ($jobBenefits) {
                            /*                             * ************************* */
                            $activeTxt = 'Make Active';
                            $activeHref = 'makeActive(' . $jobBenefits->id . ');';
                            $activeIcon = 'square-o';
                            if ((int) $jobBenefits->is_active == 1) {
                                $activeTxt = 'Make InActive';
                                $activeHref = 'makeNotActive(' . $jobBenefits->id . ');';
                                $activeIcon = 'check-square-o';
                            }
                            return '
				<div class="btn-group">
					<button class="btn blue dropdown-toggle" data-toggle="dropdown" aria-expanded="false">Action
						<i class="fa fa-angle-down"></i>
					</button>
					<ul class="dropdown-menu">
						<li>
							<a href="' . route('edit.job.benefit', ['id' => $jobBenefits->id]) . '"><i class="fa fa-pencil" aria-hidden="true"></i>Edit</a>
						</li>						
						<li>
							<a href="javascript:void(0);" onclick="deleteJobBenefit(' . $jobBenefits->id . ', ' . $jobBenefits->is_default . ');" class=""><i class="fa fa-trash-o" aria-hidden="true"></i>Delete</a>
						</li>
						<li>
						<a href="javascript:void(0);" onClick="' . $activeHref . '" id="onclickActive' . $jobBenefits->id . '"><i class="fa fa-' . $activeIcon . '" aria-hidden="true"></i>' . $activeTxt . '</a>
						</li>																																		
					</ul>
				</div>';
                        })
                        ->rawColumns(['job_benefit', 'action'])
                        ->setRowId(function($jobBenefits) {
                            return 'jobBenefitDtRow' . $jobBenefits->id;
                        })
                        ->make(true);
        //$query = $dataTable->getQuery()->get();
        //return $query;
    }

    public function makeActiveJobBenefit(Request $request)
    {
        $id = $request->input('id');
        try {
            $jobBenefit = JobBenefit::findOrFail($id);
            $jobBenefit->is_active = 1;
            $jobBenefit->update();
            echo 'ok';
        } catch (ModelNotFoundException $e) {
            echo 'notok';
        }
    }

    public function makeNotActiveJobBenefit(Request $request)
    {
        $id = $request->input('id');
        try {
            $jobBenefit = JobBenefit::findOrFail($id);
            $jobBenefit->is_active = 0;
            $jobBenefit->update();
            echo 'ok';
        } catch (ModelNotFoundException $e) {
            echo 'notok';
        }
    }

    public function sortJobBenefits()
    {
        $languages = DataArrayHelper::languagesNativeCodeArray();
        return view('admin.job_benefit.sort')
            ->with('action', '')
            ->with('languages', $languages);
    }

    public function jobBenefitSortData(Request $request)
    {
        $lang = $request->input('lang');
        $jobBenefits = JobBenefit::select('job_benefits.id', 'job_benefits.job_benefit', 'job_benefits.sort_order')
                ->where('job_benefits.lang', 'like', $lang)
                ->orderBy('job_benefits.sort_order')
                ->get();
        $str = '<ul id="sortable">';
        if ($jobBenefits != null) {
            foreach ($jobBenefits as $jobBenefit) {
                $str .= '<li id="' . $jobBenefit->id . '"><i class="fa fa-sort"></i>' . $jobBenefit->job_benefit . '</li>';
            }
        }
        echo $str . '</ul>';
    }

    public function jobBenefitSortUpdate(Request $request)
    {
        $jobBenefitOrder = $request->input('jobBenefitOrder');
        $jobBenefitOrderArray = explode(',', $jobBenefitOrder);
        $count = 1;
        foreach ($jobBenefitOrderArray as $jobBenefitId) {
            $jobBenefit = JobBenefit::find($jobBenefitId);
            $jobBenefit->sort_order = $count;
            $jobBenefit->update();
            $count++;
        }
    }

}
