<?php

namespace App\Http\Controllers\Job;

use Auth;
use DB;
use Input;
use Redirect;
use Carbon\Carbon;
use App\Job;
use App\Industry;
use App\JobApply;
use App\FavouriteJob;
use App\Company;
use App\JobSkill;
use App\JobSkillManager;
use App\JobBenefit;
use App\JobBenefitManager;
use App\Country;
use App\CountryDetail;
use App\State;
use App\City;
use App\CareerLevel;
use App\FunctionalArea;
use App\JobType;
use App\JobShift;
use App\Gender;
use App\JobExperience;
use App\DegreeLevel;
use App\ProfileCv;
use App\Helpers\MiscHelper;
use App\Helpers\DataArrayHelper;
use App\Http\Requests;
use Illuminate\Http\Request;
use Illuminate\Database\Eloquent\ModelNotFoundException;
use DataTables;
use App\Http\Requests\JobFormRequest;
use App\Http\Requests\Front\ApplyJobFormRequest;
use App\Http\Controllers\Controller;
use App\Traits\FetchJobs;
use App\Events\JobApplied;
use Illuminate\Support\Facades\Validator;
use Torann\GeoIP\Facades\GeoIP;

class Careerjet_API extends Controller
{
    public function search($args)
  {
    $result =  $this->call('search' , $args);
    if ($result->type == 'ERROR') {
      trigger_error( $result->error );
    }
    return $result;
  }
  public  function call($fname , $args)
  {
    $url = 'http://public.api.careerjet.net/'.$fname.'?sort=date&locale_code='.$this->locale;
echo 'http://public.api.careerjet.net/'.$fname.'?sort=date&locale_code='.$this->locale;
    if (empty($args['affid'])) {
      return (object) array(
        'type' => 'ERROR',
        'error' => "Your Careerjet affiliate ID needs to be supplied. If you don't " .
                   "have one, open a free Careerjet partner account."
      );
    }

    foreach ($args as $key => $value) {
      $url .= '&'. $key . '='. urlencode($value);
    }

    if (empty($_SERVER['REMOTE_ADDR'])) {
      return (object) array(
        'type' => 'ERROR',
        'error' => 'not running within a http server'
      );
    }

    if (isset($_SERVER["HTTP_CF_CONNECTING_IP"])) {
      $ip = $_SERVER["HTTP_CF_CONNECTING_IP"];
    } elseif (isset($_SERVER['HTTP_CLIENT_IP'])) {
      $ip = $_SERVER['HTTP_CLIENT_IP'];
    } elseif (isset($_SERVER['HTTP_X_FORWARDED_FOR'])) {
      // For more info: http://en.wikipedia.org/wiki/X-Forwarded-For
      $ip = trim(array_shift(array_values(explode(',', $_SERVER['HTTP_X_FORWARDED_FOR']))));
    } else {
      $ip = $_SERVER['REMOTE_ADDR'];
    }

    $url .= '&user_ip=' . $ip;
    $url .= '&user_agent=' . urlencode($_SERVER['HTTP_USER_AGENT']);
    
    // determine current page
    $current_page_url = '';
    if (!empty ($_SERVER["SERVER_NAME"]) && !empty ($_SERVER["REQUEST_URI"])) {
      $current_page_url = 'http';
      if (!empty ($_SERVER["HTTPS"]) && $_SERVER["HTTPS"] == "on") {
        $current_page_url .= "s";
      }
      $current_page_url .= "://";
  
      if (!empty ($_SERVER["SERVER_PORT"]) && $_SERVER["SERVER_PORT"] != "80") {
        $current_page_url .= $_SERVER["SERVER_NAME"].":".$_SERVER["SERVER_PORT"].$_SERVER["REQUEST_URI"];
      } else {
        $current_page_url .= $_SERVER["SERVER_NAME"].$_SERVER["REQUEST_URI"];
      }
    }

    $header = "User-Agent: careerjet-api-client-v" . $this->version . "-php-v" . phpversion();
    if ($current_page_url) {
      $header .= "\nReferer: " . $current_page_url;
    }

    $careerjet_api_context = stream_context_create(array(
      'http' => array('header' => $header)
    ));

    $response = file_get_contents($url, false, $careerjet_api_context);
    return json_decode($response);
  }
}
class JobController extends Controller
{

    //use Skills;
    use FetchJobs;

    private $functionalAreas = '';
    private $countries = '';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth', ['except' => ['jobsBySearch', 'jobDetail']]);

        $this->functionalAreas = DataArrayHelper::langFunctionalAreasArray();
        $this->countries = DataArrayHelper::langCountriesArray();
    }

    public function jobsBySearch(Request $request)
    {
        if($request['salary_from']!== NULL){
            $rules = array(
                'salary_from' => array(
                    'required',
                    'regex:/^(?:0|([1-9]\d{0,2}(,\d{3}){0,12}))$/m',
                ),
            );
            $validation = Validator::make($request->all(), $rules);
            if ($validation->fails()) {
                return redirect(route('job.list'))->withInput()->withErrors($validation);
            }
            $request['salary_from'] = str_replace(',','',$request->input('salary_from'));
        }
        if($request['salary_to']!== NULL) {
            $rules = array(
                'salary_to' => array(
                    'required',
                    'regex:/^(?:0|([1-9]\d{0,2}(,\d{3}){0,12}))$/m',
                ),
            );
            $validation = Validator::make($request->all(), $rules);
            if ($validation->fails()) {
                return redirect(route('job.list'))->withInput()->withErrors($validation);
            }
            $request['salary_to'] = str_replace(',', '', $request->input('salary_to'));
        }

        $country_ids = $request->query('country_id', array());
        $ip=geoip()->getClientIP(); 
        $location=geoip($ip);
        if(empty($country_ids)){  
          $country_code=$location->iso_code;
          $country_ids[0]="".Country::countryID($country_code).""; 
          $country_id=Country::countryID($country_code);
        }else{
          $country_id=null; 
        } 
        $currency_defoult=$location->currency;
        $search = $request->query('search', ''); 
        $job_titles = $request->query('job_title', array());
        $company_ids = $request->query('company_id', array());
        $industry_ids = $request->query('industry_id', array());
        $job_skill_ids = $request->query('job_skill_id', array());
        $job_benefit_ids = $request->query('job_benefit_id', array());
        $functional_area_ids = $request->query('functional_area_id', array());
        $country_ids = $request->query('country_id', array());
        $state_ids = $request->query('state_id', array());
        $city_ids = $request->query('city_id', array());
        $is_freelance = $request->query('is_freelance', array());
        $career_level_ids = $request->query('career_level_id', array());
        $job_type_ids = $request->query('job_type_id', array());
        $job_shift_ids = $request->query('job_shift_id', array());
        $gender_ids = $request->query('gender_id', array());
        $degree_level_ids = $request->query('degree_level_id', array());
        $job_experience_ids = $request->query('job_experience_id', array());
        $salary_from = $request->query('salary_from', '');
        $salary_to = $request->query('salary_to', '');
        $salary_currency = $request->query('salary_currency', '');
        $is_featured = $request->query('is_featured', 2);
        $order_by = $request->query('order_by', 'id');
        $limit = 10;


        $careerjet=$this->CareerJet($request);
//        $careerjet = array("data"=>array());
        $aligator=$this->aligator($request);
//        $aligator=array("total"=>'','maxcount'=>'','data'=>array());
        $careerbuilder=$this->CareerBuilder($request);
//        $careerbuilder=array("data"=>array());
        $monster=$this->Monster($request);
//        $monster=array("data"=>array());
        $jobs = $this->fetchJobs($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $job_benefit_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, $order_by, $limit);
        /*         * ************************************************** */
        $jobTitlesArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $job_benefit_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.title');

        /*         * ************************************************* */

        $jobIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $job_benefit_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.id');

        /*         * ************************************************** */

        $skillIdsArray = $this->fetchSkillIdsArray($jobIdsArray);

        /*         * ************************************************** */

        $countryIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $job_benefit_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'manage_job_location.country_id');

        /*         * ************************************************** */

        $stateIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $job_benefit_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'manage_job_location.state_id');

        /*         * ************************************************** */

        $cityIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $job_benefit_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'manage_job_location.city_id');

        /*         * ************************************************** */

        $companyIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $job_benefit_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.company_id');

        /*         * ************************************************** */

        $industryIdsArray = $this->fetchIndustryIdsArray($companyIdsArray);

        /*         * ************************************************** */


        /*         * ************************************************** */

        $functionalAreaIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $job_benefit_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.functional_area_id');

        /*         * ************************************************** */

        $careerLevelIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $job_benefit_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.career_level_id');

        /*         * ************************************************** */

        $jobTypeIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $job_benefit_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.job_type_id');

        /*         * ************************************************** */

        $jobShiftIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $job_benefit_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.job_shift_id');

        /*         * ************************************************** */

        $genderIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $job_benefit_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.gender_id');

        /*         * ************************************************** */

        $degreeLevelIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $job_benefit_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.degree_level_id');

        /*         * ************************************************** */

        $jobExperienceIdsArray = $this->fetchIdsArray($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $job_benefit_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, 'jobs.job_experience_id');

        /*         * ************************************************** */

        $seoArray = $this->getSEO($functional_area_ids, $country_ids, $state_ids, $city_ids, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids);

        /*         * ************************************************** */

        $currencies = DataArrayHelper::currenciesArray();

        /*         * ************************************************** */

        $seo = (object) array(
                    'seo_title' => $seoArray['title'],
                    'seo_description' => $seoArray['description'],
                    'seo_keywords' => $seoArray['keywords'],
                    'seo_other' => ''
        );
        return view('job.list')
                        ->with('functionalAreas', $this->functionalAreas)
                        ->with('countries', $this->countries)
                        ->with('currencies', array_unique($currencies))
                        ->with('jobs', $jobs)
                        ->with('jobTitlesArray', $jobTitlesArray)
                        ->with('skillIdsArray', $skillIdsArray)
                        ->with('countryIdsArray', $countryIdsArray)
                        ->with('stateIdsArray', $stateIdsArray)
                        ->with('cityIdsArray', $cityIdsArray)
                        ->with('companyIdsArray', $companyIdsArray)
                        ->with('industryIdsArray', $industryIdsArray)
                        ->with('functionalAreaIdsArray', $functionalAreaIdsArray)
                        ->with('careerLevelIdsArray', $careerLevelIdsArray)
                        ->with('jobTypeIdsArray', $jobTypeIdsArray)
                        ->with('jobShiftIdsArray', $jobShiftIdsArray)
                        ->with('genderIdsArray', $genderIdsArray)
                        ->with('degreeLevelIdsArray', $degreeLevelIdsArray)
                        ->with('jobExperienceIdsArray', $jobExperienceIdsArray)
                        ->with('aligator', $aligator)
                        ->with('careerjet', $careerjet)
                        ->with('careerbuilder', $careerbuilder)
                        ->with('monster',$monster)
                        ->with('currency_defoult',$currency_defoult)
                        ->with('country_id',$country_id)
                        ->with('seo', $seo);
    }
    public function queryinapiinded($request='')
    {
        $q='';
        $l='';
        $jt='';
        $ccode='';
        $scode='';
        $country='';
        $state='';
        $city='';
        $q.= ($request->query('search', ''))?$request->query('search', ''):'';
        $q.=($request->query('job_title', array()))?$this->find_job_title($request->query('job_title', array())):'';
        $q.=($request->query('industry_id', array()))?$this->find_job_industry($request->query('industry_id', array())):'';
        $q.=($request->query('job_skill_id', array()))?$this->find_job_skill($request->query('job_skill_id', array())):'';
         $q.=($request->query('functional_area_id', array()))?$this->find_job_functional_area($request->query('functional_area_id', array())):'';
         $l.=($request->query('country_id', array()))?$this->find_job_country($request->query('country_id', array())):'';
          $l.=($request->query('state_id', array()))?$this->find_job_state($request->query('state_id', array())):'';
           $l.=($request->query('city_id', array()))?$this->find_job_city($request->query('city_id', array())):'';
          
          $jt.=($request->query('job_type_id', array()))?$this->find_job_job_type($request->query('job_type_id', array())):'';

           $ccode.=($request->query('country_id', array()))?$this->find_job_countrycode($request->query('country_id', array())):'';
           $scode.=($request->query('state_id', array()))?$this->find_job_statecode($request->query('state_id', array())):'';
       
        $country.=($request->query('country_id', array()))?$this->find_job_countrysingle($request->query('country_id', array())):'';
          $state.=($request->query('state_id', array()))?$this->find_job_statesingle($request->query('state_id', array())):'';
           $city.=($request->query('city_id', array()))?$this->find_job_citysingle($request->query('city_id', array())):''; 
       /* $job_shift_ids = $request->query('job_shift_id', array());
        $gender_ids = $request->query('gender_id', array());
        $degree_level_ids = $request->query('degree_level_id', array());
        $job_experience_ids = $request->query('job_experience_id', array());
        $salary_from = $request->query('salary_from', '');
        $salary_to = $request->query('salary_to', '');
        $salary_currency = $request->query('salary_currency', '');
        $is_featured = $request->query('is_featured', 2);
        $order_by = $request->query('order_by', 'id');
        $limit = 10;*/
        $q = ltrim($q);
        $l = ltrim($l);
        $jt = ltrim($jt);
        $ccode = ltrim($ccode);
        $scode = ltrim($scode);
        $country = ltrim($country);
        $state = ltrim($state);
        $city = ltrim($city); 
$page=$request->query('page', '1');
        if($q==""){
            $q='all';
        }
         $data = array('q' => $q, 'l' => $l , 'jt' => $jt,'ccode' => $ccode, 'scode' => $scode , 'country' => $country, 'state' => $state , 'city' => $city , 'page' => $page); 
         return $data;
    }
    public function find_job_title($value='')
    {
        $title='';
       foreach ($value as $key => $val) {
          $title.=' '.$val;
          return $title;
       }
       
    }
    public function find_job_industry($value='')
    {
        $name='';
       foreach ($value as $key => $val) {
          $name.=' '.Industry::getindustryName($val); 
           return $name;
       }
      
    }
    public function find_job_skill($value='')
    {
         $name='';
       foreach ($value as $key => $val) {
          $name.=' '.JobSkill::getjobskillname($val); 
           return $name;
       }
      
    }
    public function find_job_functional_area($value='')
    {
           $name='';
       foreach ($value as $key => $val) {
          $name.=' '.FunctionalArea::getjobfunctional_areasname($val); 
          return $name;
       }
       
    }
    public function find_job_countrycode($value='')
    {
         $name='';
       foreach ($value as $key => $val) {
          $name.=' '.Country::getjobcountrycode($val);
       return $name;
       }
    }
    public function find_job_statecode($value='')
    {
        $name='';
       foreach ($value as $key => $val) {
          $name.=' '.State::getjobstatecode($val);  
       return $name;
       }
    }
    public function find_job_country($value='')
    {
         $name='';
       foreach ($value as $key => $val) {
          $name.=' '.Country::getjobcountryname($val); 
          return $name;
       }
       
    }
    public function find_job_state($value='')
    {
        $name='';
       foreach ($value as $key => $val) {
          $name.=' '.State::getjobstatename($val); 
           return $name;
       }
      
    }
    public function find_job_city($value='')
    {
        $name='';
       foreach ($value as $key => $val) {
          $name.=' '.City::getjobcityname($val); 
           return $name;
       }
      
    }
    public function find_job_countrysingle($value='')
    {
        $name='';
       foreach ($value as $key => $val) {
          $name.=' '.Country::getjobcountryname($val);
       return $name; 
       }
    }
    public function find_job_statesingle($value='')
    {
        $name='';
       foreach ($value as $key => $val) {
          $name.=' '.State::getjobstatename($val); 
       return $name;
       }
    }
    public function find_job_citysingle($value='')
    {
        $name='';
       foreach ($value as $key => $val) {
          $name.=' '.City::getjobcityname($val);
       return $name; 
       }
    }
    public function find_job_job_type($value='')
    {
        $name='';
       foreach ($value as $key => $val) {
          $name.=' '.JobType::gettypeName($val); 
          return $name;
       } 
    }
    public function aligator($requert='')
    {
        $data=$this->queryinapiinded($requert);  //inded 
        //dd($data);
        $q=$data['q']; 
        $l=$data['scode'].($data['scode']!='')?'':''.$data['ccode'];
        $start=(($data['page']-1)*5)+1;
        $cond=array(); 
         if($data['q']!=''){
            $cond['q']=$data['q'];
         }
         if($data['ccode']!=''){
            $cond['co']=$data['ccode'];
         }
         
         if($data['l']!=''){
            $cond['l']=$data['l'];
         } 
         if($data['page']!='all'){
             $start=(($data['page']-1)*5)+1;
             $cond['start']=$start;
         }
         $con['limit']=5;
        $str =''; 
        foreach ($cond as $key => $value) {
           $str.="&".$key."=".$value;
        }
        // $data = array('q' => $q, 'l' => $l , 'jt' => $jt,'ccode' => $ccode, 'scode' => $scode , 'country' => $country, 'state' => $state , 'city' => $city); 
        // 'http://api.indeed.com/ads/apisearch?sort=date&publisher=17083005191573&q=all&l=&limit=5&start='.$start
        $items = $this->get_live_feeds('http://api.indeed.com/ads/apisearch?sort=date&publisher=17083005191573&limit=5'.$str);
        return $items;
    }
    public function CareerJet($requert='')
    { 
         $data=$this->queryinapiinded($requert);
         $cond=array(); 
         if($data['q']!='' and $data['q']!='all'){
            $cond['s']=$data['q'];
         }
         if($data['l']!=''){
            $cond['l']=$data['l'];
         } 
         if($data['page']!='all'){
             $start=(($data['page']-1)*5)+1;
             $cond['b']=$start;
         }
        $str =''; 
        foreach ($cond as $key => $value) {
           $str.="&".$key."=".$value;
        }
        // dd($data);
         // dd('http://rss.careerjet.com/rss?affid=cd5789b56c9b3bc703d7fcc870d50fc6&'.$str);

         $items = $this->get_live_feeds2('http://rss.careerjet.com/rss?sort=date&affid=cd5789b56c9b3bc703d7fcc870d50fc6'.$str);

         return $items;
    }
    public function CareerBuilder($requert='')
    {  
         $data=$this->queryinapiinded($requert);
         $str =''; 
        $str.="&city=".$data['city'];
        $str.="&state=".$data['state'];
        $str.="&country=".$data['ccode'];
        $str.="&kw=".$data['q'];
        if($data['page']!='all'){
            $str.="&page=".$data['page'];
        }
         $items = $this->get_live_feeds3(url('').'/api/parse-careerbuilder.php?limit=5',$str); 
         return $items;
    }
    public function Monster($requert='')
    {
        $data=$this->queryinapiinded($requert);
        $str =''; 
        $str.="&keyword=".$data['q'];
        $str.="&loc=".$data['l'];
        if($data['page']!='all'){
            $str.="&limit=".$data['page']*5;
        }else{
            $str.="&limit=5";
        }
        $items = $this->get_live_feeds3(url('').'/api/parse-monster.php?',$data); 
        return $items;
    }
  
     public function get_live_feeds($url='')
    {
        $has_curl = function_exists('curl_init');
        $has_iconv = function_exists('iconv');
        $content=$this->getDatanew($url);
        if ( $has_iconv ) {
            $content = iconv("UTF-8", "UTF-8//TRANSLIT", $content);
        }
        if ( strpos($content, '<?xml') !== false ){
            $content = substr($content, strpos($content, '<response'));
        }

        $feed = @simplexml_load_string($content);

        if ( isset($feed->results) && isset($feed->results->result) ){
            $count = count($feed->results->result);
        } else {
            $count = 0;
        }
        $items=[];
        if ( $count ){
            foreach ($feed->results->result as $item){
                $info = array();
                $info['title'] = isset($item->jobtitle) ? (string)$item->jobtitle : '';
                $info['company'] = isset($item->company) ? (string)$item->company : '';
                $info['city'] = isset($item->city) ? (string)$item->city : '';
                $info['state'] = isset($item->state) ? (string)$item->state : '';
                $info['country'] = isset($item->country) ? (string)$item->country : '';
                $info['formattedLocation'] = isset($item->formattedLocation) ? (string)$item->formattedLocation : '';
                $info['source'] = isset($item->source) ? (string)$item->source : '';
                $info['date'] = isset($item->date) ? (string)$item->date : '';
                $info['snippet'] = isset($item->snippet) ? (string)$item->snippet : '';
                $info['url'] = isset($item->url) ? (string)$item->url : '';
                $info['onmousedown'] = isset($item->onmousedown) ? (string)$item->onmousedown : '';
                $info['jobkey'] = isset($item->jobkey) ? (string)$item->jobkey : '';

                $items[] = $info;
            }
        }
        $data = array('total' => ($feed === false ? '' : ((string)$feed->totalresults)) ,
            'maxcount' => ($feed === false ? '' : ((string)$feed->end)) , 'data' => $items);
        return ($data); 
    }
     public function get_live_feeds2($url='')
    { 
        $has_curl = function_exists('curl_init');
        $has_iconv = function_exists('iconv');
        $content=$this->getData($url);
        if ( $has_iconv ) {
            $content = iconv("UTF-8", "UTF-8//TRANSLIT", $content);
        }
        if ( strpos($content, '<?xml') !== false ){
            $content = substr($content, strpos($content, '<response'));
        }

        $feed = @simplexml_load_string($content);  
        if ( isset($feed->channel) && isset($feed->channel->item) ){
            $count = count($feed->channel->item); 
        } else {
            $count = 0; 
        } 
        $items = array();
        if ( $count ){
            $i=0;
            foreach ($feed->channel->item as $item){
                $info = array();
                $info['title'] = isset($item->title) ? (string)$item->title : ''; 
                $info['date'] = isset($item->pubDate) ? (string)$item->pubDate : '';
                $info['snippet'] = isset($item->description) ? (string)$item->description : '';
                $info['url'] = isset($item->link) ? (string)$item->link : ''; 
                $items[] = $info; 
                $i++;
                if($i==5){
                    break;
                }
            }
        }  
        $data = array('data' => $items);
        return ($data); 
    }
     public function get_live_feeds3($url='' ,$data=array())
    { 

        $has_curl = function_exists('curl_init');
        $has_iconv = function_exists('iconv');
        $content=$this->getData($url);
        $items=json_decode($content); 
        $data = array('data' => $items);
        return ($data); 
    }
    public function getData($url='')
    { 
        $curl = curl_init();
        $header[0] = "Accept: text/xml,application/xml,application/xhtml+xml,";
        $header[0] .= "text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
        $header[] = "Cache-Control: max-age=0";
        $header[] = "Connection: keep-alive";
        $header[] = "Keep-Alive: 300";
        $header[] = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
        $header[] = "Accept-Language: en-us,en;q=0.5";
        $header[] = "Pragma: "; // browsers keep this blank.

        curl_setopt($curl, CURLOPT_URL, $url);
        curl_setopt($curl, CURLOPT_USERAGENT, 'Mozilla/5.0 compatible RSS Fetcher');
        curl_setopt($curl, CURLOPT_HTTPHEADER, $header);
        curl_setopt($curl, CURLOPT_REFERER, '-');
        curl_setopt($curl, CURLOPT_ENCODING, 'gzip,deflate');
        curl_setopt($curl, CURLOPT_AUTOREFERER, true);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 120);
        curl_setopt($curl, CURLOPT_TIMEOUT, 120);
        curl_setopt($curl, CURLOPT_MAXREDIRS, 5);

        $html = curl_exec($curl); // execute the curl command
        curl_close($curl); // close the connection
        return $html; // and finally, return $html
    }
    public function getDatanew($url='')
    {

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, 'http://api.indeed.com/ads/apisearch?sort=date&publisher=17083005191573&limit=5&q=Business%20Systems%20Analyst&start=1');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');

        curl_setopt($ch, CURLOPT_ENCODING, 'gzip, deflate');

        $headers = array();
        $headers[] = 'Connection: keep-alive';
        $headers[] = 'Pragma: no-cache';
        $headers[] = 'Cache-Control: no-cache';
        $headers[] = 'Upgrade-Insecure-Requests: 1';
        $headers[] = 'User-Agent: Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/76.0.3809.132 Safari/537.36';
        $headers[] = 'Accept: text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3';
        $headers[] = 'Accept-Encoding: gzip, deflate';
        $headers[] = 'Accept-Language: en-US,en;q=0.9,id;q=0.8';
        $headers[] = 'Cookie: CTK=1dkaaqgi7f18g800; jasx_pool_id=515637';
        curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);

        $result = curl_exec($ch);
        if (curl_errno($ch)) {
            echo 'Error:' . curl_error($ch);
        }

        return $result; // and finally, return $html
    }
    public function jobDetail(Request $request, $job_slug)
    {
        $job = Job::where('slug', 'like', $job_slug)->firstOrFail();
        /*         * ************************************************** */
        $search = '';
        $job_titles = array();
        $company_ids = array();
        $industry_ids = array();
        $job_skill_ids = (array) $job->getJobSkillsArray();
        $job_benefit_ids = (array) $job->getJobBenefitsArray();
        $functional_area_ids = (array) $job->getFunctionalArea('functional_area_id');
        $country_ids = (array) $job->getCountry('country_id');
        $state_ids = (array) $job->getState('state_id');
        $city_ids = (array) $job->getCity('city_id');
        $is_freelance = $job->is_freelance;
        $career_level_ids = (array) $job->getCareerLevel('career_level_id');
        $job_type_ids = (array) $job->getJobType('job_type_id');
        $job_shift_ids = (array) $job->getJobShift('job_shift_id');
        $gender_ids = (array) $job->getGender('gender_id');
        $degree_level_ids = (array) $job->getDegreeLevel('degree_level_id');
        $job_experience_ids = (array) $job->getJobExperience('job_experience_id');
        $salary_from = 0;
        $salary_to = 0;
        $salary_currency = '';
        $is_featured = 2;
        $order_by = 'id';
        $limit = 5;

        $relatedJobs = $this->fetchJobs($search, $job_titles, $company_ids, $industry_ids, $job_skill_ids, $job_benefit_ids, $functional_area_ids, $country_ids, $state_ids, $city_ids, $is_freelance, $career_level_ids, $job_type_ids, $job_shift_ids, $gender_ids, $degree_level_ids, $job_experience_ids, $salary_from, $salary_to, $salary_currency, $is_featured, $order_by, $limit);
        /*         * ***************************************** */

        $seoArray = $this->getSEO((array) $job->functional_area_id, (array) $job->country_id, (array) $job->state_id, (array) $job->city_id, (array) $job->career_level_id, (array) $job->job_type_id, (array) $job->job_shift_id, (array) $job->gender_id, (array) $job->degree_level_id, (array) $job->job_experience_id);
        /*         * ************************************************** */
        $seo = (object) array(
                    'seo_title' => $seoArray['description'],
                    'seo_description' => $seoArray['description'],
                    'seo_keywords' => $seoArray['keywords'],
                    'seo_other' => ''
        );
        return view('job.detail')
                        ->with('job', $job)
                        ->with('relatedJobs', $relatedJobs)
                        ->with('seo', $seo);
    }

    /*     * ************************************************** */

    public function addToFavouriteJob(Request $request, $job_slug)
    {
        $data['job_slug'] = $job_slug;
        $data['user_id'] = Auth::user()->id;
        $data_save = FavouriteJob::create($data);
        flash(__('Job has been added in favorites list'))->success();
        return \Redirect::route('job.detail', $job_slug);
    }

    public function removeFromFavouriteJob(Request $request, $job_slug)
    {
        $user_id = Auth::user()->id;
        FavouriteJob::where('job_slug', 'like', $job_slug)->where('user_id', $user_id)->delete();

        flash(__('Job has been removed from favorites list'))->success();
        return \Redirect::route('job.detail', $job_slug);
    }

    public function applyJob(Request $request, $job_slug)
    {
        $currencies = DataArrayHelper::currenciesArray();

        $user = Auth::user();
        $job = Job::where('slug', 'like', $job_slug)->first();
        
        if ((bool)$user->is_active === false) {
            flash(__('Your account is inactive contact site admin to activate it'))->error();
            return \Redirect::route('job.detail', $job_slug);
            exit;
        }
        
        if ((bool) config('jobseeker.is_jobseeker_package_active')) {
            if (
                    ($user->jobs_quota <= $user->availed_jobs_quota) ||
                    ($user->package_end_date->lt(Carbon::now()))
            ) {
                flash(__('Please subscribe to package first'))->error();
                return \Redirect::route('home');
                exit;
            }
        }
        if ($user->isAppliedOnJob($job->id)) {
            flash(__('You have already applied for this job'))->success();
            return \Redirect::route('job.detail', $job_slug);
            exit;
        }
        
        

        $myCvs = ProfileCv::where([
            ['user_id', '=', $user->id],
            ['title', '<>', '']
        ])->pluck('title', 'id')->toArray();

        return view('job.apply_job_form')
                        ->with('job_slug', $job_slug)
                        ->with('job', $job)
                        ->with('myCvs', $myCvs)
                        ->with('currencies',$currencies)
                        ->with('user', $user);
    }

    public function postApplyJob(ApplyJobFormRequest $request, $job_slug)
    {
        $request['current_salary'] = str_replace(',','',$request->input('current_salary'));
        $request['expected_salary'] = str_replace(',','',$request->input('expected_salary'));

        $user = Auth::user();
        $user_id = $user->id;
        $job = Job::where('slug', 'like', $job_slug)->first();

        $jobApply = new JobApply();
        $jobApply->user_id = $user_id;
        $jobApply->job_id = $job->id;
        $jobApply->cv_id = $request->post('cv_id');
        $jobApply->current_salary = $request->post('current_salary');
        $jobApply->expected_salary = $request->post('expected_salary');
        $jobApply->salary_currency = $request->post('salary_currency');
        $jobApply->save();

        /*         * ******************************* */
        if ((bool) config('jobseeker.is_jobseeker_package_active')) {
            $user->availed_jobs_quota = $user->availed_jobs_quota + 1;
            $user->update();
        }
        /*         * ******************************* */
        event(new JobApplied($job, $jobApply));

        flash(__('You have successfully applied for this job'))->success();
        return \Redirect::route('job.detail', $job_slug);
    }

    public function myJobApplications(Request $request)
    {
        $myAppliedJobIds = Auth::user()->getAppliedJobIdsArray();
        $jobs = Job::whereIn('id', $myAppliedJobIds)->paginate(10);
        return view('job.my_applied_jobs')
                        ->with('jobs', $jobs)
                         ->with('active', 'myJobApplications');
    }

    public function myFavouriteJobs(Request $request)
    {
        $myFavouriteJobSlugs = Auth::user()->getFavouriteJobSlugsArray();
        $jobs = Job::whereIn('slug', $myFavouriteJobSlugs)->paginate(10);
        return view('job.my_favourite_jobs')
                        ->with('jobs', $jobs)
                        ->with('active', 'myFavouriteJobs');
    }

}
