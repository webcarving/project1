<?php

namespace App;

use App;
use App\Traits\Lang;
use App\Traits\IsDefault;
use App\Traits\Active;
use App\Traits\Sorted;
use Illuminate\Database\Eloquent\Model;

class Country extends Model
{

    use Lang;
    use IsDefault;
    use Active;
    use Sorted;

    protected $table = 'countries';
    public $timestamps = true;
    protected $guarded = ['id'];
    //protected $dateFormat = 'U';
    protected $dates = ['created_at', 'updated_at'];

    public function states()
    {
        return $this->hasMany('App\State', 'country_id', 'id');
    }
    public static function countryID($id)
    {
        $country = self::where('countries.code', '=', $id)->lang()->active()->first();
        if (null === $country) {
            $country = self::where('countries.code', '=', $id)->active()->first();
        }  
         return (isset($country->country_id))?$country->country_id:0;
    }
    public static function getjobcountryname($id)
    {
        $city = self::where('countries.id', '=', $id)->lang()->active()->first();

        if (null === $city) {
            $city = self::where('countries.id', '=', $id)->active()->first();
        }
        return (isset($city->country))?$city->country:'';
    }
    public static function getjobcountrycode($id='')
    {
        $city = self::where('countries.id', '=', $id)->lang()->active()->first();
        if (null === $city) {
            $city = self::where('countries.id', '=', $id)->active()->first();
        }
        return (isset($city->code))?$city->code:'';
    }

}
