<?php

namespace App;

use App;
use App\Traits\Lang;
use App\Traits\IsDefault;
use App\Traits\Active;
use App\Traits\Sorted;
use Illuminate\Database\Eloquent\Model;

class JobSkill extends Model
{

    use Lang;
    use IsDefault;
    use Active;
    use Sorted;

    protected $table = 'job_skills';
    public $timestamps = true;
    protected $guarded = ['id'];
    //protected $dateFormat = 'U';
    protected $dates = ['created_at', 'updated_at'];
    public static function getjobskillname($id)
    {
        $city = self::where('job_skills.id', '=', $id)->lang()->active()->first();
 		if (null === $city) {
            $city = self::where('job_skills.id', '=', $id)->active()->first();
        }
        return (isset($city->job_skill))?$city->job_skill:'';
    }

    public static function getLangLastid()
    {
        $active_lang = app()->getLocale();
        $order = self::whereRaw('id = (select max(`id`) from job_skills where lang="'.$active_lang.'")')->first();
        return $order;
    }
}
